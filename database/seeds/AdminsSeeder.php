<?php

use Illuminate\Database\Seeder;
use App\Admin_user;


class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        Admin_user::create(array(
            'name'     => 'Davi Leichsenring',
            'email'    => 'davileichs@gmail.com',
            'password' => Hash::make('dragonlord'),
            'type'      => 1,
        ));
        
        Admin_user::create(array(
            'name'     => 'Contato',
            'email'    => 'contato@tibiacharts.com',
            'password' => Hash::make('dragon'),
            'type'      => 2,
        ));
    }
}
