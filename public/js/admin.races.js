$(document).ready(function(){

var coord;
var size;
var name;
var floor;

    $('#tableRaces').DataTable();


    $(document).on('click','.editRace',function(){
        $('#method').val('PATCH');
        var idRace = $(this).find('.getIdRace').val();
        var name = $(this).find('.getName').val();
        var type = $(this).find('.getType').val();
        var hp = $(this).find('.getHp').val();
        var exp = $(this).find('.getExp').val();
        var coords = $(this).find('.getCoords').val();

        $('#raceId').val(idRace);
        $('#raceName').val(name);
        $('#raceType').val(type);
        $('#raceHp').val(hp);
        $('#raceExp').val(exp);
        $('#raceCoords').val(coords);

        var html = '<iframe src="/map.html" width="540" height="350"></iframe>';
        $('.setMap').html(html);

    });

    $(document).on('click','.cleanForm',function(){
       $('.formRace').find("input[type=text],input[type=password], textarea").val("");
       $('#raceId').val('');
    });

    $(document).on('click','.newRace',function(){
        $('#method').val('PUT');
    });

    $(document).on('click','.deleteRace',function(e){
        if (confirm("Deleta mesmo?")) {
          $('#formDelete').submit();
      } else {
        e.preventDefault();
      }
    });

});
