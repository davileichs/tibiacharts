$(document).ready(function(){

    $('#tableHouses').DataTable();

    $(document).on('click','.editHouse',function(){
        var idHouse = $(this).find('.getIdHouse').val();
        var coord = $(this).find('.getCoord').val();
        var name = $(this).find('.getName').val();
        var floor = $(this).find('.getFloor').val();
        var idTown = $(this).find('.getIdTown').val();

        $('#housesIdHouse').val(idHouse);
        $('#housesCoord').val(coord);
        $('#housesFloor').val(floor);
        $('#housesName').val(name);
        $('#housesIdTown').val(idTown);

        start_zoom = 5;

        var html = '<iframe src="/map.html" width="540" height="350"></iframe>';
        $('.setMap').html(html);

    });

    $(document).on('click','.cleanForm',function(){
       $('.formHouse').find("input[type=text],input[type=password], textarea").val("");
       $('#housesIdHouse').val('');
    });

});

function getLatLongX() {

  var coords = $("#housesCoord").val();
  var latlong = coords.split(',');
  return parseInt(latlong[0]-1006);


}


function getLatLongY() {
  var coords = $("#housesCoord").val();
  var latlong = coords.split(',');
  return parseInt(latlong[1]-1024);

}

function getLatLongW() {

  var coords = $("#housesCoord").val();
  var latlong = coords.split(',');
  return parseInt(latlong[2]-1006);


}


function getLatLongE() {
  var coords = $("#housesCoord").val();
  var latlong = coords.split(',');
  return parseInt(latlong[3]-1024);

}


function getName() {

  return $("#housesName").val();

}



function getFloor() {
  return $("#housesFloor").val();
}
