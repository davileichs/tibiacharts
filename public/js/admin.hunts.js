$(document).ready(function(){

var coord;
var size;
var name;
var floor;

    $('#tablePlacesHunt').DataTable();


    $(document).on('click','.editHunt',function(){
        $('#method').val('PATCH');
        var idHunt = $(this).find('.getIdPlaceHunt').val();
        var name = $(this).find('.getName').val();

        var town = $(this).find('.getTown').val();
        $('#huntIdPlaceHunt').val(idHunt);
        $('#huntName').val(name);

        $('#huntTown').val(town);

        var latlong = coord.split(',');
        start_zoom = 5;
        start_x = parseInt(latlong[0]);
        start_y = parseInt(latlong[1]);

        var html = '<iframe src="/map.html" width="540" height="350"></iframe>';
        $('.setMap').html(html);

    });

    $(document).on('click','.cleanForm',function(){
       $('.formHunt').find("input[type=text],input[type=password], textarea").val("");
       $('#huntIdPlaceHunt').val('');
    });

    $(document).on('click','.newHunt',function(){
        $('#method').val('PUT');
    });


    $(document).on('click','.openPlaces',function(){
        var id = $(this).data('id');

        listPlaces(id);
    });

    $(document).on('click','.newPlace',function(){

        $('#formPlace').modal('show');

        $('#methodPlace').val('PUT');

        $('#title').val('');
        $('#huntFloor').val(7);
        $('#huntCoord').val('');
        $('#huntSize').val('');
        $('#huntIdPlaceHunt').val('');
        $('#huntLevel').val('');


        var html = '<iframe src="/map.html" width="540" height="350"></iframe>';
        $('.setMap').html(html);
    });

    $(document).on('click','.editPlace',function(){

        $('#formPlace').modal('show');
        $('#methodPlace').val('PATCH');

        var result = $.ajax({
            url: 'getPlace',
            data: {id: $(this).data('id')},
            async: false,
            success: function(data){}
        });

        var data = $.parseJSON(result.responseText);

        $('#title').val(data.title);
        $('#huntFloor').val(data.floor);
        $('#huntCoord').val(data.coord);
        $('#huntSize').val(data.size);
        $('#id').val(data.id);
        $('#huntIdPlaceHunt').val(data.id);
        $('#huntLevel').val(data.level);

        coord = data.coord;
        size = data.size;
        name = data.title;
        floor = data.floor;

        var latlong = coord.split(',');
        start_zoom = 5;
        start_x = parseInt(latlong[0]);
        start_y = parseInt(latlong[1]);

        var html = '<iframe src="/map.html" width="540" height="350"></iframe>';
        $('.setMap').html(html);


    });


    $(document).on('click','.copyPlace',function(){

        $('#formPlace').modal('show');
        $('#methodPlace').val('PUT');

        var result = $.ajax({
            url: 'getPlace',
            data: {id: $(this).data('id')},
            async: false,
            success: function(data){}
        });

        var data = $.parseJSON(result.responseText);

        $('#title').val(data.title);
        $('#huntFloor').val(data.floor);
        $('#huntCoord').val(data.coord);
        $('#huntSize').val(data.size);

        $('#huntIdPlaceHunt').val(data.id);

        coord = data.coord;
        size = data.size;
        name = data.title;
        floor = data.floor;

        var latlong = coord.split(',');
        start_zoom = 5;
        start_x = parseInt(latlong[0]);
        start_y = parseInt(latlong[1]);

        var html = '<iframe src="/map.html" width="540" height="350"></iframe>';
        $('.setMap').html(html);


    });



    $(document).on('submit','.deletePlace',function(e){
        e.preventDefault();

        if (confirm("Tem certeza que deseja deletar?")) {
            $.ajax({
               url: '/admin/places',
               data: $(this).serialize(),
               method: 'POST',
               success: function(data){
                   console.log(data);
                   $('#formPlace').modal('hide');
                   listPlaces(data);
               }
           });
       }
    });

    $(document).on('submit','.formPlace',function(e){
        e.preventDefault();

        $.ajax({
            url: '/admin/places',
            data: $('.formPlace').serialize(),
            method: 'POST',
            success: function(data){
                console.log(data);
                $('#formPlace').modal('hide');
                listPlaces(data);
            }
        });

    });

/***************************   RACES   *********************************/


    $(document).on('click','.openRaces',function(){

        var id = $(this).data('id');
        $("#place_id").val(id);

        listRaces(id);
    });

    $(document).on('click','.newRace',function(){

        $('#formRace').modal('show');

        $('#race').val('');
        $('#race').focus();

    });


    $(document).on('submit','.deleteRace',function(e){
        e.preventDefault();
    console.log($(this).serialize());

        if (confirm("Tem certeza que deseja deletar?")) {
            $.ajax({
               url: '/admin/place_race',
               data: $(this).serialize(),
               method: 'POST',
               success: function(data){
                   console.log(data);
                   $('#formPlace').modal('hide');
                   listRaces(data);
               }
           });
       }
    });

    $(document).on('submit','.formRace',function(e){
        e.preventDefault();

        $.ajax({
            url: '/admin/place_race',
            data: $('.formRace').serialize(),
            method: 'POST',
            success: function(data){

                $('#formRace').modal('hide');
                listRaces(data);
            }
        });

    });



});

function listPlaces(id)
{
    $("#hunt_id").val(id);
    $.ajax({
            url: 'getPlaces',
            data: {id: id},
            success: function(data){
                html = data;

                $('#placesBox').modal('show');
                $("#placesBox .modal-body").html(html);
            }, error: function(){
                console.log('error');
            }
        });
}


function listRaces(place_id)
{

    $.ajax({
            url: 'getRaces',
            data: {place_id: place_id},
            success: function(data){
                html = data;

                $('#racesBox').modal('show');
                $("#racesBox .modal-body").html(html);
            }, error: function(){
                console.log('error');
            }
        });
}

function getLatLongX() {

  var coords = $("#huntCoord").val();
  var latlong = coords.split(',');

  if (latlong[0]) {
    return parseInt(latlong[0]);
  }
    return false;
}


function getLatLongY() {
  var coords = $("#huntCoord").val();
  var latlong = coords.split(',');

  if (latlong[1]) {
    return parseInt(latlong[1]);
  }
    return false;

}


function getLatLongBounds() {

  var coords = $("#huntCoord").val();
  var latlong = coords.split(',');

  var bounds = [];
  var aux;
  if (latlong.length > 0) {
    latlong.forEach(function(value, i){

        if (i%2==1) {
          bounds.push([aux, value]);
        } else {
          aux = value;
        }
    });
    return bounds;
  }
    return false;

}

function getName() {

  return $("#huntName").val();

}


function getRadius() {

  return radius = parseInt($("#huntSize").val());

}


function getFloor() {
  return $("#huntFloor").val();
}
