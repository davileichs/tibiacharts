npcs = [];
function getNpcs() {
    if (npcs) {
        var length = npcs.length;
        for (var i = 0; i < length; i++) {
            newNpc(npcs[i].id, npcs[i].name, npcs[i].img, npcs[i].coord, npcs[i].floor, i);
        }
    }
}
function newNpc(id, name, img, coord, floor, pos){
    var x = coord[0]+1005;
    var y = coord[1]+1025;

    npcs[pos] = {
        map:
            new google.maps.Marker({
            map: map,
            position: CoordToLatLng(x,y),
            title: name,
            icon: img,
            id: id
          }),
        floor: floor
    };
    google.maps.event.addListener(npcs[pos].map, 'click', function() {
        loadNpc(this.id);
    });
}

function checkFloor(){
      if (npcs) {
        var length = npcs.length;

        for (var i = 0; i < length; i++) {
            npcs[i].map.setMap(npcs[i].floor == floor?map:null);

        }
    }
}

var boxText = document.createElement("div");
boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: #333; color: #FFF; padding: 5px;";
boxText.innerHTML = "";

function loadNpc(id) {
    var html = '';
    $.ajax({
        url: 'getNpc',
        data: {id: id},
        success: function(data){
            html = data;
            $('#infobox').modal('show');
            $(".modal-body").html(html);
        }, error: function(){
            console.log('error');
        }
    });
}


function listNpc() {

    $.get({
        url: 'listNpcs',
        data: {profession: $("#profession").val() },
        beforeSend: function() {

        },
        success: function(data){

           $("#showNpcs").html(data);

        }, error: function(){
            console.log('error');
        }
    });
}

$(document).ready(function(){

    if (npcs!='') {
        getNpcs();

    }

    $( "#tags" ).focus(function(){
        $(this).val('');
    });
    $( "#tags" ).autocomplete({
        source: npcsName,
        appendTo: "#containerTag",
        select: function( event, ui ) {

            $('<input>').attr({
                    type: 'hidden',
                    name: 'id',
                    value: ui.item.value
            }).appendTo('form');

                var start_x = ui.item.coord_x;
                var start_y = ui.item.coord_y;
                var floor = ui.item.floor;
                var start_zoom = 7;

                Init(start_x,start_y,floor,start_zoom);

            $(this).val(ui.item.label);
            $(this).blur();

            $('html, body').animate({
                scrollTop: $("#showMap").offset().top-50
            }, 1000);

            return false;
          }
    });

    $('.npc-unit').click(function(){
        var start_x = $(this).data('coord_x');
        var start_y = $(this).data('coord_y');
        var floor = $(this).data('floor');
        var start_zoom = 7;

        Init(start_x,start_y,floor,start_zoom);

        $('html, body').animate({
            scrollTop: $("#showMap").offset().top-50
        }, 1000);

    });



    $('#profession').change(function(){

        listNpc();

        $('html, body').animate({
            scrollTop: $("#showNpcs").offset().top-50
        }, 1000);

    });

});
