$(document).ready(function(){


    $('#tableProfessionsNpc').DataTable();


    $(document).on('click','.editNpc',function(){
        $('#method').val('PATCH');
        var idNpc = $(this).find('.getIdNpc').val();
        var name = $(this).find('.getName').val();
        var observation = $(this).find('.getObservation').val();
        var coord = $(this).find('.getCoord').val();
        var town = $(this).find('.getTown').val();
        var profession = $(this).find('.getProfession').val();

        $('#npcId').val(idNpc);
        $('#npcName').val(name);
        $('#npcCoord').val(coord);
        $('#npcObservation').val(observation);
        $('#npcTown').val(town);
        $('#npcProfession').val(profession);


        var result = $.ajax({
            url: 'getDataNpc',
            data: {id: idNpc},
            async: false,
            success: function(data){}
        });

        var data = $.parseJSON(result.responseText);

        $('#title').val(data.title);
        $('#npcFloor').val(data.floor);
        $('#npcCoord').val(data.coord);
        $('#id').val(data.id);

        coord = data.coord;
        size = data.size;
        name = data.title;
        floor = data.floor;

        var html = '<iframe src="/map.html" width="540" height="350"></iframe>';
        $('.setMap').html(html);


    });

    $(document).on('click','.cleanForm',function(){
       $('.formNpc').find("input[type=text],input[type=password], textarea").val("");
       $('#npcId').val('');
    });

    $(document).on('click','.newNpc',function(){
        $('#method').val('PUT');
    });


});

function listProfessions(id)
{
    $("#npc_id").val(id);
    $.ajax({
            url: 'getProfessions',
            data: {id: id},
            success: function(data){
                html = data;

                $('#professionsBox').modal('show');
                $("#professionsBox .modal-body").html(html);
            }, error: function(){
                console.log('error');
            }
        });
}


function getLatLongX() {

  var coords = $("#npcCoord").val();
  var latlong = coords.split(',');
  return parseInt(latlong[0]);


}


function getLatLongY() {
  var coords = $("#npcCoord").val();
  var latlong = coords.split(',');
  return parseInt(latlong[1]);

}


function getName() {

  return $("#npcName").val();

}


function getImg() {

  return radius = parseInt($("#huntSize").val());

}


function getFloor() {
  return $("#npcFloor").val();
}
