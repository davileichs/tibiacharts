var MAP_VERSION = 6;
var MAP_WIDTH = 8*256;
var MAP_HEIGHT = 8*256;
var ORIGIN_X = 32750;
var ORIGIN_Y = 32000;
var ORIGIN_Z = 7;
var MAX_ZOOM = 6;
var MIN_ZOOM = 1;
var map_max = Math.max(MAP_WIDTH,MAP_HEIGHT);
var map;
var floor;
var start_floor;
var start_x;
var start_y;
var start_zoom;
var type_tiles = 'tiles2';


function CheckInitCoords() {
    if (start_x == null)
    start_x = ORIGIN_X+MAP_WIDTH/2;
    if (start_y == null)
    start_y = ORIGIN_Y+MAP_HEIGHT/2;
    if (floor == null)
    start_floor = floor = ORIGIN_Z;
    if (start_zoom == null)
    start_zoom = 1;
}

function setInitCoords(coord_x, coord_y, floor_i, zoom) {

    start_x = coord_x;
    start_y = coord_y;
    start_floor = floor = floor_i;
    start_zoom = zoom;
}
//var hashupdate;
function LatLngToCoord(latLng) {
	return new google.maps.Point(ORIGIN_X+Math.floor(latLng.lng()*map_max/2.56),ORIGIN_Y+Math.floor(latLng.lat()*map_max/2.56));
}; 

function CoordToLatLng(x,y) {
	return new google.maps.LatLng((y-ORIGIN_Y+0.5)/map_max*2.56,(x-ORIGIN_X+0.5)/map_max*2.56);
}; 

function CoordBound(n,w,s,e) {
	return new google.maps.LatLngBounds(CoordToLatLng(e,n),CoordToLatLng(w,s));
}; 

function Coord(value) {
	return (value-ORIGIN_Y+0.5)/map_max*2.56;
}; 

function SelectElement(element) {
	var r = document.createRange();
	r.selectNodeContents(document.getElementById(element));
	window.getSelection().addRange(r);
}

function ChangeFloor(change) {
	floor+=change;
	
	if (floor<0)
		floor=0;
	else if (floor>15)
		floor=15;
		
	MapUpdate();
	map.setMapTypeId(floor+"");
	document.getElementById("floor_div").innerHTML = 7-floor;
	
	var backgroundColor = "#000000";
	
	if (floor==7)
	backgroundColor = "#336699";
	
	document.getElementById("tibia_map").style.backgroundColor = backgroundColor;

        
}; 

function MapUpdate() {
	var coord_x = LatLngToCoord(map.getCenter()).x;
	var coord_y = LatLngToCoord(map.getCenter()).y;
	var coord_z = floor;
	var zoom = map.getZoom();
	checkFloor();
	document.getElementById("info_coords").innerHTML = "x: "+coord_x+"&nbsp;&nbsp;&nbsp;y: "+coord_y+"&nbsp;&nbsp;&nbsp;z: "+floor;
	zoom2 = Math.max(3,zoom+2);

}; 

function setType() {
    if (type_tiles == 'tiles') {
        type_tiles = 'tiles2';
       
    } else {
        type_tiles = 'tiles';
    }
    Init();
}

function MapResize() {
	var center = map.getCenter(); 
	google.maps.event.trigger(map,"resize"); 
	map.setCenter(center); 
}

function SetSize() {
    document.getElementById("tibia_map").style.height=(window.innerHeight-300)+"px";  
}

function LoadMap() {
	var backgroundColor = "#000000";
	
	if (floor==7) {
            backgroundColor = "#336699";
        }
	var mapOptions = {
		backgroundColor: backgroundColor,
		center: CoordToLatLng(start_x,start_y),
		zoom: start_zoom,
		mapTypeControlOptions: {
			mapTypeIds: ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"]
		},
		disableDefaultUI: true,
		overviewMapControl: false,
		overviewMapControlOptions: {
			opened: "true"
		},
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE
		},
                
	};
	
	var tibiaMapOptions = {
		getTileUrl: function(coord,zoom) {
			var max = Math.pow(2,zoom) - 1;
			var x = coord.x;
			var y = max - coord.y;
			
			if (x<0 || x>max || y<0 || y>max) {
				if (floor==7)
					return "/map/"+type_tiles+"/blue.png?v=" + MAP_VERSION;
					
				return  "/map/"+type_tiles+"/black.png?v=" + MAP_VERSION;
			}
			return "/map/"+type_tiles+"/" + floor + "/" + zoom + "/" + x + "/" + y + ".png";
		},
		tileSize: new google.maps.Size(256,256),
		isPng: true,
		maxZoom: MAX_ZOOM,
		minZoom: MIN_ZOOM,
	};

	map = new google.maps.Map(document.getElementById("tibia_map"),mapOptions);

    var tibiaMapType;
	for (var i=0;i<=15;i++) {
		tibiaMapType = new google.maps.ImageMapType(tibiaMapOptions);
		tibiaMapType.projection = new SimpleProjection();
		map.mapTypes.set(i+"",tibiaMapType);
	}
	map.setMapTypeId(floor+"");
	google.maps.event.addListener(map,"bounds_changed",MapUpdate);
	google.maps.event.addDomListener(window,"resize",MapResize);
    
    
}
function fillColorMap() {
        rectangle.setOptions({
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#5BADFF',
          fillOpacity: 0.15,
          map: map,
          bounds: map.getBounds()
        });
      
}
function FloorControl() {
	var controlDiv = document.createElement("DIV");
	controlDiv.style.margin = "0px";
	controlDiv.style.padding = "0px";
	
	var upDiv = document.createElement("DIV");
	upDiv.style.cursor="pointer";
	upDiv.style.width = "20px";
	upDiv.style.height = "20px";
	upDiv.style.margin = "0px";
	upDiv.style.padding = "5px 6px";
	upDiv.title = "+1";
	upDiv.style.background = "url('/map/up.png') no-repeat center";
	controlDiv.appendChild(upDiv);
	
	var floorDiv = document.createElement("DIV");
	floorDiv.style.width = "20px";
	floorDiv.style.height = "21px";
	floorDiv.style.margin = "0px 0px";
	floorDiv.style.padding = "0px";
	floorDiv.style.pointerEvents = "none";
	floorDiv.style.background = "url('/map/floor.png') no-repeat center";
	controlDiv.appendChild(floorDiv);
	
	var textDiv = document.createElement("DIV");
	textDiv.id = "floor_div";
	textDiv.style.width = "18px";
	textDiv.style.height = "21px";
	textDiv.style.margin = "0px";
	textDiv.style.padding = "1px 0px";
	textDiv.style.textAlign = "center";
	textDiv.style.color = "red";
	textDiv.style.font = "bold 15px Helvetica,Arial,sans-serif";
	textDiv.innerHTML = 7-floor;
	floorDiv.appendChild(textDiv);
	
	var downDiv = document.createElement("DIV");
	downDiv.style.cursor="pointer";
	downDiv.style.width = "20px";
	downDiv.style.height = "20px";
	downDiv.style.margin = "0px 0px";
	downDiv.style.padding = "5px 6px";
	downDiv.title = "-1";
	downDiv.style.background = "url('/map/down.png') no-repeat center";
	controlDiv.appendChild(downDiv);
	
	var centerDiv = document.createElement("DIV");
	centerDiv.style.cursor="pointer";
	centerDiv.style.width = "20px";
	centerDiv.style.height = "20px";
	centerDiv.style.margin = "0px 0px";
	centerDiv.style.padding = "5px 6px";
	centerDiv.title = "Centro";
	centerDiv.style.background = "url('/map/center.png') no-repeat center";
	controlDiv.appendChild(centerDiv);
        
        var typeDiv = document.createElement("DIV");
	typeDiv.style.cursor="pointer";
	typeDiv.style.width = "20px";
	typeDiv.style.height = "20px";
	typeDiv.style.margin = "0px 0px";
	typeDiv.style.padding = "5px 6px";
	typeDiv.title = "Versão 2";
	typeDiv.style.background = "url('map/layer.png') no-repeat center";
//	controlDiv.appendChild(typeDiv);
	
	var CENTER = CoordToLatLng(start_x,start_y);
	google.maps.event.addDomListener(upDiv,"click",function() {ChangeFloor(-1)});
	google.maps.event.addDomListener(downDiv,"click",function() {ChangeFloor(+1)});
	google.maps.event.addDomListener(centerDiv,"click",function() {map.setCenter(CENTER)});
//        google.maps.event.addDomListener(typeDiv,"click",function() {setType()});
	map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);
}

function Crosshair() {
	if (map.controls[google.maps.ControlPosition.TOP_CENTER].getLength()==0) {
		var xDiv = document.createElement("DIV");
		xDiv.style.width = "100%";
		xDiv.style.height = "1px";
		xDiv.style.background = "url('/images/cx.png') repeat-x center";
		xDiv.style.pointerEvents = "none";
		map.controls[google.maps.ControlPosition.LEFT_CENTER].push(xDiv);
		
		var yDiv = document.createElement("DIV");
		yDiv.style.width = "1px";
		yDiv.style.height = "100%";
		yDiv.style.background = "url('/images/cy.png') repeat-y center";
		yDiv.style.pointerEvents = "none";
		map.controls[google.maps.ControlPosition.TOP_CENTER].push(yDiv);
	}else{
		map.controls[google.maps.ControlPosition.LEFT_CENTER].clear();
		map.controls[google.maps.ControlPosition.TOP_CENTER].clear();
	}
};

function SimpleProjection() {
};

SimpleProjection.prototype.fromLatLngToPoint = function(latLng) {
	return new google.maps.Point(latLng.lng()*100,latLng.lat()*100);
};

SimpleProjection.prototype.fromPointToLatLng = function(point,noWrap) { 
	return new google.maps.LatLng(point.y/100,point.x/100);
};

function Init(coord_x,coord_y,floor,zoom) {
	
        if (coord_x != null && coord_y != null && floor != null && zoom != null) {
            setInitCoords(coord_x,coord_y,floor,zoom);
        } else {
            SetSize();
        }
        CheckInitCoords();
        
	LoadMap();
	FloorControl();
	Crosshair();
}

$(document).ready(function(){
    Init();
});
