houses = [];
function getHouses() {
    if (houses) {
        var length = houses.length;
        for (var i = 0; i < length; i++) {
            newHouse(houses[i].id, houses[i].name, houses[i].color, houses[i].coord, houses[i].floor, i);
        }
    }
}

function newHouse(id, name, color, coord, floor, pos) {
    var bound = CoordBound(coord[3],coord[2],coord[1],coord[0]);
    houses[pos] = {
        map:
            new google.maps.Rectangle({
            strokeColor: '#000000',
            strokeOpacity: 1,
            strokeWeight: 2,
            fillColor: color,
            fillOpacity: 0.9,
            map: map,
            bounds:  bound,
            title: name,
            id: id
            }),
        floor: floor
    };
    google.maps.event.addListener(houses[pos].map, 'click', function() {
        loadHouse(this.id);
    });

}

function checkFloor() {

    if (houses) {
        var length = houses.length;

        for (var i = 0; i < length; i++) {
            houses[i].map.setMap(houses[i].floor == floor?map:null);


        }
    }
}

$(document).ready(function() {
    getHouses();
});

function loadHouse(id) {

    $.get({
        url: 'getHouse',
        data: {house_id: id, world_id: $("#head_world").val()},
        beforeSend: function() {

        },
        success: function(data){

           var html = data;

           $('#infobox').modal('show');
//           $('.modal-title').html(house.name + " - " + house.type);
           $(".modal-body").html(html);
        }, error: function(){
            console.log('error');
        }
    });
}

function listHouse() {

    $.get({
        url: 'listHouses',
        data: {status: $('#housesstatus').val(), world_id: $("#idworld").val(), town_id: $("#idwtown").val(), order: $("#changeorder").val() },
        beforeSend: function() {

        },
        success: function(data){

           $("#showHouses").html(data);

           $('html, body').animate({
               scrollTop: $("#showHouses").offset().top-50
           }, 1000);

        }, error: function(){
            console.log('error');
        }
    });
}

$(document).ready(function() {

    // if ($('#idworld').val()!='') {
    //     $('html, body').animate({
    //         scrollTop: $("#showMap").offset().top-50
    //     }, 1000);
    // }

    $('#idwtown').change(function(){
      listHouse();
    });


    $('.listHouse').click(function() {
        var status = $(this).data('status');
        $('#housesstatus').val(status);
        listHouse();
        $('html, body').animate({
            scrollTop: $("#showHouses").offset().top-50
        }, 1000);
    });



});
