$(document).ready(function(){

    
    $(document).on('click','.editProfession',function(){
        $('#method').val('PATCH');
        var idProfession = $(this).find('.getIdProfession').val();
        var name = $(this).find('.getName').val();
       
        
        var town = $(this).find('.getTown').val();
        $('#professionId').val(idProfession);
        $('#professionName').val(name);

    
        
    });
    
    $(document).on('click','.cleanForm',function(){
       $('.formProfession').find("input[type=text],input[type=password], textarea").val("");
       $('#professionId').val('');
    });
    
    $(document).on('click','.newProfession',function(){
        $('#method').val('PUT');
    });


    $(document).on('click','.openProfessions',function(){
        var id = $(this).data('id');
        
        listProfessions(id);
    });
    
    $(document).on('click','.newProfession',function(){
        
        $('#formProfession').modal('show');
        
        $('#methodProfession').val('PUT');
        
        $('#title').val('');
        $('#professionIdProfession').val('');

    });
    
    $(document).on('click','.editProfession',function(){
        
        $('#formProfession').modal('show');
        $('#methodProfession').val('PATCH');
        
        var result = $.ajax({
            url: 'getProfession',
            data: {id: $(this).data('id')},
            async: false,
            success: function(data){}
        });
        
        var data = $.parseJSON(result.responseText);
        
        $('#title').val(data.title);
      
        $('#id').val(data.id);
        $('#professionIdProfession').val(data.id);         
        
    });
    
  
    
    $(document).on('submit','.deleteProfession',function(e){
        e.preventDefault();
    
        if (confirm("Tem certeza que deseja deletar?")) {
            $.ajax({
               url: '/admin/professions',
               data: $(this).serialize(),
               method: 'POST',
               success: function(data){
                   $('#formProfession').modal('hide');
                   location.reload();
               }   
           });
       }
    });
    
    $(document).on('submit','.formProfession',function(e){
        e.preventDefault();
        
        $.ajax({
            url: '/admin/professions',
            data: $('.formProfession').serialize(),
            method: 'POST',
            success: function(data){
                console.log(data);
                $('#formProfession').modal('hide');
                location.reload();
            }   
        });
        
    });
});
