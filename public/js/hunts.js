hunts = [];
function getHunts() {
    if (hunts) {
        var length = hunts.length;
        for (var i = 0; i < length; i++) {
            newHunt(hunts[i].id, hunts[i].name, hunts[i].coord, hunts[i].color, hunts[i].radius, hunts[i].floor, i);
        }
    }
}
function newHunt(id, name, coord, color, radius, floor, pos){
    var x = coord[0]+1005;
    var y = coord[1]+1025;

    var bounds = [];
    var aux;
    if (coord.length > 0) {
      coord.forEach(function(value, i){

          if (i%2==1) {

            bounds.push (CoordToLatLng(aux+1005, value+1025));
          } else {
            aux = value;
          }
      });
    }


    if (bounds.length >= 3) {

    } else {

          bounds = [
            CoordToLatLng(x, y),
            CoordToLatLng(x, y+20),
            CoordToLatLng(x+40, y+20),
            CoordToLatLng(x+40, y)
          ];
    }

      hunts[pos] = {
          map:
              new google.maps.Polygon({
              map: map,
              strokeColor: '#FFFFFF',
              strokeOpacity: 1,
              strokeWeight: 2,
              fillColor: color,
              fillOpacity: 0.75,
              title: name,
              paths: bounds,
              id: id
            }),
          floor: floor
      };

    google.maps.event.addListener(hunts[pos].map, 'click', function() {
        loadHunt(this.id);
    });
}
var labelPos;
function checkFloor(){
    if (hunts) {
        var length = hunts.length;

        for (var i = 0; i < length; i++) {
            hunts[i].map.setMap(hunts[i].floor == floor?map:null);
        }
    }
}

var boxText = document.createElement("div");
boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: #333; color: #FFF; padding: 5px;";
boxText.innerHTML = "";

$(document).ready(function(){
    if (hunts!='') {
        getHunts();
    }
});

function loadHunt(id) {
    var html = '';
    $.ajax({
        url: 'getHunt',
        data: {id: id},
        success: function(data){
            html = data;
            $('#infobox').modal('show');
            $(".modal-body").html(html);
        }, error: function(){
            console.log('error');
        }
    });
}

function listHunts(race_id) {
    console.log(race_id);
    $.get({
        url: 'listHunts',
        data: {race_id: race_id},
        beforeSend: function() {

        },
        success: function(data){

           $("#showHunts").html(data);

        }, error: function(){
            console.log('error');
        }
    });
}

$(document).ready(function(){


        $('html, body').animate({
            scrollTop: $("#showMap").offset().top-50
        }, 1000);


    if ($('.listHunts').val()!= "") {

        var race_id = $('.listHunts').val();
        listHunts(race_id);

        $('html, body').animate({
            scrollTop: $("#showHunts").offset().top-50
        }, 1000);
    };

});
