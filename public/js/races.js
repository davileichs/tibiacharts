races = [];
mapRaces = [];

function getRaces() {
    if (races) {
        var length = races.length;
        for (var i = 0; i < length; i++) {
            newRace(races[i].id, races[i].name, races[i].img, races[i].coord, i);
        }


        $.each(mapRaces, function(i, race){
          google.maps.event.addListener(race.map, 'click', function() {
              loadRace(this.id);
          });
        });


    }
}
function newRace(id, name, img, coords, pos){

  $.each(coords, function(index, value){

        mapRaces.push( {
          map:
              new google.maps.Marker({
              map: map,
              position: CoordToLatLng(value.x+1005,value.y+1025),
              title: name,
              icon: img,
              id: id
            }),
            floor: value.floor
        });
    });
}
var labelPos;

function checkFloor(){
    if (mapRaces) {
        var length = mapRaces.length;

        for (var i = 0; i < length; i++) {
            mapRaces[i].map.setMap(mapRaces[i].floor == floor?map:null);
        }
    }
}

var boxText = document.createElement("div");
boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: #333; color: #FFF; padding: 5px;";
boxText.innerHTML = "";

$(document).ready(function(){
    if (races!='') {
        getRaces();
    }
});

function loadRace(id) {
    var html = '';
    $.ajax({
        url: 'getRace',
        data: {id: id},
        success: function(data){

            html = data;
            $('#infobox').modal('show');
            $(".modal-body").html(html);

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {

                  var jsonData = $.ajax({
                  url: "/getKills",
                  //  dataType: "json",
                  data: {'race_id': id},
                  async: false
                  }).responseText;

                  // Create our data table out of JSON data loaded from server.
                  // var data = new google.visualization.DataTable(eval(jsonData));

                  var returnData = [];
                  returnData.push(['Date', 'Players killed', 'Killed by Player']);

                  $.each(eval(jsonData), function(i, value){
                      returnData.push([value.day, value.kill, value.killed]);
                  });



                  var data = google.visualization.arrayToDataTable(returnData);

                  var options = {
                    width: '100%',
                    bar: {groupWidth: "95%"},
                    legend: { position: 'none' },
                    series: {
                      0:{color: 'black', visibleInLegend: true},
                      1:{color: 'red', visibleInLegend: true}
                    }
                  };

                  var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));

                  chart.draw(data, options);
            }

        }, error: function(){
            console.log('error');
        }
    });
}



function listRaces(race_id) {

    $.get({
        url: 'listRaces',
        data: {id: race_id},
        beforeSend: function() {

        },
        success: function(data){

           $("#showRaces").html(data);

        }, error: function(){
            console.log('error');
        }
    });
}

$(document).ready(function() {
        //
        // $('html, body').animate({
        //     scrollTop: $("#showMap").offset().top-50
        // }, 1000);


        $('.listRaces').change(function(){

            listRaces($(this).val());

            $('html, body').animate({
                scrollTop: $("#showRaces").offset().top-50
            }, 1000);

        });


});
