<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PlayersLog extends Model
{
    
    
    protected $dates = ['date'];
    protected $table = 'players_log';
    public $dateStart = '';
    public $dateEnd = '';
    
    
    
    public function __construct()
    {
        $this->dateStart = date('Y-m-d', strtotime('-6 days'));
        $this->dateEnd = date('Y-m-d');
        
    }
    
    
    
    public function player()
    {
        return $this->belongsTo(Player::class, 'idplayer');
    }
    
    
    
    public function getDateJsAttribute()
    {
        return datePHPtoJS($this->attributes['date'], 'year');
    }
    
    
    public function getHourJsAttribute()
    {
        return datePHPtoJS($this->attributes['date']);
    }
    

}
