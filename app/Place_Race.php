<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Place_Race extends Model
{
    protected $fillable = ['place_id', 'race_id'];
    public $timestamps = false;
    public $table = 'places_races';




}
