<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Races_loot extends Model
{
    public $timestamps = false;
    
    
    
    public function race() {
        
        return $this->belongsTo(Race::class, 'race_id');
    }
    
    
    
    public function logs() {
        
        return $this->belongsTo(LogsSystem::class);
    }
    
    
    
    public function getLootAttribute()
    {
        $loot = $this->attributes['loot'];
        
        $arrayImg = getImgs('itens');

        preg_match_all('/(?P<quantum>\d+)(?P<item>[[:alnum:]\'+\s\,]+)\((?P<amount>\d+)\)/',$loot, $matches);
     
        $return = array();
        
        foreach ($matches['item'] as $j=>$item) {
            
            $file = imgMatch($arrayImg, $item);
            
            $list = new \stdClass();
       
            if ($file) {
                $list->img = "/images/itens/".$file.".gif";
                $list->title = titleCase($item);
            
                $list->amount = $matches['amount'][$j];
             
                if ($item!='nothing') {
                    $list->urlWiki = 'http://www.tibiawiki.com.br/wiki/'.urlencode(str_replace(' ','_', titleCase($item)));
                } else {
                    $list->urlWiki = '';
                }
                
                array_push($return, $list);
            }
        }

        return $return;
                
    }
}
