<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorldPlayersOnline extends Model
{
    
    
    
    public function world() {
        
        return $this->belongsTo(World::class);
    }
    
    
    
    public function scopeLast($query) {
        
        return $query->orderBy('date','desc')->orderBy('hour','desc');
    }
    
}
