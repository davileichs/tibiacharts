<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    
    
    protected $fillable = ['name', 'date_start', 'date_end', 'description'];
    
    
    
    public function getDateStartFormatedAttribute()
    {
        $date = $this->attributes['date_start'];
        
        return implode('/',array_reverse(explode('/', $date))).'/'.date('Y');
    }
    
    
    public function getDateEndFormatedAttribute()
    {
        $date = $this->attributes['date_end'];
        return implode('/',array_reverse(explode('/', $date))).'/'.date('Y');
    }
    
    
    
    public function getDateStartIntAttribute()
    {
        $date = $this->attributes['date_start'];
        return $date.'/'.date('Y');
    }
    
    
    
    public function getDateEndIntAttribute()
    {
        $date = $this->attributes['date_end'];
        return $date.'/'.date('Y');
    }
    
    
    
    public function getColorAttribute()
    {
        return randColor($this->id);
        
    }
    
    
    public function getNameUrlAttribute()
    {
        return urlencode($this->name);
        
    }
    
    
    public function getNameHtmlAttribute()
    {
        return mb_convert_encoding($this->name, "UTF-8", "HTML-ENTITIES");
        
    }
}
