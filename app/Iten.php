<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iten extends Model
{
    
    public static $url = '';
    public static $urlBase = 'http://www.tibiawiki.com.br/wiki/';
    
    public $timestamps = false;
    
    /* script to capture itens from tibiawiki */
    public static function getItensContent() 
    {

        $itemArray = array();
        $arrayTd = array();
        
        $tables = getData(self::$urlBase.self::$url,'','.sortable');

        $line = 1;
        foreach($tables['tr'] as $tr) {
                
            $td = trim($tr->firstChild->nodeValue);
        
            $arrayTd = preg_replace('/\s+/', ' ', $td);
            
            if (strcmp('Nome', $td) != 0) {
                array_push($itemArray, $arrayTd);
            }
            
         }

        return $itemArray;

    }
}
