<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    
    
    
    public function npcs() 
    {
        $this->hasMany(Npc::class);
    }
    
    
    
    public function houses() 
    {
        $this->hasMany(House::class);
    }
    
    
    
    public function housesStatus() {
        
        return $this->hasMany(HouseStatus::class);
    }
    
    
    
    public function hunts() 
    {
        $this->hasMany(Hunt::class);
    }
    
    
    public function getCoordXAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);
        return $coords[0];
        
    }
    
    public function getCoordYAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);
        return $coords[1];
        
    }
}
