<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Npc extends Model
{    
    
    public static $url = 'Lista_de_NPCs_por_Localiza%C3%A7%C3%A3o';
    public static $urlBase = 'http://www.tibiawiki.com.br/wiki/';
    
    protected $fillable = ['name', 'town_id', 'coord', 'floor', 'observation', 'profession_id'];
    public $timestamps = false;
    
    
    public function town() 
    {
        return $this->belongsTo(Town::class);
    }

    
    
    public function profession() 
    {
        return $this->belongsTo(Profession::class);
    }
    
    
    
    public function getRealFloorAttribute() 
    { 
        return (($this->attributes['floor'] -7) * (-1));
    }
    
    
    
    public function getImagePathAttribute() 
    {
        if (empty($arrayImg)) {
            $arrayImg = getImgs('npcs');
        }
        $path = '/images/npcs/'.imgMatch($arrayImg, $this->attributes['name']).'.gif';
        
        return $path;
    }
    
    
    
    public function getNameDecodedAttribute()
    {
        return str_replace("'","&apos;",$this->attributes['name']);
    }
    
    
    
    public function getNameUrlAttribute()
    {
        return str_replace(" ","_",$this->attributes['name']);
    }
    
    
    
     public function getCoordXAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);
        return ($coords[0]+1005);
        
    }
    
    
    
    public function getObservationTransAttribute()
    {
        $locale = \App::getLocale();
        
        $text = $this->attributes['observation'];

        preg_match('/\<'.$locale.'>(?P<translated>.*)\<\/'.$locale.'\>/', $text, $match);
        
        if (!empty($match['translated'])) {
            return ($match['translated']);
        } else {
            return '';
        }

        
    }
    
    
    
    public function getCoordYAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);
        return ($coords[1]+1025);
        
    }
    
    
    /* script to capture npcs from tibiawiki */
    public static function getNpcsContent() 
    {

        $npcArray = array();
        $arrayTd = array();
        
        $tables = getData(self::$urlBase . self::$url,'','.sortable');

        $line = 1;
        foreach($tables['tr>td'] as $tr) {

            $td = $tr->nodeValue;
        
            $find = array('/\s+\s+/', '/\n/');
            $replace = array('\s+', '');
            $arrayTd[$line] = preg_replace($find, $replace, $td);
            
            if ($line==5) {
                array_push($npcArray, $arrayTd);
                
                $line = 1;
                $arrayTd = array();
            } else {
                $line++;
            }
            
         }

        return $npcArray;

    }
 
}
