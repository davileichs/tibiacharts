<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{

    protected $fillable = ['coord', 'floor', 'town_id'];
    public $timestamps = false;


    public function town()
    {
        return $this->belongsTo(Town::class);
    }



    public function status()
    {
        return $this->hasMany(HouseStatus::class);
    }



    public function alert()
    {
        return $this->hasMany(UserHouseAlert::class);
    }



    public function getRealFloorAttribute()
    {
        return (($this->attributes['floor'] -7) * (-1));
    }



    public function getCoordXAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);
        $center = ($coords[0]+$coords[2])/2;
        return $center;

    }



    public function getCoordYAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);
        $center = ($coords[1]+$coords[3])/2;
        return $center;
    }



    public function getNameDecodedAttribute()
    {
        return urlencode($this->attributes['name']);
    }



    public function getNameUtf8Attribute()
    {
        return str_replace("&nbsp;"," ",htmlentities($this->attributes['name'], ENT_QUOTES, "UTF-8"));
    }


    public function getTypeAttribute()
    {
        return ucfirst($this->attributes['type']);
    }



    public function getCoordAuxAttribute()
    {
        if ($this->attributes['coord'] == '') {
            $town = Town::where('id',$this->attributes['town_id'])->first();
            return $town->coord;
        }
    }

}
