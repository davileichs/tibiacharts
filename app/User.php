<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
   

    public function players() 
    {
        return $this->belongsToMany(Player::class, 'user_players')->wherePivot('type', 1);
    }
    

    
    public function friends() 
    {
        return $this->belongsToMany(Player::class, 'user_players')->wherePivot('type', 2);
    }
    
    
    
    public function enemies() 
    {
        return $this->belongsToMany(Player::class, 'user_players')->wherePivot('type', 3);
    }

    
    
    public function worlds() 
    {
        return $this->hasManyThrough(World::class, Player::class);
    }
    
    
    
    public function alert()
    {
        return $this->hasMany(UserHouseAlert::class);
    }
    
}
