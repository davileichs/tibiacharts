<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogsSystem extends Model
{
        
    public $table = 'logs_system';
    public $timestamps = false;
    
    protected $fillable = ['name', 'hunt', 'hits', 'exp', 'heal', 'itens_used', 'date', 'code', 'profit', 'waste'];
    
    
    
    public function player()
    {
        return $this->belongsTo(Player::class);
    }
    
    
    
    public function races()
    {
        return $this->hasMany(Races_loot::class);
    }
    
    
    
    public function getImagePathAttribute() 
    {
        
        $arrayImg = getImgs('creaures');
        
        return imgMatch($arrayImg, $this->attributes['name']);
    }
    
    
    
    public function getExpArrayAttribute()
    {
        $experiences = json_decode($this->attributes['exp']);
        foreach ($experiences as &$exp) {
            $exp *= 60;
        }
        return ($experiences);
    }
    
    
    
    public function getItensUsedAttribute()
    {
        $itens = $this->attributes['itens_used'];
        $ItensAux = explode(',',$itens);
        
        $arrayItens = getImgs('itens');
        
        $return = array();
        
        foreach ($ItensAux as $i=>$itemUsed) {
            
            $aux = explode(':',$itemUsed);
            $list = new \stdClass();
            
            $file = imgMatch($arrayItens, $aux[0]);

                if ($file) {
                    $list->img = '/images/itens/'.$file.'.gif';
                    $list->title = ucwords($aux[0]);
                    $list->label = $aux[1];
                }
            array_push($return, $list);
        }
        
        return $return;
    }
    
    
    
    public function getHuntAttribute()
    {
            
        $hunt = explode(',',$this->attributes['hunt']);
        
        $log = new \stdClass();
        $log->exp = $hunt[0];
        $log->time = $hunt[1];
        $log->perhour = $hunt[2];
        $log->perhour_int = str_replace('k','000',$hunt[2]);

        return $log;
        
    }
    
    
    
    public function getHitsAttribute()
    {

        $hits = explode(',',$this->attributes['hits']);
        
        $log = new \stdClass();
        $log->total = $hits[0];
        $log->dpt = $hits[3];
        $log->best = $hits[2];

        return $log;
        
    }
    
    
    
    public function getHealAttribute()
    {
        $heal = explode(',',$this->attributes['heal']);

        $log = new \stdClass();
        $log->total = round($heal[0]);
        $log->dpt = round($heal[2]);
        $log->best = round($heal[1]);

        return $log;
        
    }
    
    
    
    public function getProfitAttribute()
    {
        $hunt = explode(',',$this->attributes['hunt']);
            
        $totalTime = explode(" h",trim($hunt[1]," min"));

        $minTotal = ($totalTime[0]*60)+($totalTime[1]);
        
        $media = $this->attributes['profit']/$minTotal;

        $result = $media*60;
        
        $log = new \stdClass();
        $log->profitPerhour = round($result);

        $log->total = number_format(($this->attributes['profit']-$this->attributes['waste']), 0, ',','.');
        $log->profit_noformat = $this->attributes['profit'];
        $log->profit = number_format($this->attributes['profit'], 0, ',','.');
        $log->waste = number_format($this->attributes['waste'], 0, ',','.');

        return $log;
        
    }
    
    
    
    
}
