<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hunts_Place extends Model
{
    protected $fillable = ['title', 'coord', 'floor', 'hunt_id', 'size', 'level'];
    public $timestamps = false;
    public $table = 'hunts_places';



    public function races() {

        return $this->belongsToMany(Race::class, 'places_races', 'place_id');
    }



    public function hunt() {

        return $this->belongsTo(Hunt::class);
    }



    function getRealFloorAttribute() {

        return (($this->attributes['floor'] -7) * (-1));
    }



    public function getCoordXAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);

        $aux = 0;
        $t = count($coords)/2;
        foreach ($coords as $i=>$coord) {
           if ($i%2==0) {
              $aux += $coord;
           }
        }

        $x_center = $aux/$t;

        $center = ($x_center+1005);
        return $center;

    }



    public function getCoordYAttribute()
    {
        $coords = explode(',',$this->attributes['coord']);

        $aux = 0;
        $t = count($coords)/2;
        foreach ($coords as $i=>$coord) {
           if ($i%2==1) {
              $aux += $coord;
           }

        }

        $y_center = $aux/$t;

        $center = ($y_center+1025);
        return $center;
    }



    function getColorAttribute() {
        $level = $this->attributes['level'];


        if ( $level < 49) {
            return '#53D42D';
        }
        else if ($level >= 50 && $level < 99) {
            return '#A1D42D';
        }
        else if ($level >= 100 && $level < 149) {
            return '#F5F92E';
        }
        else if ($level >= 150 && $level < 199) {
            return '#F9DA2E';
        }
        else if ($level >= 200 && $level < 249) {
            return '#ED541E';
        } else {
            return '#000000';
        }

    }


}
