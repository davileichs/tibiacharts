<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseStatus extends Model
{
    
    public $timestamps = false;
    
    protected $table = 'houses_status';
    
    public static $url = 'subtopic=houses';
    public static $urlBase = 'https://secure.tibia.com/community/?';
    
    
    
    public function house() 
    {
        return $this->belongsTo(House::class);
    }
    
    
    
    public function world() 
    {
        return $this->belongsTo(World::class);
    }
    
    
    
    public function town() 
    {
        return $this->belongsTo(Town::class);
    }
    

    
    public function getLastUpdateFormatedAttribute()
    {
        
        $date = $this->attributes['last_update'];
        
        return implode('/',array_reverse(explode('-', $date)));
    }
    
    
    
    public function getSimpleStatusAttribute()
    {
        preg_match('/(?P<status>[^(]*)/',$this->attributes['status'],$match);
        
        return $match['status'];
    }
    
    
    
    public function getTimeStatusAttribute()
    {
        preg_match('/(\((?P<left>.*)\))$/',$this->attributes['status'],$match);
        
        if (!empty($match['left'])) {
            return explode(';',$match['left']);
        }
    }
    
    
    public function getColorAttribute() 
    {
        $status = $this->attributes['status'];
        
        switch($status) {
            case 'rented':
                return "#FF0000";
                
            case 'auctioned (no bid yet)':
                return "#00FF00";
                
            default:
                return '#FFFF00';
            
        }
    }
    
    
    public function getBtnAttribute()
    {
        
        $status = $this->attributes['status'];
        
        switch($status) {
            case 'rented':
                return 'danger';
                
            case 'auctioned (no bid yet)':
                return 'success';
                
            default:
                return 'warning';
            
        }
        
        
    }
    
    
    
    /* script to capture houses status from tibia.com */
    public static function getHousesContent($world = '', $town = '') 
    {
        $types = array('houses', 'guildhalls');
        $housesArray = array();
        $stringTd = '';
        
        foreach ($types as $type) {
            $post = 'world='.$world.'&town='.$town.'&type='.$type;
            $tables = getData(self::$urlBase.self::$url, $post);           
           
            foreach($tables['tr>td'] as $tr) {
                
                $td = $tr->nodeValue;
                
                if (stristr($td,'World')==false
                    && stristr($td,'Available ')==false
                    && stristr($td,'Name')==false
                    && stristr($td,'Size')==false
                    && strstr($td,'Rent')==false
                    && stristr($td,'Status')==false) 
                {

                    if (strlen($td)>2) {
                        $td = utf8_decode(str_replace('&Acirc;','', $td));
                        $stringTd .= $td.'|';

                    } else {

                        if (!empty($stringTd)) {
                            $arrayTd =  explode('|', $stringTd);
                            $arrayTd[4] = substr($type,0,-1);

                            if (count($arrayTd)==5) {
                                    array_push($housesArray, $arrayTd);
                            }

                            $stringTd = '';
                        }
                    }
                }
            }
        }

        return $housesArray;

    }
    
    
    
    /* script to capture unique house status from tibia.com */
    public static function getHouseContent($world, $town, $house_id) 
    {
        self::$url = 'subtopic=houses&page=view';
        $housesArray = array();
        $stringTd = '';
        

        $post = 'world='.$world.'&town='.urlencode($town).'&houseid='.$house_id;
        $tables = getData(self::$urlBase.self::$url, $post);           

        preg_match('/The auction(?P<content>.*)/', $tables, $match);

        if (empty($match['content'])) {
            return false;
        }
        preg_match('/will end at (?P<time>.*)CEST.* is <b>(?P<gold>.*) gold.*by (?P<player>.*)\./',utf8_encode($match['content']),$housesArray);

        return $housesArray;

    }
    
   
}
