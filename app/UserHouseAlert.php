<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHouseAlert extends Model
{
    
   
    protected $table = 'user_houses_alert';
    
    protected $fillable = ['user_id', 'house_id', 'world_id'];




    public function user()
    {
       return $this->belongsTo(User::class);
    }
    
    
    
    public function world()
    {
       return $this->belongsTo(World::class);
    }
    
    
    
    public function house()
    {
       return $this->belongsTo(House::class);
    }
    
   
}
