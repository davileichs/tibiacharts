<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{    
    
    
    protected $fillable = ['name'];
    public $timestamps = false;
    
    
    
    public function getProfessionTransAttribute()
    {
        $locale = \App::getLocale();
        
        $text = $this->attributes['name'];

        preg_match('/\<'.$locale.'>(?P<translated>.*)\<\/'.$locale.'\>/', $text, $match);
        
        if (!empty($match['translated'])) {
            return ($match['translated']);
        } else {
            return '';
        }
        
    }
 
}
