<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    protected $arrayImg;
    public static $url = 'Lista_de_Criaturas';
    public static $urlBase = 'http://www.tibiawiki.com.br/wiki/';
    protected $fillable = ['name', 'type', 'coords', 'hp', 'exp'];
    public $timestamps = false;



    public function places() {

        return $this->belongsToMany(Place_Race::class, 'places_races', 'race_id', 'place_id');
    }



    public function loots() {

        return $this->hasMany(Races_loot::class,'race_id');
    }



    public function getImagePathAttribute() {

        if (empty($this->arrayImg)) {
            $this->arrayImg = getImgs('creatures');
        }

        $path = '/images/creatures/'.imgMatch($this->arrayImg, $this->attributes['name']).'.gif';

        return $path;
    }



    public function getNameUrlAttribute()
    {
        return str_replace(" ","_",$this->attributes['name']);
    }



    public function getHpNumberAttribute()
    {
        $hp = number_format($this->attributes['hp'], 0, ',', '.');

        return $hp;
    }



    public function getExpNumberAttribute()
    {
        $exp = number_format($this->attributes['exp'], 0, ',', '.');

        return $exp;
    }



    public function getTypeNameAttribute()
    {
        if ($this->attributes['type'] == 1) {
          return 'Boss';
        }

    }



    function getCoordsObjAttribute() {

        $arr_coord = '['.$this->attributes['coords'].']';
        $coords = json_decode($arr_coord);

        foreach ($coords as &$coord) {
            $coord->x += 1005;
            $coord->y += 1025;
        }

        return $coords;

    }



    /* script to capture npcs from tibiawiki */
    public static function getRaceListContentWiki()
    {

        $raceArray = array();
        $arrayTd = array();

        $tables = getData(self::$urlBase.self::$url);

        $line = 1;
        $start = false;
        foreach($tables['tr>td'] as $tr) {

            $td = $tr->nodeValue;

            $tdx = preg_replace('/\s+/', ' ', $td);

                $arrayTd[$line] = $tdx;

                if ($line=='7') {
                    array_push($raceArray, $arrayTd);

                    $line = 1;
                    $arrayTd = array();
                } else {
                    $line++;
                }

         }

        return $raceArray;

    }



    /* script to capture information of especific race from tibiawiki */
    public static function getRaceContentWiki($url)
    {

        $raceArray = array();
        $arrayTd = array();

        $tables = getData(self::$urlBase.$url);

        foreach($tables['tr>td'] as $tr) {

            $td = $tr->nodeValue;

            $tdx = preg_replace('/\s+/', ' ', $td);

            return $tdx;

         }


    }



    static public function getRaceListContentTibia()
    {

    		$tables = getData(self::$urlBase.self::$url, '', 'div');

    		$racesArray = array();

        // dd($tables->nodeValue);

    		foreach($tables['div'] as $tr) {

    						$td = preg_split('/&nbsp;/', htmlentities($tr->nodeValue), 0, PREG_SPLIT_NO_EMPTY);

    						if (!empty($td)) {

    							array_push($racesArray, str_replace('&Acirc;','',$td));
    						}


    		}

    		return $racesArray;

    }
}
