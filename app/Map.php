<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\HouseStatus;

class Map extends Model
{

    // Get maps
    /* param */
    /* x = base or x dimenson - auto
     * y = heigth or y dimenson - auto
     * zoom = zoom (zoom of map 1 - 6)
     * floor = floor of map (7 = ground)
     * q = actual size of map based in zoom
     */
    
    
    static public function saveMap($floor = 0, $zoom = 1) {

            $q = pow(2,$zoom);
     
            for ($x=0;$x<$q;$x++) {

                for($y=0;$y<$q;$y++) {

                    $url = 'http://www.tibiawiki.com.br/tiles/'.$floor.'/'.$zoom.'/'.$x.'/'.$y.'.png';
                    $output = 'map/tiles/'.$floor.'/'.$zoom.'/'.$x.'/'.$y.'.png';

                    if (!is_dir('map/tiles')) {
                        mkdir('map/tiles');
                    }
                   
                    if (!is_dir('map/tiles/'.$floor)) {
                        mkdir('map/tiles/'.$floor);
                    }

                    if (!is_dir('map/tiles/'.$floor.'/'.$zoom)) {
                        mkdir('map/tiles/'.$floor.'/'.$zoom);
                    }

                    if (!is_dir('map/tiles/'.$floor.'/'.$zoom.'/'.$x)) {
                        mkdir('map/tiles/'.$floor.'/'.$zoom.'/'.$x);
                    }
                    
                    file_put_contents($output, file_get_contents($url)); 
                }

            }
            
            return true;
        }
    
        
}
