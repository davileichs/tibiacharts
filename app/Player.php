<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\PlayersLog;

class Player extends Model
{
    
    public $dateStart = '';
    public $dateEnd = '';
    
    
    public function world() 
    {
        return $this->belongsTo(World::class, 'world_id');
    }
    
    
    
    public function town() 
    {
        return $this->belongsTo(Town::class);
    }
    
    
    
    public function users() 
    {
        return $this->belongsToMany(User::class, 'user_players');
    }
    
    
    
    public function log()
    {
        return $this->hasOne(PlayersLog::class,'idplayer')->orderBy('date', 'desc');
    }
    
    
    
    public function logs()
    {
        return $this->hasMany(PlayersLog::class,'idplayer')->orderBy('date', 'desc');
    }
    
    
    
    public function loots()
    {
        return $this->hasMany(logsSystem::class)->orderBy('date', 'desc');
    }
    
    
    
    public function levels()
    {
        return $this->hasMany(LevelsUp::class)->orderBy('level', 'asc');
    }
    
    
    
    public function chartWeek()
    {
        $this->dateStart = '2015-11-07 00:00';
        $this->dateEnd = '2015-11-13 23:59';
        
        $logs = PlayersLog::select('date')
                ->where('idplayer',$this->id)
                ->where('date', '>=', $this->dateStart)
                ->where('date', '<=', $this->dateEnd)
                ->get();

        $arrayPlayer = array();
        
        $count = count($logs);
  
        for ($i=0;$i<$count;$i++) {
            
            if ($i==0 || ($logs[($i-1)]->date->format('Y-m-d') <> $logs[$i]->date->format('Y-m-d'))) {
                $start = true;
            }
                
                if ($start == true) {
                    $timeStart = $logs[$i]->date;
                    
                    $strPlayer = array();
                    
                    $strPlayer['date'] = $timeStart->format('d/m/Y');
                    $strPlayer['name'] = date('l',strtotime($timeStart));
                    $strPlayer['dateStart'] = datePHPtoJS($timeStart);
                }
                
                
            if (($i+1)<$count) {    
                $hourStart = $logs[$i]->date->format('H:i');
                $hourEnd = $logs[($i+1)]->date->format('H:i');
                $diff = diff_time($hourStart, $hourEnd);
            }
                if (!empty($diff) && $diff['hour'] == 0 && $diff['minute'] <= 30) {
                    $start = false;
                } else {
                    $timeEnd = $logs[$i]->date;
                    $strPlayer['dateEnd'] = datePhpToJs($timeEnd);
                    array_push($arrayPlayer, $strPlayer);
                    $start = true;
                }

        }
    
        return ($arrayPlayer);
    }
    
    
    public function chartYear()
    {
        $this->dateStart = '2015-01-01 00:00';
        $this->dateEnd = '2015-12-31 23:59';
        
        return  PlayersLog::select(DB::raw('count(*)*15 as minutes, date'))
                ->where('idplayer',$this->id)
                ->where('date', '>=', $this->dateStart)
                ->where('date', '<=', $this->dateEnd)
                ->groupBy(DB::raw('DATE_FORMAT(date, "%Y-%m-%d")'))
                ->orderBy('date','ASC')
                ->get();

    }
    
    
    
    public function getNameUrlAttribute() 
    {
        return str_replace("&nbsp;","%20",htmlentities($this->attributes['name'], ENT_QUOTES, "UTF-8"));
    }
    


    public function getColorAttribute() 
    {
        // check if online
        return 'green';
        
    }
    
    
}
