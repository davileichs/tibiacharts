<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Races_kill extends Model
{

        public $timestamps = false;
        public $table = 'races_kills';
        protected $fillable = ['race_id', 'kill', 'killed', 'world_id', 'date'];
        public static $url = 'Lista_de_Criaturas';
        public static $urlBase = 'http://www.tibiawiki.com.br/wiki/';



        public function race() {

            return $this->belongsTo(Race::class, 'race_id');
        }



        function getDayAttribute() {

            $date = explode('-', $this->attributes['date']);


            return $date[2].'/'.$date[1];

        }



        static public function getKillStatisticContent($post)
        {

        		$tables = getData(self::$urlBase.self::$url, $post);

        		$killsArray = array();

        		foreach($tables['tr'] as $tr) {

        			if (stristr($tr->nodeValue,'World')==false) {

        				if (stristr($tr->nodeValue,'Last')==false) {

        					if (stristr($tr->nodeValue,'Race')==false) {

        						$td = preg_split('/&nbsp;/', htmlentities($tr->nodeValue), 0, PREG_SPLIT_NO_EMPTY);

        						if (!empty($td)) {

        							if (stristr($td[0],'Total')==true) {
        								// The last data from Table (Total) has a error that gets Total with the first number in same position
        								$td[0] = str_replace('Total','',$td[0]);
        								array_unshift($td,'Total');
        							}

        							array_push($killsArray,str_replace('&Acirc;','',$td));
        						}

        					}

        				}

        			}

        		}

        		return $killsArray;

        }





}
