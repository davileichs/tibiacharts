<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hunt extends Model
{

    protected $fillable = ['name', 'coord', 'floor', 'level', 'best_for', 'town_id', 'size'];
    public $timestamps = false;
    public static $url = 'Lista_de_Criaturas';
    public static $urlBase = 'http://www.tibiawiki.com.br/wiki/';



    public function town() {

        return $this->belongsTo(Town::class);
    }



    public function places() {

        return $this->hasMany(Hunts_Place::class);
    }



    function getUrlNameAttribute() {

        $name = $this->attributes['name'];
        $url = urlencode($name);
        $url = str_replace('+','_',$url);

        return $url;

    }

    

     /* script to capture npcs from tibiawiki */
    public static function getHuntContent()
    {

        $raceArray = array();
        $arrayTd = array();

        $tables = getData(self::$urlBase.self::$url,'table tbody');

        $line = 1;
        $start = false;
        foreach($tables['tr>td'] as $tr) {

            $td = $tr->nodeValue;

            $tdx = preg_replace('/\s+/', ' ', $td);

            if ($tdx == 'Nome') {
                $start = true;
                $line = 1;
            }

            if ($start == true) {

                $arrayTd[$line] = $tdx;

                if ($line=='6') {
                    array_push($raceArray, $arrayTd);

                    $line = 1;
                    $arrayTd = array();
                } else {
                    $line++;
                }
            }

         }

        return $raceArray;

    }
}
