<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain' => 'cron.tibiacharts.com'], function()
{
    Route::get('/map', 'CronController@cronMap');
    Route::get('/houses', 'CronController@HouseCronJob');
});

Route::group(['middleware' => 'web', 'prefix' => LaravelLocalization::setLocale()], function () {

    Route::get('setCookie', 'Controller@setCookie');
    Route::get('getCookie/{name}', 'Controller@getCookie');

    Route::get('/', 'HomeController@index');

    Route::get('/home', 'HomeController@index');

//    Route::get('/characters', 'PlayerController@indexPlayers');
//    Route::post('/characters', 'PlayerController@indexPlayers');
//
    Route::get('/logs', 'LogsSystemController@index');
    Route::post('/logs', 'LogsSystemController@store');
    Route::get('/log/{code}', 'LogsSystemController@indexLog');
//
//    Route::get('/events', 'EventController@index');

    Route::match(['get','post'], '/maps', 'HouseController@index');

    Route::match(['get','post'], '/houses', 'HouseController@index');
    Route::get('/getHouse', 'HouseController@getHouse');
    Route::get('/alertHouse', 'HouseController@alertHouse');
    Route::get('/listHouses', 'HouseController@listHouses');

    Route::match(['get', 'post'], '/npcs', 'NpcController@index');
    Route::get('/getNpc', 'NpcController@getNpc');
    Route::get('/listNpcs', 'NpcController@listNpcs');

    Route::match(['get','post'], '/hunts', 'HuntController@index');
    Route::get('/getHunt', 'HuntController@getHunt');
    Route::get('/listHunts', 'HuntController@listHunts');

    Route::match(['get','post'], '/races', 'RaceController@index');
    Route::match(['get','post'], '/bosses', 'RaceController@bosses');
    Route::get('/getRace', 'RaceController@getRace');
    Route::get('/listRaces', 'RaceController@listRaces');
    Route::get('/getKills', 'Races_killController@getKills');




    /* Route for Cronjobs */
    Route::group(['prefix'=>'cron'], function(){
        Route::get('/houses', 'HouseStatusController@cronHouses');
        Route::get('/auctionedHouses', 'HouseStatusController@cronHousesAuctioned');
        Route::get('/worlds', 'WorldController@cronWorld');
        Route::get('/npcs/{url?}', 'NpcController@cronNpc');
        Route::get('/races', 'RaceController@cronRacesWiki');
        Route::get('/normalraces', 'RaceController@getRacesContent');
        Route::get('/killstatistics', 'Races_killController@cronKillStatistics');
        Route::get('/itens/{url?}', 'ItenController@cronItens');

    });

    /* Route for ADMIN */
    Route::group(['prefix'=>'admin', 'middleware' => 'admin'], function(){

        Route::get('', 'UserController@indexAdmin');

        Route::get('scripts/{tipo?}', 'ScriptsController@index');

        Route::get('crop', 'CropImageController@index');
        Route::post('crop', 'CropImageController@crop');

        Route::get('users', 'UserController@indexAdmin');
        Route::put('users', 'UserController@store');
        Route::patch('users', 'UserController@update');
        Route::delete('users', 'UserController@delete');

        Route::get('home', 'UserController@indexAdmin');

        Route::get('npcs', 'NpcController@indexAdmin');
        Route::get('getDataNpc', 'NpcController@getDataNpc');
        Route::put('npcs', 'NpcController@store');
        Route::patch('npcs', 'NpcController@update');


        Route::get('professions', 'ProfessionController@indexAdmin');
        Route::put('professions', 'ProfessionController@store');
        Route::patch('professions', 'ProfessionController@update');
        Route::delete('professions', 'ProfessionController@delete');
        Route::get('getDataProfession', 'NpcController@getDataProfession');


        Route::get('houses', 'HouseController@indexAdmin');
        Route::patch('houses', 'HouseController@update');

        Route::get('races', 'RaceController@indexAdmin');
        Route::put('races', 'RaceController@store');
        Route::patch('races', 'RaceController@update');
        Route::delete('races', 'RaceController@delete');

        Route::get('getRacesMap', 'Race_mapController@getRacesMap');
        Route::get('getRaceMap', 'Race_mapController@getRaceMap');

        Route::put('races_map', 'Race_mapController@store');
        Route::patch('races_map', 'Race_mapController@update');
        Route::delete('races_map', 'Race_mapController@delete');


        Route::get('hunts', 'HuntController@indexAdmin');
        Route::put('hunts', 'HuntController@store');
        Route::patch('hunts', 'HuntController@update');
        Route::delete('hunts', 'HuntController@delete');

        Route::get('getPlaces', 'Hunts_PlaceController@getPlaces');
        Route::get('getPlace', 'Hunts_PlaceController@getPlace');
        Route::get('getRaces', 'Hunts_PlaceController@getRaces');

        Route::put('places', 'Hunts_PlaceController@store');
        Route::patch('places', 'Hunts_PlaceController@update');
        Route::delete('places', 'Hunts_PlaceController@delete');

        Route::put('place_race', 'Place_RaceController@store');
        Route::delete('place_race', 'Place_RaceController@delete');

        Route::get('events', 'EventController@indexAdmin');
        Route::put('events', 'EventController@store');
        Route::patch('events', 'EventController@update');
        Route::delete('events', 'EventController@delete');

        Route::get('/map', 'MapController@indexAdmin');
        Route::post('/map', 'MapController@generate');

    });

    Route::auth();

});
