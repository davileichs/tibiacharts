<?php


    /* function to get the name of image based on name
    *
    * @param
    * arrayImg => a list of all images from a folder
    * compare =>  string of the name to search for
    * sensitivity => level of levenshtein
    */
    function imgMatch($arrayImg, $compare, $sensitivity = 5)
    {
        $shortest = -1;
        foreach ($arrayImg as $img) {
            $lev = levenshtein(strtolower($compare), strtolower($img));
            if ($lev == 0) {
                $closest = $img;
                $shortest = 0;
            break;
        }
        if ($lev <= $shortest || $shortest < 0) {
            $closest = $img;
            $shortest = $lev;
        }

        }
        if($shortest <= $sensitivity){
            return $closest;
        } else {
            return 0;
        }

    }



    /* function to get all images in array of folder
     *
     * @param
     * $dir => folder inside images
     */

    function getImgs($dir = '')
    {

        $files = scandir('images/'.$dir);

        $k = array_search('.',$files);
        $i = array_search('..',$files);
        unset($files[$k]);
        unset($files[$i]);

        $files = array_map('removeExtension', $files);

        return $files;
    }




    function removeExtension($item)
    {
        return str_replace('.gif', '', $item);
    }



    /* function to not parse to Uppercase determineds kinds of words
     *
     *
     */
    function titleCase($stringRaw, $delimiters = array(" ", "-", "."), $exceptions = array("a", "in", "of", "on"))
    {

        $string = mb_convert_case($stringRaw, MB_CASE_TITLE, "UTF-8");
        foreach ($delimiters as $dlnr => $delimiter) {
            $words = explode($delimiter, $string);
            $newwords = array();
            foreach ($words as $wordnr => $word) {
                if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
                    // check exceptions list for any words that should be in upper case
                    $word = mb_strtoupper($word, "UTF-8");
                } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
                    // check exceptions list for any words that should be in upper case
                    $word = mb_strtolower($word, "UTF-8");
                } elseif (!in_array($word, $exceptions)) {
                    // convert to uppercase (non-utf8 only)
                    $word = ucfirst($word);
                }
                array_push($newwords, $word);
            }
            $string = join($delimiter, $newwords);
       }
       return $string;
    }


    /* just random color */
    function randColor($num = null)
    {

        $color = array(
            '#4682B4',
            '#20B2AA',
            '#3CB371',
            '#6B8E23',
            '#B8860B',
            '#D2691E',
            '#9370DB',
            '#FFB6C1',
            '#FF7F50',
            '#FFA500',
        );

        if ($num) {
            return $color[$num];
        }

        return $color[mt_rand(0,9)];
    }


    function getButtonColor($old = null, $input = null)
    {
        if ($old != null && $old == $input) {
            print 'success';
        } else {
            print 'default';
        }
    }



    function datePHPtoJS($dateFull, $type = 'day')
    {
        $year = date('Y', strtotime($dateFull));

        $month = date('m', strtotime($dateFull));

        $day = date('d', strtotime($dateFull));

        $hour = date('H', strtotime($dateFull));

        $minute = date('i', strtotime($dateFull));

        if ($type=='day') {
            return 'new Date(0,0,0,'.$hour.','.$minute.',0)';
        }
        if ($type=='year') {
            return 'new Date('.$year.', '.($month-1).', '.$day.')';
        }

    }



    function diff_time($hourStart, $hourEnd) {
        $i = 1;
        $timeTotal;

        $times = array($hourEnd, $hourStart);

        foreach($times as $time) {
            $seconds = 0;

            list($h, $m) = explode(':', $time);

            $seconds += $h * 3600;
            $seconds += $m * 60;

            $timeTotal[$i] = $seconds;

            $i++;
        }
        // if lower that's means it's after 00:00
        if ($timeTotal[1] < $timeTotal[2])
        {
            $timeTotal[1] += 86400;
        }
        $seconds = $timeTotal[1] - $timeTotal[2];

        $hours = floor($seconds / 3600);
        $seconds -= $hours * 3600;
        $minutes = str_pad((floor($seconds / 60)), 2, '0', STR_PAD_LEFT);

        return array('hour'=>$hours, 'minute'=>$minutes);
    }



    function timeLeftToServerSave($serverSave = 10)
    {
        date_default_timezone_set('Europe/Berlin');
        $now = intval(date('H'));
        if ($serverSave > $now) {
            $diff = $serverSave - $now;
        } else {
            $diff = $serverSave+24 - $now;
        }

        return ($diff-1);

    }


    /* script to capture html from site to PHP */

    function getData($url, $post='', $pq = 'table')
    {

        require_once 'plugins/phpquery-master/phpQuery/phpQuery.php';

        $html = getCurl($url, $post);

        phpQuery::newDocument($html, $contentType = null);

        $tables = pq($pq);

        return $tables;

    }


    /* script to capture sites from CURL */

    function getCurl($url=null, $post=null)
    {

            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
            curl_setopt($ch,CURLOPT_POSTFIELDS,$post);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
            curl_setopt($ch,CURLOPT_TIMEOUT, 20);
            $response = curl_exec($ch);
            curl_close ($ch);

            return $response;

    }



    function recursiveRemove($dir)
    {
        $structure = glob(rtrim($dir, "/").'/*');
        if (is_array($structure)) {
            foreach($structure as $file) {
                if (is_dir($file)) recursiveRemove($file);
                elseif (is_file($file)) unlink($file);
            }
        }
        rmdir($dir);
    }



    // calculate if a Point is in Circle
    // $p = position of a point to calculate (x, y)
    // $c = central position of the circle (x, y)
    // $r = radius
    function inCircle($p = array(), $c = array(), $r)
    {
      $Px = $p[0];
      $Px = $p[1];
      $Cx = $c[0];
      $Cx = $c[1];

      $d = sqrt(pow($Px - $Cx) + pow($Py - $Cy));

      if ($d <= $r) {
        return true;
      }

      return false;
    }
