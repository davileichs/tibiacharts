<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Player;


class PlayerController extends Controller
{
    
    
    
    public function indexPlayers(Request $request)
    {
        
        $user = Auth::user();
        $chars = $user->players;
                
        if (!empty($request->input('id'))) {
            $player = Player::find($request->input('id'));
 
        }
        
        return view('characters', compact('chars', 'player', 'request'));
    }
    
    
    
    public function indexLogs(Request $request, $code = null)
    {
        
        $user = Auth::user();
        $chars = $user->players;
                
        if (!empty($request->input('id'))) {
            $player = Player::find($request->input('id'));
        }
        
        return view('logs', compact('chars', 'player', 'request'));
    }
}
