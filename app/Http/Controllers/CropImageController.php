<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Http\Requests;


class CropImageController extends Controller
{
    
    
    public function index() {
        
        
        return view('admin.pages.crop');
    }
    
    
   
    public function crop(Request $request)
    {
        $directoryFrom = 'images/places/source/'.$request->locationFrom;
        $directoryTo = 'images/places/location/'.$request->locationTo;
        $name = $request->name;
        
        $imgSize_x = $request->xSize ? $request->xSize : 566;
        $imgSize_y = $request->ySize ? $request->ySize : 416;
        $imgStart_x = $request->xStart ? $request->xStart : 272;
        $imgStart_y = $request->yStart ? $request->yStart : 100;
        
        $markSize_x = 108;
        $markSize_y = 108;
        $markStart_x = 1184;
        $markStart_y = 125;
        
        $p = 0.7;
 
        $files = \File::allFiles($directoryFrom);
        
        if (!is_dir($directoryTo)) {
            mkdir($directoryTo);
        }
       
        foreach ($files as $k=>$file){
       
                $img = Image::make($file);
                $mark = Image::make($file);
                
                
                $img->crop($imgSize_x, $imgSize_y, $imgStart_x, $imgStart_y);
                $mark->crop($markSize_x,$markSize_y,$markStart_x,$markStart_y);
                
                $img->resize(($imgSize_x*$p),($imgSize_y*$p));
                $mark->resize(($markSize_x*$p), ($markSize_y*$p));
                $newFile = $directoryTo.'/'.$name.'_'.$k.'.png';
                
                $img->insert($mark, 'bottom-right', 1, 1);
                $img->save($newFile);

        } 

    
        return view('admin.pages.crop');
    }
    
    
    
 
}
