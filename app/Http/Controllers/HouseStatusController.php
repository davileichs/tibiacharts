<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\House;
use App\World;
use App\Town;
use App\HouseStatus;

class HouseStatusController extends Controller
{
    
    public $timeout = 60;
    
    
    
    public function cronHouses() {
        
        $worlds = World::get();
        
        $towns = Town::where('type',1)->get();
       
        $this->captureHouseStatus($worlds, $towns);
       
    }
    
    
    
    public function cronHousesAuctioned()
    {
       
        $this->captureHouseAuctioned();
        
    }
    
    
    
    
    public function captureHouseStatus($worlds = 'all', $towns = 'all')
    {
        $housesArray = array();

        $start = time();
        
        foreach ($worlds as $world) {

            // if table is empty, should permit new inserts
            $emptyWorld = HouseStatus::where('world_id', $world->id)->exists();
            
            $houses = HouseStatus::where('world_id', $world->id)->where('last_update', '<', date('Y-m-d'))->count();
            $totalHouses = HouseStatus::where('world_id', $world->id)->count();
            
            print ' <br />check :'.$world->name . " <br />";
            /* se houver status do dia anterior para o World atual, faz o update */
            if ($houses > 0 || $emptyWorld === false || $totalHouses != 1042) {
            
                print " update world ".$world->name . " <br />";
                foreach ($towns as $town) {

                    print "check t:".$town->name." <br />";
                    
                    $emptyTown = HouseStatus::where('world_id', $world->id)->where('town_id', $town->id)->exists();
                    
                    $status = HouseStatus::where('world_id', $world->id)->where('town_id', $town->id)->where('last_update', '<', date('Y-m-d'))->count();
                  
                    /* se houver status do dia anterior para a Town atual, faz o update */
                    if ($status > 0 || $emptyTown === false || $totalHouses != 1042) {

                        print "update town " . $town->name." <br />";
                        
                        $housesArray = HouseStatus::getHousesContent($world->name, $town->name);

                        if (count($housesArray) > 0 ) {
                            
                            $this->saveStatus($housesArray, $world->id, $town->id);
                        } else {
			    print "content empty <br />";
			}
                    }
                    
                }
                // stop before 300s (timeout of cronjob)

                if(time()-$start > $this->timeout) { 
                    exit();
                }

            }

        }

    }
    
    
    
    public function captureHouseAuctioned()
    {
        
        $start = time();
                      
        $status = HouseStatus::where('status', 'LIKE', '%hour%')->get();

        /* se houver status do dia com menos de um dia para 'rent' faz o update */
        if ($status) {

            foreach ($status as $value) {
              
                print "update house " . $value->house->name." | ";
                print "world " . $value->world->name." | ";
                print "town " . $value->town->name." <br />";

                $houseArray = HouseStatus::getHouseContent($value->world->name, $value->town->name, $value->house->idtibia);
                // should return array 'time', 'gold', 'player'
                
                if (count($houseArray) > 0 ) {
                    $timeLeft = timeLeftToServerSave();
                    $this->updateStatus($value->id, $houseArray['gold'], $timeLeft);
                } else {
                    print "content empty <br />";
                }


            }
            // stop before 300s (timeout of cronjob)

            if(time()-$start > $this->timeout) { 
                exit();
            }

        }

    }
    
    
    
    private function saveStatus($fullData, $world_id, $town_id)
    {
 
        foreach ($fullData as $data) {
            $house_name = str_replace('&Acirc;','', utf8_encode($data[0]));
            $house = House::where('name', $house_name)->first();
            
            print "update - w:".$world_id." | t:" .$town_id . " | h:" . $house->id . " <br />";
            $this->delete(array(
                'world_id' => $world_id,
                'town_id' => $town_id,
                'house_id' => $house->id,
            ));

            $this->store(array(
                'world_id' => $world_id,
                'town_id' => $town_id,
                'house_id' => $house->id,
                'status'=> utf8_encode($data[3]),
            ));
        }
    
    }
    
    
    
    private function updateStatus($id, $gold, $timeLeft)
    {
        $status = HouseStatus::where('id', $id);
        
        if ($gold == '') {
            $gold = 0;
        }
        
        $gold_text = $gold . ' gold';
        if ($gold <= 1) {
            $gold_text .= 's';
        } 
        
        $hour_text = $timeLeft . ' hour';
        if ($timeLeft > 1) {
            $hour_text .= 's';
        }
        
        $text = 'auctioned ('.$gold_text.'; '.$hour_text.' left)';
        
        $status->update(['status'=>$text]);
        
    }
        
    

    public function store($data) 
    {

        $status = new HouseStatus;
        
        $status->world_id = $data['world_id'];
        $status->town_id = $data['town_id'];
        $status->house_id = $data['house_id'];
        $status->status = $data['status'];
        $status->last_update = date('Y-m-d');
        
        $status->save();

    }
    
    

    public function delete($data)
    {
        $status = HouseStatus::where('world_id', $data['world_id'])->where('town_id', $data['town_id'])->where('house_id', $data['house_id']);
        $status->delete();
  
    }
}
