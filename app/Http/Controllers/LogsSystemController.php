<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\LogsSystem;
use App\Iten;
use App\Race;
use App\Races_loot;

class LogsSystemController extends Controller
{
    
     public $dates = ['date'];
    public $loots = array();
    public $experience = array();
    public $heal = array();
    public $attack = array();
    public $expByTime = array();
    public $defense = array();
    public $time = array();
    public $itens = array();
    
    public $arrayImgItens = array();
    public $arrayImgRaces = array();
    
    public $code = '';
    public $profit = 0;
    public $waste = 0;
    public $minGoldCoins = 0;
    public $minValueItens = 0;
    public $onlyPlatinum = 0;
    public $munition = 0;
    public $qtdMunition = 0;
    public $getMunition = 0;
    
    
    
    public function index($code = null)
    {
       
        return view('logs');
        
    }
    
    
    
    public function indexLog($code = null)
    {
        
        $logs = LogsSystem::where('code','LIKE',$code)->get();

        
        return view('log', compact('code','logs'));
        
    }
    
    
    
    public function store(Request $request)
    {
        
            $log = $this->exportLog($request);
       
            $logStore = new LogsSystem;
          
            $logStore->fill($log);
            
            $logStore->save();
        
            foreach ($log['loots'] as $value) {
                
                $value['logs_system_id'] = $logStore->id;
                
                Races_lootController::store($value);               
            
            }
            
            
            return redirect('log/'.$log['code']);
        
    }
    
    
    
    public function exportLog(Request $request)
    {
        
        if (!empty($request->logtext)) {

            $file = $request->logtext;

        } else if ($request->file('log')->isValid()) {

            $file = file_get_contents($request->file('log')->getPathName());
        }
        
        if (!empty($file)) {
            
            if (!empty($request->range_goldcoins)) {
                $this->minGoldCoins = $request->range_goldcoins;
            }
            if (!empty($request->range_itens)) {
                $this->minValueItens = $request->range_itens;
            }
            if (!empty($request->onlyPlatinum)) {
                $this->onlyPlatinum = $request->onlyPlatinum;
            }
            if (!empty($request->munition)) {
                $this->munition = $request->munition;
            }
            if (!empty($request->qtdMunition)) {
                $this->qtdMunition = $request->qtdMunition;
            }
            if (!empty($request->getMunition)) {
                $this->getMunition = $request->getMunition;
            }
            
            $this->arrayImgItens = getImgs('itens');
            $this->arrayImgRaces = getImgs('creatures');
             
            $this->exportDropRaces($file);
            
            $this->exportExperience($file);
            
            $this->exportHeals($file);
            
            $this->exportAttacks($file);
            
            $this->exportDefenses($file);
            
            $this->exportTimeHunt($file);
            
            $this->exportItensUsed($file);
           
            
            $hunt = $this->experience['total'].','.$this->time['total'].','.$this->experience['perhour'];
            $hits = $this->attack['total'].','.$this->defense['total'].','.$this->attack['max'].','.$this->attack['dps'];
            $heals = $this->heal['total'].','.$this->heal['max'].','.$this->heal['dps'];
            $exp = json_encode($this->expByTime);
        
            $itensUsed = $this->itens['list'];

            $dateLog = $this->time['start'];

            $code = uniqid('chart');
            
            
            $log['name'] = $request->name;
            $log['hunt'] = $hunt;
            $log['hits'] = $hits;
            $log['exp'] = $exp;
            $log['heal'] = $heals;
            $log['itens_used'] = $itensUsed;
            $log['date'] = $dateLog;
            $log['code'] = $code;
            $log['profit'] = $this->profit;
            $log['waste'] = $this->waste;
            
            $log['loots'] = $this->loots;
            
            return $log;
        }
        
    }
    
    
    
    public function exportDropRaces($log = '') 
    {
        //Loot of a {race}: {loot}, {loot}, [...]
        $races = array();
        $loots = array();
        $log = nl2br($log);
        $list = explode('<br />',$log);

        foreach ($list as $l) {

            preg_match("/Loot of (an|a) +(?<race>[[a-zA-Z '-]+): (?P<loot>.*)/", $l, $matches);
            if ($matches){
                array_push($races, $matches['race']);
                // cria um id para salvar cada loot de acordo com a race
                $id = $matches['race'];

                if (empty($loots[$id])) {
                    $loots[$id] = '';
                }

                $loots[$id] .= ','.$matches['loot'];
            }
        }

        $drops = array('races'=>$races,'loots'=>$loots);

        $this->sumDrop($drops);

        return $drops;
    }
    
    
    
    public function sumDrop($drops) 
    {
        
        $races = $drops['races'];
        $racesCount = array_count_values($races);
        $racesLoot = array();

        $loots = $drops['loots'];
        
        foreach ($loots as $race=>$loot) {
            
            $arrayLoot = explode(',',$loot);
            $aux = array();
            $raceItens = $this->organizeLoot($arrayLoot);
            
            foreach ($raceItens as $key=>$v) {
                
                if ($key!="") {
                    
                    if ($key!='gold coin' && $key!='platinum coin' && $key!='crystal coin') {
                        
                       $itens = Iten::whereRaw('(name LIKE "'.$key.'" OR name LIKE "'.$key.'e" OR name LIKE "'.$key.'es" OR name LIKE "'.$key.'s" OR name LIKE "'.$key.'ies")');
                       
//                       if ($valuables['buy'] >= $this->minValueItens) {
//                           
//                        $this->profit += $valuables['buy']*intval($v['amount']);
//                        
//                       }
                    }
                }
            }

            $racesLoot[$race] = $raceItens;
            $racesLoot[$race]['killed'] = $racesCount[$race];
        }
        
        
        $arrayReturn = array();
        
        foreach ($racesLoot as $race=>$loots) {
            
            $lootString = '';
            
            foreach ($loots as $item=>$loot) {
                
                if ($item!='killed') {
                    $lootString .= $loot['quantum'].$item.'('.$loot['amount'].'),';
                }
            }
            
            $race = Race::where('name','like',$race)->first();
            
            if ($race) {
                array_push($arrayReturn, array('race_id'=>$race->id,'killed'=>$loots['killed'],'loot'=>rtrim($lootString,',')));
            }
        }
        
        $this->loots = $arrayReturn;
        
    }
    
    
    
    public function organizeLoot($arrayLoot = array()) {
        $auxArray = array();
        $count = array();
        $amount = 0;
        
        foreach ($arrayLoot as $loot) {
            
            preg_match("/((?P<amount>(\d+|an|a))*\\s)*(?P<item>.*)/", $loot, $match);
            
            if ($match) {
                
                if (strcmp($match['amount'], "an") == 0 || strcmp($match['amount'], "a") == 0 || empty($match['amount'])) {
                      $match['amount'] = 1;
                } 

                $id = $match['item'];
                $amount = $match['amount'];
                
                if (!empty($auxArray[$id])) {
                    $amount = $auxArray[$id]['amount']+$amount;
                }
                if (empty($count[$id])) {
                    $count[$id] = 1;
                }
                
                $count[$id]++;
                $auxArray[$id] = array('quantum'=>$count[$id],'amount'=>$amount);
                
                // procura apenas coins por montante
                if ($id=='gold coin' || $id=='gold coins') {
                    
                    if ($match['amount'] >= $this->minGoldCoins && $this->onlyPlatinum == 0) {
                        $this->profit += intval($match['amount']);
                    }
                    
                } 
                else if ($id=='platinum coin' || $id=='platinum coins') {
                    $this->profit += intval($match['amount']*100);
                } 
                else if ($id=='crystal coin' || $id=='crystal coins') {
                    $this->profit += intval($match['amount']*10000);
                } 
               
                
            }
        }
        // elimina os itens em plural e joga para o singular
        foreach ($auxArray as $key=>$v) {

            if ($v['amount']>1) {
                // para plural terminado em ies => y
                preg_match('/(?P<item>(.*))ies$/', $key, $match);
                $auxArray = $this->removePlural($auxArray, $key, $match, 'y');
                // para plural terminado em es
                preg_match('/(?P<item>^(?!.*ies).*)es$/', $key, $match);
                $auxArray = $this->removePlural($auxArray, $key, $match);
                // para plural terminado em s
                preg_match('/(?P<item>^(?!.*ies)^(?!.*es).*)s$/', $key, $match);
                $auxArray = $this->removePlural($auxArray, $key, $match);
            }
            
        }
       
        return $auxArray;
    }
    
    
    
    private function removePlural($auxArray, $key, $match, $modify = '') {
        if (!empty($match['item'])) {
            $newId = $match['item'].$modify;

            if (array_key_exists($newId, $auxArray)) {
                $auxArray[$key]['amount'] += $auxArray[$newId]['amount'];
            }
            if (empty($auxArray[$newId]['quantum'])) {
                $auxArray[$newId]['quantum'] = 0;
            }
            $auxArray[$newId]['amount'] = $auxArray[$key]['amount'];
            $auxArray[$newId]['quantum'] += $auxArray[$key]['quantum'];
            unset($auxArray[$key]);
        }
                
        return $auxArray;
    }
    
    
    
    public function exportExperience($log = '') {
        //10:21 You gained 360 experience points.
        $experiences = array();
        $times = array();
        
        $log = nl2br($log);
        $list = explode('<br />',$log);
        
        foreach ($list as $string) {
            
            preg_match('/(?<time>\d+:\d+):* You gained +(?<exp>\d+) experience points/', $string, $matches);
            
            if ($matches){
                
                array_push($experiences, $matches['exp']);
                $hour = $matches['time'];
                
                if (empty($times[$hour])) {
                    $times[$hour] = $matches['exp'];
                } else {
                    $times[$hour] += $matches['exp'];
                }
            }
            
        }

        $this->expByTime = array_values($times);
            
        $this->experience['total'] = $this->sumArray($experiences, 'k');
            
        return $experiences;
    }
    
    
    
    public function exportHeals($log = '') {
        //10:20 You healed yourself for 128 hitpoints.
        //12:55 You heal Zos Io Pan for 1233 hitpoints.
        $healsY = array();
        $healsO = array();
        
            preg_match_all('/You healed yourself for +(?<heal>\d+) hitpoints/', $log, $matchesYourself);
            preg_match_all('/You heal (?<other>[[:alpha:]+\s]+) for +(?<heal>\d+) hitpoints/', $log, $matchesOthers);
            
            if ($matchesYourself){
                $healsY = $matchesYourself['heal'];
            }
            
            if ($matchesOthers){
                $healsO = $matchesOthers['heal'];
            }
            
            $maxHeal = 0;
            $minHeal = 10000;
            foreach($healsY as $heal) {
                
                if ($heal > $maxHeal) {
                    $maxHeal = $heal;
                }
            }
            
            foreach($healsO as $heal) {
                
                if ($heal > $maxHeal) {
                    $maxHeal = $heal;
                }
            }
            
            foreach($healsY as $heal) {
                
                if ($heal < $minHeal) {
                    $minHeal = $heal;
                }
            }
            
            foreach($healsO as $heal) {
                
                if ($heal < $minHeal) {
                    $minHeal = $heal;
                }
            }
        
        $healsT = ($this->sumArray($healsY)+ $this->sumArray($healsO))/1000;
            
        $this->heal['totalYourself'] = $this->sumArray($healsY,'k');
        $this->heal['totalOther'] = $this->sumArray($healsO,'k');
        $this->heal['total'] = round($healsT).'k';
        $this->heal['max'] = $maxHeal;
        $this->heal['min'] = $minHeal;
        
        $this->heal['dps'] = ($healsO+$healsY) ? ($healsT*1000)/(count($healsO+$healsY)) : 0;
        #return $heals;
    }

    
    
    public function exportAttacks($log = '') {
        //10:21 A tarantula loses 225 hitpoints due to your attack.
        $allHits = array();
        $logs = explode('.',$log);
        
        foreach ($logs as $string) {
            
            preg_match("/A (?<race>[[a-zA-Z '-]+) loses (?<hit>\d+) hitpoints due to your attack/", $string, $matches);
            
            if (!empty($matches)) {
                array_push($allHits, $matches['hit']);                
                
                if (!empty($hits)) {
                    $hits = array();
                }
            }
            
        }
        
        //array(0=>array('mobA'=>10,'mobB'=>20),1=>array('mobA'=>12,'mobB'=>25));
        $maxHit = 0;
        $race = '';

        foreach($allHits as $k=>$hit) {
            
            if ($hit > $maxHit) {
                $maxHit = $hit;
            }
        }
            
        $this->attack['total'] = $this->sumArray($allHits,'k');
        $this->attack['max'] = $maxHit;
        $this->attack['hits'] = $allHits;
        $this->attack['dps'] = ($allHits) ? round($this->sumArray($allHits)/count($allHits)) : 0;

        return $allHits;
            
    }
    
    
        
    public function exportDefenses($log = '') {
        //10:21 You lose 46 hitpoints due to an attack by a giant spider.
        //10:22 You lose 6 hitpoints.
        //12:55 You lose 13 hitpoints due to your own attack.
        $hits = array();

            preg_match_all('/You lose (?<hit>\d+) hitpoints( due to an attack by (an|a) (?<race>[[:alpha:]+\s]+))*/', $log, $matches);
            if ($matches){
                $hits = $matches['hit'];
            }
       
            $maxHit = 0;
            $race = '';
            
            foreach($hits as $k=>$hit) {
                
                if ($hit > $maxHit) {
                    $maxHit = $hit;
                    #$race = $matches['race'][$k];
                }
            }
            
        $this->defense['total'] = $this->sumArray($hits,'k');
        $this->defense['max'] = $maxHit;
        $this->defense['dps'] = ($hits) ? round($this->sumArray($hits)/count($hits)) : 0;
        #$this->defense['race'] = $race;
        return $hits;
    }
    
    
    
    public function exportTimeHunt($log = '') {
        // Começa a partir do primeiro hit dado/sofrido
        // Termina depois do último hit dado/sofrid
        //10:21 A tarantula loses 225 hitpoints due to your attack.
        //10:21 You lose 46 hitpoints due to an attack by a giant spider.
        $times = array();
    
        preg_match_all('/(?<time>\d+:\d+):* ([\D+\s]+) loses?/', $log, $matches);
        if ($matches){
            $times = $matches['time'];   
        }

        $start = array_shift($times);
        $end = array_pop($times);

        
        //Channel Server Log saved at Fri Oct 02 09:14:32 2015
        preg_match('/Channel Server Log saved at (?<start>.*)/', $log, $match);
        
        if (!empty($match)) {
            $logTime =  date('Y-m-d H:i:s', strtotime($match['start']));
        } else {
            $logTime = date('Y-m-d '.$start.'');
        }

        $this->time['start'] = $logTime;
        
        $totalTime = diff_time($start, $end);
        
        $exp = $this->experience['total'];
        
        $minTotal = ($totalTime['hour']*60)+($totalTime['minute']);
        $media = $exp/$minTotal;
        $result = $media*60;
        
        $this->experience['perhour'] = round($result).'k';
        $this->time['total'] = $totalTime['hour'].' h'.$totalTime['minute'] .' min';
    }
    
    
    
    public function exportItensUsed($log = '') {
        
        //Using one of 79 strong mana potions...
        $itens = array();
        $listItens = array();
        
        preg_match_all('/Using one of (?<list>.*)/', $log, $matches);

        foreach ($matches['list'] as $list) {
            
            preg_match('/(?<amount>\d+) (?<item>[\D+\s]+)..../', $list, $match);
            $id = $match['item'];
            
            if (empty($itens[$id])) {
                $itens[$id] = array();
            }
            
            array_push($itens[$id], $match['amount']);
        }
        
        foreach ($itens as $item=>$values) {
            
            $i=1;
            foreach ($values as $k=>$value) {
                
                if ($k>0) {
                    
                    if ($value != $values[($k-1)]) {
                        $i++;
                    }
                }
            }
           
            #$valuables = Iten::whereRaw('v.name LIKE "'.substr($item,0,-2).'%"');
            #$this->waste += ($valuables['sell']*$i);
            
            array_push($listItens, $item.':'.$i);

        }
        
        if ($this->getMunition == 1) {
                $valuables = $valControl->getSqlValuables('v.idvaluable = '.$this->munition.'','sell');
                $this->waste += ($valuables['sell']*$this->qtdMunition);
                array_push($listItens, $valuables['name'].':'.$this->qtdMunition);
            }
            
        $this->itens['list'] = implode(',',$listItens);

    }
      
    
    
    public function lootToString() {
        
        $racesLoot = $this->loots;
        $arrayReturn = array();
        
        foreach ($racesLoot as $race=>$loots) {
            
            $lootString = '';
            
            foreach ($loots as $item=>$loot) {
                
                if ($item!='killed') {
                    $lootString .= $loot['quantum'].$item.'('.$loot['amount'].'),';
                }
            }
            
            array_push($arrayReturn, array('race'=>$race,'killed'=>$loots['killed'],'loot'=>rtrim($lootString,',')));
        }
        
        return $arrayReturn;
    }
    
    
    
    public function sumArray($number, $type='normal') {
        
        $total = array_sum($number);
        
        if ($type=='k') {
            if ($total/1000000 > 1) {
                return round($total/1000).'kk';
            }
            if ($total/1000 > 1) {
                return round($total/1000).'k';
            }
        }
        return $total;
    }
}
