<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Iten;

class ItenController extends Controller
{
    
    
    public function cronItens($url = '') {
        
        $urls = array(
            'Capacetes',
            'Armaduras',
            'Calças',
            'Botas',
            'Spellbooks',
            'Escudos',
            'Machados',
            'Rods',
            'Distância',
            'Munição',
            'Wands',
            'Clavas',
            'Espadas',
            'Antigas_Wands_e_Rods',
            'Livros',
            'Documentos_e_Papéis',
            'Instrumentos_Musicais',
            'Troféus',
            'Dolls_e_Bears',
            'Recipientes',
            'Prêmios_de_Eventos',
            'Decorações',
            'Itens_de_Fansites',
            'Runas_de_Decoração',
            'Comidas',
            'Líquidos',
            'Plantas_e_Ervas',
            'Produtos_de_Criaturas',
            'Amuletos_e_Colares',
            'Ferramentas_de_Cozinha',
            'Fontes_de_Luz',
            'Anéis',
            'Chaves',
            'Itens_de_Domar',
            'Ferramentas',
            'Itens_de_Addons',
            'Itens_de_Quest',
            'Valiosos',
            'Lixos',
            'Cristais_(Itens)',
            'Itens_Encantados',
            'Jogos_e_Diversão',
            'Itens_de_Festa',
            'Runas'
        );
        
        if (!empty($url)) {
            Iten::$url = $url;
        }
        
        foreach ($urls as $url) {
            Iten::$url = $url;
            $itens = Iten::getItensContent();

            foreach ($itens as $value) {

                $item = new Iten;
                $item->name = $value;

                $item->save();
            }
        }
       
    }
}
