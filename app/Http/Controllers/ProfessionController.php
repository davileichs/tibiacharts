<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
Use App\Profession;


class ProfessionController extends Controller
{
    
    
   

    public function indexAdmin()
    {
        $professions = Profession::paginate(15);


        return view('admin.pages.professions', compact('professions'));
    }

    
    
    public function getDataProfession(Request $request)
    {

        $profession = Profession::where('id', $request->id)->first();
        
        return $profession;
    }
    
    
    
    public function store(Request $request) 
    {

        $professionStore = new Profession;
        $professionStore->fill($request->all());
        
        $professionStore->save();
  
        return back();
    }
    
    
    
    public function update(Request $request) 
    {
        $professionUpdate = Profession::find($request->id);

        $professionUpdate->update($request->all());
  
        return back();
    }
    
   
    
    public function delete(Request $request)
    {
        $professionDelete = Profession::find($request->id);
        $professionDelete->delete();
        
        return back();
    }
    
   
}
