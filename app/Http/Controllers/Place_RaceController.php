<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Place_Race;

class Place_RaceController extends Controller
{


    public function store(Request $request)
    {

        $store = new Place_Race;
        $store->fill($request->all());


        $store->save();

       return $store->place_id;
    }



    public function delete(Request $request)
    {

        $delete = Place_Race::where('place_id', $request->place_id)->where('race_id', $request->race_id)->first();

        $delete->delete();

        return $delete->place_id;
    }

}
