<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Http\Requests;
use App\World;
use App\Town;
use App\HouseStatus;
use App\Map;

class MapController extends Controller
{
    //




    public function indexAdmin(Request $request)
    {
        $directory = public_path().'/map/fullmap';
        $files = \File::allFiles($directory);

        return view('admin.pages.maps', compact('request', 'files'));
    }



    public function generate(Request $request)
    {

        ini_set('max_execution_time', 600);

        $local_img = $request->file;
        $dir_map = public_path().'/map/';

        $tile = 'tiles2';
        if (!empty($request->tile)) {
            $tile = $request->tile;
        }

        if (!is_dir($dir_map.$tile)) {
            mkdir($dir_map.$tile);
        }

        if ($request->new) {
          recursiveRemove($dir_map.$tile.'/'.$request->level);
        }

        if (!is_dir($dir_map.$tile.'/'.$request->level)) {
            mkdir($dir_map.$tile.'/'.$request->level);
        }

        $x = 2048;
        $y = 2048;

        $z = 6;

        for ($zm=0;$zm<=$z;$zm++) {

            if (!is_dir($dir_map.$tile.'/'.$request->level.'/'.$zm)) {
                mkdir($dir_map.$tile.'/'.$request->level.'/'.$zm);
            }

            if ($zm > 0) {
                $zoom = pow(2,$zm);

                $width = round($x/$zoom);
                $height = round($y/$zoom);

            } else {
                $zoom = 0;

                if (!is_dir($dir_map.$tile.'/'.$request->level.'/'.$zm.'/0')) {
                    mkdir($dir_map.$tile.'/'.$request->level.'/'.$zm.'/0');
                }

                $width = round($x);
                $height = round($y);
                $file = $dir_map.$tile.'/'.$request->level.'/'.$zm.'/0/0.png';
                $img = Image::make($local_img);
                $img->resize(256,256);
                $img->save($file);

            }

            for ($w=0;$w<$zoom;$w++) {

                for ($h=0;$h<$zoom;$h++) {

                    $dir = $dir_map.$tile.'/'.$request->level.'/'.$zm;
                    $file = $dir.'/'.$w.'/'.$h.'.png';

                    if ($request->new || !is_file($file)) {


                        $invert = ($zoom-$h-1);

                        $xStart = ($w*$width);
                        $yStart = ($invert*$height);


                        $ySize = $height;
                        $xSize = $width;

                        $img = Image::make($local_img);
                        $img->crop($xSize, $ySize, $xStart, $yStart);

                        if (!is_dir($dir)) {
                          mkdir($dir);
                        }

                        if (!is_dir($dir.'/'.$w)) {
                          mkdir($dir.'/'.$w);
                        }


                        $img->resize(256,256);
                        $img->save($file);

                    }
                }

            }


        }


        return redirect()->action('MapController@indexAdmin');
    }

}
