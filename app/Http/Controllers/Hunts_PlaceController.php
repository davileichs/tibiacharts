<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Hunts_Place;
use App\Place_Race;
use App\Race;

class Hunts_PlaceController extends Controller
{
    
    
    
    public function getPlaces(Request $request)
    {

        $places = Hunts_Place::where('hunt_id', $request->id)->get();
        $races = Race::all();
   
        return view('admin.pages.ajax.places',compact('places','races'));
    }
    
    
    
    public function getPlace(Request $request)
    {

        $place = Hunts_Place::where('id', $request->id)->first();
        
        return $place;
    }
    
    
    
    public function getRaces(Request $request)
    {
        $races = Hunts_Place::find($request->place_id)->races;
        
        return view('admin.pages.ajax.races',compact('races'));
    }
    
    
    
    
    public function store(Request $request) 
    {

        $store = new Hunts_Place;
        $store->fill($request->all());
        
        $store->save();
  
       return $store->hunt_id;
    }
    
    
    
    public function update(Request $request) 
    {
        $update = Hunts_Place::find($request->id);

        $update->update($request->all());
  
        return $update->hunt_id;
    }
    
    

    public function delete(Request $request)
    {
       
        $delete = Hunts_Place::find($request->id);
        $delete->delete();
        
        return $delete->hunt_id;
    }
}
