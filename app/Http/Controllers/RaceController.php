<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Race;
use App\Hunts_Place;
use App\Races_kill;
use App\World;
use \App\Libraries\Inflect;

class RaceController extends Controller
{



    public function index(Request $request)
    {

        $races = Race::whereNotNull('coords')->get();
        $places = null;


        $places = Hunts_Place::whereHas('races',function($q) use ($request){
            $q->whereNotNull('coords');
        })->get();


        $race_selected = Race::where('id', $request->id)->first();

        return view('principal.pages.races', compact('races', 'race_selected', 'places' , 'request'));

    }




    public function bosses(Request $request)
    {

        $races = Race::whereNotNull('coords')->where('type', 1)->get();

        $places = Hunts_Place::whereHas('races',function($q) use ($request){
            $q->whereNotNull('coords');
        })->get();


        return view('principal.pages.races', compact('races', 'places', 'request'));

    }



    public function indexAdmin()
    {
        $races = Race::orderBy('name')->paginate(50);

        return view('admin.pages.races', compact('races'));
    }



    public function getRace(Request $request)
    {

        $race = Race::where('id', $request->id)->first();
        $kills = Races_kill::where('race_id', $request->id)->where('world_id', \Cookie::get('world_id'))->orderBy('date','desc')->limit(7)->get();

        return view('ajax.race',compact('race', 'kills'));
    }



    public function listRaces(Request $request)
    {

        $race = Race::where('id', $request->id)->first();

        $coords = $race->coords_obj;


        $places = Hunts_Place::whereHas('races',function($q) use ($request){
            $q->where('race_id', $request->id);
        })->get();

        foreach($coords as &$coord) {
          $coord->town = Controller::getClosestTown($coord->x,$coord->y);
        }

        return view('ajax.list_races',compact('race', 'coords', 'places'));

    }



    public function store(Request $request)
    {

        $raceStore = new Race;
        $raceStore->fill($request->all());

        $raceStore->save();

        return back();
    }



    public function update(Request $request)
    {
        $raceUpdate = Race::find($request->id);

        $raceUpdate->update($request->all());

        return back();
    }



    public function delete(Request $request)
    {
        $raceDelete = Race::find($request->id);
        $raceDelete->delete();

        return back();
    }



    public function cronRacesWiki() {

        // $array = Race::getRaceListContentWiki();

        // foreach ($array as $k=>$value) {
        //
        //     $race = new Race;
        //     $race->name = trim($value[1]);
        //     $race->hp = $value[3];
        //     $race->exp = $value[4];
        //
        //     $race->save();
        //
        // }

        $races = Race::whereNotNull('coords')->get();

        foreach ($races as $race) {
            $raceContent = Race::getRaceContentWiki($race->name_url);

            $geo = array();

            preg_match_all('/\((?P<x>\d+)\,(?P<y>\d+)\,(?P<z>\d+)/', $raceContent, $match);
            $x = @$match['x'];
            $y = @$match['y'];
            $z = @$match['z'];

            $count = count($x);

            if ($count > 0 ) {
              for ($i=0;$i<$count;$i++) {
                  $geo[] = '{"x": '.$x[$i].', "y": '.$y[$i].', "floor": '.$z[$i].'}';

              }

              $race->coords = implode(',', $geo);
              $race->type = 1;
              $race->update();

            }
        }


    }



    public function getRacesContent()
    {

      Race::$url = 'subtopic=creatures';
      Race::$urlBase = 'https://secure.tibia.com/library/?';

      $races = Race::getRaceListContentTibia();

      for ($i=141;$i<930;$i+=2) {
        $name =  $races[$i][0];

        $nameSingular = Inflect::singularize($name);

        $nameArray = explode(' ', $name);
        $firstName = array_shift($nameArray);
        $lastName = array_pop($nameArray);
        $restName = implode(' ', $nameArray);
        $allInName = str_replace(' ', '', $name);


        $race = Race::whereRaw('REPLACE(name, " ", "") LIKE  "' . Inflect::singularize($allInName) . '%"')
            ->orWhere('name','like', $name .'%')
            ->orWhere('name','like', $nameSingular .'%')
            ->orWhere('name','like', str_replace('  ', ' ' , Inflect::singularize($firstName) . ' ' . $restName . ' ' . Inflect::singularize($lastName) ) . '%')
            ->first();


        if (!empty($race)) {
            print $race->id;
              $race->type = 2;
              $race->update();
        } else {

        }

      }
    }

}
