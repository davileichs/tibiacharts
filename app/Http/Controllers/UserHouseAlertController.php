<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\UserHouseAlert;


class UserHouseAlertController extends Controller
{
    
    
    public function store(Request $request) 
    {
        $alert = new Alert;
        $alert->house_id = $request->input('house_id');
        $alert->world_id = $request->input('world_id');
        #$alert->user_id = Auth::user->id();
        
        $alert->save();
  
        return back();
    }
}
