<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\HouseStatus;
Use App\House;
Use App\Town;
Use App\World;
Use DB;

class HouseController extends Controller
{

    public function index(Request $request)
    {
        DB::enableQueryLog();

        $worlds = $this->getWorlds();
        $towns = $this->getTowns(array(1,2));


        if (!\Cookie::get('world_id')) {
            $world_first = World::first();
            $select_world = $world_first->id;
        } else {
            $select_world = \Cookie::get('world_id');
        }
        $status = HouseStatus::where('world_id', $select_world)->get();
        $world = World::where('id', \Cookie::get('world_id'))->first();


        return view('principal.pages.houses', compact('request', 'worlds', 'world', 'towns', 'status'));
    }



    public function indexAdmin()
    {

        $houses = House::all();

        $towns = $this->getTowns();

        return view('admin.pages.houses', compact('houses', 'towns'));
    }



    public function update(Request $request)
    {
        $houseUpdate = House::find($request['id']);

        $houseUpdate->update($request->all());

        return back();
    }



    public function getHouse(Request $request)
    {

        $status = HouseStatus::where('house_id', $request->input('house_id'))->where('world_id', $request->input('world_id'))->first();


        return view('ajax.house',compact('status'));
    }



    public function listHouses(Request $request)
    {


            $houses = HouseStatus::where('world_id', \Cookie::get('world_id'));

            if (!empty($request->input('status'))) {

                if ($request->input('status') == 'rented') {
                    $houses = $houses->where('status','LIKE','Rented');
                }
                if ($request->input('status') == 'bided') {

                    $houses = $houses->where(function($query) {
                        $query->where('status','NOT LIKE','Rented')
                                ->Where('status', 'NOT LIKE', 'auctioned (no bid yet)');
                    });
                }
                if ($request->input('status') == 'auctioned') {
                    $houses = $houses->where('status','LIKE','auctioned (no bid yet)');
                }
            }

            if (!empty($request->input('town_id'))) {
                $houses = $houses->where('houses_status.town_id',$request->input('town_id'));
            }

            if (!empty($request->input('order'))) {
                $houses = $houses->join('houses', 'houses.id', '=', 'houses_status.house_id');
                $houses = $houses->orderBy($request->input('order'),'desc');

            }

            $houses = $houses->get();


            return view('ajax.list_houses',compact('houses'));

    }



    public function makeAlert(Request $request)
    {



    }
}
