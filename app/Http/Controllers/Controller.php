<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;


Use App\Town;
use App\World;

class Controller extends BaseController
{

    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    public function getTowns($type='')
    {

        $towns = new Town;
        if (is_array($type)) {
            foreach ($type as $i) {
              $towns = $towns->orWhere('type', $i);
            }
        } else {
            $towns = $towns->Where('type', $type);
        }
        $towns = $towns->orderBy('name','asc')->get();

        return $towns;
    }



    static public function getWorlds()
    {

        $worlds = World::orderBy('name','asc')->get();

        return $worlds;
    }



    // calculate the closest Town of a x y coord
    // $x = position x Long
    // $y = position y o Lat
    public function getClosestTown($x, $y)
    {
        $towns = Town::where('type','=',1)->orderBy('name','asc')->get();

        $dist = 100000;
        foreach ($towns as $town) {
          $coords = explode(',', $town['coord']);

          $coord_x = trim($coords[0]);
          $coord_y = trim($coords[1]);

          if ($coord_x > 0 && $coord_y > 0) {
                $b = abs($x - $coord_x);
                $c = abs($y - $coord_y);

                $a = sqrt(pow($b,2)+pow($c,2));

                if ($a < $dist) {
                  $dist = $a;
                  $nameTown = $town['name'];
                }
          }
        }

      return $nameTown;

    }


    public function setCookie(Request $request)
    {

      if (empty($request->ck_name)) {
        $request->ck_name = 'Empty';
      }

      return back()->cookie($request->ck_name, $request->ck_value, intval(3600*24*60));
    }



    public function getCookie($name)
    {
        $cookie = \Cookie::get($name);

        return $cookie;

    }
}
