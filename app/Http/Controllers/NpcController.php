<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
Use App\Npc;
Use App\Profession;

class NpcController extends Controller
{


    public function index(Request $request)
    {

        $npcs = Npc::whereNotNull('coord')->orderBy('name','asc')->get();
        $professions = Profession::get();

        return view('principal.pages.npcs', compact('npcs', 'professions' , 'request'));
    }



    public function indexAdmin()
    {
        $npcs = Npc::paginate(15);

        $towns = $this->getTowns();

        $professions = Profession::get();

        return view('admin.pages.npcs', compact('npcs', 'towns', 'professions'));
    }



    public function getNpc(Request $request)
    {

        $npc = Npc::where('id', $request->input('id'))->first();


        return view('ajax.npc',compact('npc'));
    }



    public function getDataNpc(Request $request)
    {

        $npc = Npc::where('id', $request->id)->first();

        return $npc;
    }



    public function listNpcs(Request $request)
    {

        if (!empty($request->input('profession'))) {

            $npcs = Npc::where('profession_id',$request->input('profession'))->get();

        } else {
            $npcs = null;
        }

        return view('ajax.list_npcs',compact('npcs'));
    }



    public function store(Request $request)
    {

        $npcStore = new Npc;
        $npcStore->fill($request->all());

        $npcStore->save();

        return back();
    }



    public function update(Request $request)
    {
        $npcUpdate = Npc::find($request->id);

        $npcUpdate->update($request->all());

        return back();
    }



    public function cronNpc($url = '') {

        if (!empty($url)) {
            Npc::$url = $url;
        }
        $npcs = Npc::getNpcsContent();

        #dd($npcs);

        $arrayTowns = array();
        $towns = $this->getTowns();
        foreach ($towns as $town) {
          $arrayTowns[$town->id] = $town->name;

        }

        foreach ($npcs as $npc) {
            $name = $npc[1];
            $prof = $npc[3];

            preg_match('/\((?P<x>\d+)\,(?P<y>\d+)\,(?P<z>\d+)/', $npc[5], $match);
            $x = @$match['x'];
            $y = @$match['y'];
            $z = @$match['z'];

            preg_match('/'.implode('|',$arrayTowns).'/', $npc[5], $matchTown);

            $idTown = array_search(@$matchTown[0], $arrayTowns);

            $fillNpc = Npc::where('name','like',$name)->first();
            if (!$fillNpc) {
                $fillNpc = new Npc;
            } else {
                $idTown = $fillNpc->town_id;
            }

            $fillNpc->name = $name;
            if (!empty($match['x']) && !empty($match['y']) && !empty($match['z'])) {
                $fillNpc->coord = $match['x'] .','.$match['y'];
                $fillNpc->floor = $match['z'];
            }

            $profession = Profession::where('name','like','%'.$prof.'%')->first();
            if (!$profession) {
                $newProfession = new Profession;
                $newProfession->name = '<pt>'.$prof.'</pt>';
                $newProfession->save();
                $idProfession = $newProfession->id;
            } else {
                $idProfession = $profession->id;
            }

            $fillNpc->status = 1;
            $fillNpc->town_id = !empty($idTown) ? $idTown : 0;
            $fillNpc->profession_id = $idProfession;
            $fillNpc->save();
        }

    }


}
