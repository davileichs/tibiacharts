<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Races_kill;
use App\Race;
use \App\Libraries\Inflect;

class Races_killController extends Controller
{


          public function getKills(Request $request)
          {
                $kills = Races_kill::where('race_id', $request->race_id)->where('world_id', \Cookie::get('world_id'))->where('date', '>', date('Y-m-d', strtotime('- 7 days')))->get();


                $return = array();
                foreach ($kills as &$kill) {
                  $return[$kill['date']] = array('day'=>$kill['day'], 'kill'=>$kill['kill'], 'killed'=>$kill['killed']);
                }

                $begin = new \DateTime();
                $begin = $begin->modify( '-7 days' );

                $end = new \DateTime();

                $interval = new \DateInterval('P1D');
                $daterange = new \DatePeriod($begin, $interval ,$end);

                foreach($daterange as $date){
                    $dt = $date->format("Y-m-d");

                    if (empty($return[$dt])) {
                          $return[$dt] = array('day'=>$date->format("d/m"), 'kill'=>0, 'killed'=>0);
                    }
                }
                sort($return);
                return json_encode($return);
          }



          static public function store($request)
          {
              $kill = new Races_kill;

              $kill->create($request);

          }



          public function cronKillStatistics()
          {

              ini_set('max_execution_time', 300);

                $worlds = $this::getWorlds();

                foreach ($worlds as $world) {

                      $post = 'world=' . $world->name;
                      Races_kill::$url = 'subtopic=killstatistics';
                      Races_kill::$urlBase = 'https://secure.tibia.com/community/?';

                      $kills = Races_kill::getKillStatisticContent($post);

                      $count = 0;
                      foreach ($kills as $value)
                      {
                          $name = $value[0];
                          $kill = $value[1];
                          $killed = $value[2];

                          if ($name != 'Total') {

                              $nameSingular = Inflect::singularize($name);

                              $nameArray = explode(' ', $name);
                              $firstName = array_shift($nameArray);
                              $lastName = array_pop($nameArray);
                              $restName = implode(' ', $nameArray);
                              $allInName = str_replace(' ', '', $name);


                              $race = Race::whereRaw('REPLACE(name, " ", "") LIKE  "' . Inflect::singularize($allInName) . '%"')
                                  ->orWhere('name','like', $name .'%')
                                  ->orWhere('name','like', $nameSingular .'%')
                                  ->orWhere('name','like', str_replace('  ', ' ' , Inflect::singularize($firstName) . ' ' . $restName . ' ' . Inflect::singularize($lastName) ) . '%')
                                  ->first();


                              if (empty($race)) {

                                $newRace = new Race;
                                $newRace->name = $name;
                                $newRace->save();
                                $request['race_id'] = $newRace->id;

                              } else {
                                $request['race_id'] = $race->id;
                              }


                              $killsSaved = Races_kill::where('world_id', $world->id)->where('race_id', $request['race_id'])->where('date', '=', date('Y-m-d'))->count();

                              // check if killstatics had saved in this day
                              if (!$killsSaved) {

                                $request['kill'] = $kill;
                                $request['killed'] =$killed;
                                $request['world_id'] = $world->id;
                                $request['date'] = date('Y-m-d');

                                $race_kill = new Races_killController;
                                $race_kill->store($request);

                              }

                            }
                      }

                }

          }

}
