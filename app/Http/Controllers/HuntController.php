<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Hunt;
use App\Race;
use App\Hunts_Place;
use Illuminate\Support\Facades\DB;


class HuntController extends Controller
{



    public function index(Request $request)
    {

        $races = Race::all();

        if (!empty($request->id)) {
            $places = Hunts_Place::whereHas('races',function($q) use ($request){
                $q->where('race_id', $request->id);
            })->get();

        } else {
            $places = Hunts_Place::all();
        }

        $hunt_zoom = null;
        if (!empty($request->id)) {
            $hunt_zoom = Hunt::find($request->id);

        }


        return view('pages.hunts', compact('places', 'races', 'hunt_zoom', 'request'));

    }



    public function indexAdmin()
    {

        $hunts = Hunt::orderBy('id','ASC')->paginate(100);

        $towns = $this->getTowns();

        $races = Race::all();

        return view('admin.pages.hunts', compact('hunts', 'towns', 'races'));
    }



    public function getHunt(Request $request)
    {

        $place = Hunts_Place::where('id', $request->id)->first();


        return view('ajax.hunt',compact('place'));
    }



    public function listHunts(Request $request)
    {

        $places = Hunts_Place::whereHas('races',function($q) use ($request){
                $q->where('race_id', $request->race_id);
                })->get();

        return view('ajax.list_hunts',compact('places'));

    }



    public function store(Request $request)
    {

        $huntStore = new Hunt;
        $huntStore->fill($request->all());

        $huntStore->save();

        return back();
    }



    public function update(Request $request)
    {
        $huntUpdate = Hunt::find($request->id);

        $huntUpdate->update($request->all());

        return back();
    }



    public function delete(Request $request)
    {
        $huntDelete = Hunt::find($request->id);
        $huntDelete->delete();

        return back();
    }
}
