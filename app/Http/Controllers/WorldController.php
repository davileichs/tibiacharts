<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\World;

class WorldController extends Controller
{
   
    
    public function cronWorld()
    {
        
        $worlds = World::getWorldsContent();
        
        $newWorldsArray = array();
        foreach ($worlds as $world) {
            array_push($newWorldsArray, $world[0]);
        }

        $oldWorlds = World::get();
        $oldWorldsArray = array();
        foreach ($oldWorlds as $oldWorld) {
            array_push($oldWorldsArray, $oldWorld->name);
        }
        
        $this->deleteWorlds(array_diff($oldWorldsArray,$newWorldsArray));
        $this->storeWorlds(array_diff($newWorldsArray,$oldWorldsArray), $worlds);

    }
    
    
    public function deleteWorlds($worlds)
    {
        
        foreach ($worlds as $world) {
            World::where('name','LIKE',$world)->delete();
        }
        
    }
    
    public function storeWorlds($worlds, $extra)
    {
        
        foreach ($worlds as $k=>$world) {
            
            $storeWorld = new World;
            $storeWorld->name = $world;
            $storeWorld->location = $extra[$k][2];
            $storeWorld->type = $extra[$k][3];
            $storeWorld->info = $extra[$k][4];
            
            $storeWorld->save();
        }
        
    }
    
    
}
