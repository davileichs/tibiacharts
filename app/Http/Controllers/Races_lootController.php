<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Races_loot;

class Races_lootController extends Controller
{
    
    
    static public function store($request)
    {
        $loot = new Races_loot;
        
        $loot->logs_system_id = $request['logs_system_id'];
        $loot->race_id = $request['race_id'];
        $loot->killed = $request['killed'];
        $loot->loot = $request['loot'];
        $loot->save();
        
    }
}
