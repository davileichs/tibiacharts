<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    public function indexAdmin()
    {
        
        $users = User::orderBy('name','ASC')->get();
   
        return view('admin.pages.users', compact('users'));
    }
    
    
    
    public function store(Request $request) 
    {

        $userStore = new User;
        $userStore->fill($request->all());
        
        $userStore->save();
  
        return back();
    }
    
    
    
    public function update(Request $request) 
    {
        $userUpdate = User::find($request['id']);

        $userUpdate->update($request->all());
  
        return back();
    }
    
    

    public function delete(Request $request)
    {
        $userDelete = User::find($request['id']);
        $userDelete->delete();
        
        return back();
    }
}
