<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Event;
use App\World;
use App\WorldPlayersOnline;
use DB;

class HomeController extends Controller
{


    public function index() {

        return view('principal.pages.home');
    }
}
