<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Event;

class EventController extends Controller
{
    
    
    public function index() 
    {
        $events = Event::get();
   
        return view('events', compact('events'));
    }
    
    public function indexAdmin()
    {
        
        $events = Event::orderBy('name','ASC')->get();
   
        return view('admin.pages.events', compact('events'));
    }
    
    
    
    public function store(Request $request) 
    {

        $eventStore = new Event;
        $eventStore->fill($request->all());
        
        $eventStore->save();
  
        return back();
    }
    
    
    
    public function update(Request $request) 
    {
        $eventUpdate = Event::find($request['id']);

        $eventUpdate->update($request->all());
  
        return back();
    }
    
    

    public function delete(Request $request)
    {
        $eventDelete = Event::find($request['id']);
        $eventDelete->delete();
        
        return back();
    }
}
