<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPlayer extends Model
{
    
    
    public function users() {
        
        return $this->belongsTo(User::class);
    }
    
    
    
    public function players() {
        
        return $this->belongsTo(Player::class);
    }
}
