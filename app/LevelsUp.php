<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelsUp extends Model
{
    
    
    protected $dates = ['date'];
    protected $table = 'levels_up';
    
    
    public function player()
    {
        return $this->belongsTo(Player::class);
    }
    
    
   
  
    public function levelUp() 
    {    
        if (!empty($this->level)) {
            
            $time = PlayersLog::where('idplayer',$this->player->id)
                ->where('date', '>=' , function($query){
                    $query->select('date')
                            ->from('levels_up')
                            ->where('level', $this->level)
                            ->where('player_id',$this->player->id)
                            ->where('type', 'LIKE', 'upgrade')
                            ->orderBy('level','ASC')
                            ->take(1);
                })->whereRaw('date < coalesce((SELECT date FROM levels_up WHERE level = ' .($this->level+1). ' AND player_id = ' . $this->player->id . ' AND type = "upgrade" ORDER BY level ASC LIMIT 1), "2100-01-01")')
                ->count();
        }
        return !empty($time) ? $time*15/60 : 0;
    }
    
    
    
    
    public function levelDown() 
    {    
        if (!empty($this->level)) {
            
            $time = PlayersLog::where('idplayer',$this->player->id)
                ->where('date', '>=' , function($query){
                    $query->select('date')
                            ->from('levels_up')
                            ->where('level', $this->level-1)
                            ->where('player_id',$this->player->id)
                            ->where('type', 'LIKE', 'downgrade')
                            ->orderBy('level','ASC')
                            ->take(1);
                })->whereRaw('date < coalesce((SELECT date FROM levels_up WHERE level = ' .$this->level. ' AND player_id = ' . $this->player->id . ' AND type = "upgrade" ORDER BY level DESC LIMIT 1), "2100-01-01")')
                ->count();
        }
        
        return !empty($time) ? $time*15/60 : '0';
    }
    
    
}
