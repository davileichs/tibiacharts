<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class World extends Model
{
    
    public static $url = 'subtopic=worlds';
    public static $urlBase = 'https://secure.tibia.com/community/?';
    
    public $timestamps = false;
    
    
    
    public function players() {
        
        return $this->hasMany(Player::class);
    }
    
    
    
    public function status() {
        
        return $this->hasMany(WorldPlayersOnline::class);
    }
    
    
    
    public function housesStatus() {
        
        return $this->hasMany(HouseStatus::class);
    }
    
    
    
    public static function getWorldsContent() {
	
        $tables = getData(self::$urlBase.self::$url);
        $worldArray = array();
     
        $k=0;
        foreach($tables['tr > td'] as $tr) {
                $td = $tr->nodeValue;
                if (strstr($td,'World')==false
                    && stristr($td,'Players')==false
                    && stristr($td,'PvP Type')==false
                    && stristr($td,'Additional Information')==false
                    && stristr($td,'Location')==false) {

                        $aux[] = $td;
                        if ($k%5==4 && $k!=0) {
                            array_push($worldArray, $aux);
                            unset($aux);
                        }
                    $k++;
                    }
        }

            return $worldArray;
    }


}
