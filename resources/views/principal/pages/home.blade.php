@extends('principal.skell')

@push('title')
Home
@endpush
@push('styles')
<style>
    .logo {
        text-align: center;

    }
</style>
@endpush
@section('content')

<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <form action="setCookie" id="formHomeWorld">
          <input type="hidden" name="ck_name" value="world_id" />
          <select class="form-control" name="ck_value" id="home_world">
              @foreach (App\Http\Controllers\Controller::getWorlds() as $w)
              <option value="{{ $w->id }}" @if (Cookie::get('world_id') == $w->id) selected @endif>{{ $w->name }}</option>
              @endforeach
          </select>
      </form>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <a href="/houses">
  				<div class="pricing-box-alt">
    					<div class="pricing-heading">
    						      <i class="fa fa-home fa-5x"></i>
    					</div>
    					<div class="pricing-action">
    						      <span class="btn-default"> @lang('texts.houses_guildhalls')</span>
    					</div>
  				</div>
        </a>
		</div>
    <div class="col-sm-4">
        <a href="/npcs">
  				<div class="pricing-box-alt">
    					<div class="pricing-heading">
    						      <i class="fa fa-users fa-5x"></i>
    					</div>
    					<div class="pricing-action">
    						      <span class="btn-default"> @lang('texts.npcs')</span>
    					</div>
  				</div>
        </a>
		</div>
    <div class="col-sm-4">
        <a href="/bosses">
  				<div class="pricing-box-alt">
    					<div class="pricing-heading">
    						      <i class="fa fa-dot-circle-o fa-5x"></i>
    					</div>
    					<div class="pricing-action">
    						      <span class="btn-default"> @lang('texts.bosses')</span>
    					</div>
  				</div>
        </a>
		</div>

</div>
@endsection
@push('scripts')
<script src="/js/home.js?version={{ rand(10,99) }}"></script>
@endpush
