@extends('principal.skell')

@push('title')
@lang('texts.npcs')
@endpush

@push('styles')
<link href="/css/npcs.css?version={{ rand(10,99) }}" rel="stylesheet">
@endpush
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('texts.npcs')</div>
            <div class="panel-body">
                      <label>@lang('texts.select_npc'):</label>
                      <input type="text" id="tags" class="form-control">
                      <div id="containerTag"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('texts.profession')</div>
            <div class="panel-body">
                <form id="form" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label>@lang('texts.select_profession'):</label>

                    <select class="form-control" id="profession" name="profession">
                        <option value="">@lang('texts.all')</option>
                        @foreach ($professions as $k=>$profession) {
                            <option value="{{ $profession->id }}" @if (!empty($request->input('id') && $request->input('id') == $profession->id)) selected @endif >{{ $profession->profession_trans }}</option>
                        @endforeach
                    </select>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="row" id="showMap">
  <div class="col-sm-12">
    <div class="panel panel-primary">
        <div class="panel-heading">Map</div>
        <div class="panel-body">
            <div id="tibia_map"></div>
            <div id="map_info" class="bg-gray">
                <div  id="info_coords" onclick="SelectElement('info_coords');"></div>
            </div>
        </div>
    </div>
  </div>
</div>

<div id="showNpcs">

</div>

<div class="modal fade" id="infobox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content formNew" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Npc</h4>

            </div>
            <div class="modal-body col-md-12">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">@lang('texts.close')</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="/js/npcs.js?version={{ rand(10,99) }}"></script>
<script src="/js/maps.js?version={{ rand(10,99) }}"></script>


@if(!empty($npcs))
<script type="text/javascript">
SetSize();
var npcsName = [];

@foreach ($npcs as $i=>$npc)
    npcs[{{ $i }}] = {id: {{ $npc->id }}, name: "{{ $npc->name_decoded }}", img: "{{ $npc->image_path }}", coord:[{{ $npc->coord }}], floor:{{ $npc->floor }} };
    npcsName[{{ $i }}] = {value: {{ $npc->id }}, label:"{{ $npc->name_decoded }}", coord_x: {{ $npc->coord_x }}, coord_y: {{ $npc->coord_y }}, floor:{{ $npc->floor }} };
@endforeach

@if(!empty($town_zoom))
    floor = {{ $town->floor }};
    start_x = {{ $town->coord_x }};
    start_y = {{ $town->coord_y }};
    start_zoom = 4;
 @endif


</script>
@endif

@endpush
