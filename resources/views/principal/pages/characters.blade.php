@extends('layouts.default')

@section('title')
Characters
@endsection

@section('cssfile')

@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h4>Escolha um personagem para visualizar</h4>
            </div>
            <div class="box-body with-border">
                @if (!empty($chars))
                    @foreach ($chars as $char)
                <form class="col-md-3 col-xs-6 margin-bottom" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-lg btn-block btn-{{ getButtonColor($request->input('id'), $char->id ) }}">
                        <h4>{{ $char->name }}</h4>
                        <h6>{{ $char->level }}</h6>
                    </button>
                    <input type="hidden" name="id" value="{{ $char->id }}">
                </form>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

@if (!empty($player))
    <div class="col-md-4 col-xs-6">
        <div class="box">
            <div class="box-body">
               
                <h3 class="profile-username text-center">{{ $player->name }}</h3>

                <p class="text-muted text-center">{{ $player->vocation }}</p>

                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Level</b> <a class="pull-right">{{ $player->level }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>World</b> <a class="pull-right">{{ $player->world->name }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Residence</b> <a class="pull-right">{{ $player->town->name }}</a>
                  </li>
                  <li class="list-group-item">
                    <strong>Último Login</strong> <a class="pull-right">{{ $player->log->date->format('d/m/Y H:i') }}</a>
                  </li>
                </ul>
          </div>

        </div>
    </div>    
    <div class="col-md-8 col-xs-12">
        <div class="box">
            <div class="box-body" style="overflow-x: scroll; height: 375px; padding: 10px;">
                    <div id="chart_column" style="width: 100% !important;"></div>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                    <h3 class="box-title">Acesso semanal</h3>
                </div>
            <div class="box-body">
            

            <div id="chart_time" style="padding: 10px; background-color: #FFFFFF"></div>
            
            </div>
      </div>
    </div>

    
    @if (!empty($player->chartYear()))
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Acesso mensal</h3>
            </div>            
            
            <div  style="overflow-x: scroll; height: 375px; padding: 10px;">
                <div id="chart_calendar" style="padding: 10px; background-color: #FFFFFF"></div>
            </div>
      </div>
    </div>
    @endif
@endif
</div>
@endsection

@section('jsfile')

@if (!empty($player))
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart', 'calendar', 'timeline']});
    google.charts.setOnLoadCallback(drawChartLevels);
    
    
    
    function drawChartLevels() {
        
      var data = google.visualization.arrayToDataTable([
        ['Level',  'Horas jogadas', 'Horas recuperadas'],
        @foreach ($player->levels as $level)
        ['{{ $level->level }}', {{ $level->levelUp() }}, {{ $level->levelDown() }}],
        @endforeach
      ]);

      var options = {
        title: 'Tempo em cada level',
        chartArea:{width:'100%'},
        heigth: '70%',
        vAxis: {title: 'Horas'},
        hAxis: {title: 'Level'},
        isStacked: true,
        connectSteps: false,
        legend: { position: 'top' }
      };

      var chart = new google.visualization.SteppedAreaChart(document.getElementById('chart_column'));

      chart.draw(data, options);
    }

/*---------------- CHART BY WEEK ------------------*/


    google.charts.setOnLoadCallback(drawChartTimeLine);

    function drawChartTimeLine() {

      var container = document.getElementById('chart_time');
      var chartTime = new google.visualization.Timeline(container);
      var dataTable = new google.visualization.DataTable();
      dataTable.addColumn({ type: 'string', id: 'Data' });
      dataTable.addColumn({ type: 'string', id: 'Nome' });
      dataTable.addColumn({ type: 'date', id: 'Start' });
      dataTable.addColumn({ type: 'date', id: 'End' });
      dataTable.addRows([
            @foreach ($player->chartWeek() as $line)
                ['{{ $line['date']}}', '{{ $line['name'] }}', {{ $line['dateStart'] }}, {{ $line['dateEnd'] }}],
            @endforeach
        ]);

      var options = {

        timeline: { rowLabelStyle: {color: '#603913',  fontSize: 11 },
        barLabelStyle: { fontName: 'Tahoma', fontSize: 11 } },
        height: 315
      };

      chartTime.draw(dataTable, options);
    }
       

/*-------------------- END BY WEEK -------------------*/

/*-------------------- CHART BY YEAR -----------------*/


    google.charts.setOnLoadCallback(drawChart);

     function drawChart() {
          var dataTable = new google.visualization.DataTable();
          dataTable.addColumn({ type: 'date', id: 'Date' });
          dataTable.addColumn({ type: 'number', id: 'Tempo' });
          dataTable.addRows([
             @foreach ($player->chartYear() as $line)
             [{{ $line->date_js }}, {{ $line->minutes }}],
              @endforeach
           ]);

          var chart = new google.visualization.Calendar(document.getElementById('chart_calendar'));

          var options = {
            title: "Acessos",
             height: 305,
             width: 800,
            calendar: { cellSize: 13 }
          };

          chart.draw(dataTable, options);
      }
/*---------------- END BY YEAR ------------------*/
</script>
@endif

@endsection


