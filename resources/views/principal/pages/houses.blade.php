@extends('principal.skell')

@push('title')
@lang('texts.houses_guildhalls')
@endpush

@push('styles')
<link href="/css/houses.css?version={{ rand(10,99) }}" rel="stylesheet">
@endpush
@section('content')
<h1>@yield('title')</h1>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('texts.town')</div>
            <div class="panel-body">
                <form id="form" class="box" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="box-body">

                            <div class="form-group">
                                <label>@lang('texts.select_town')</label>
                                <select class="form-control" id="idwtown" name="town_id">
                                    <option value="">@lang('texts.all')</option>
                                    @foreach ($towns as $town)
                                    <option value="{{ $town->id }}" @if (!empty($request->input('town_id') && $request->input('town_id') == $town->id)) selected @endif>{{ $town->name }}</option>
                                    @endforeach
                                </select>
                            </div>


                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('texts.legends')</div>

            <div class="panel-body">
                <label>@lang('texts.select_type')</label>

                <div class="btn btn-block btn-danger col-lg-4 col-sm-12 margin-bottom listHouse" data-status="rented">
                    @lang('texts.rented')
                </div>

                <div class="btn btn-block btn-warning col-lg-4 col-sm-12 margin-bottom listHouse" data-status="bided">
                    @lang('texts.active_bid')
                </div>

                <div class="btn btn-block btn-success col-lg-4 col-sm-12 margin-bottom listHouse" data-status="auctioned">
                    @lang('texts.no_bid')
                <input type="hidden" value="" id="housesstatus">
                </div>
              </div>
        </div>
      </div>
</div>
<div class="row" id="showMap">
  <div class="col-sm-12">
    <div class="panel panel-primary">
        <div class="panel-heading">{{ $world->name }}</div>
        <div class="panel-body">
                <div id="tibia_map"></div>
                <div id="map_info" class="bg-gray">
                    <div  id="info_coords" onclick="SelectElement('info_coords');"></div>

                </div>

        </div>
    </div>

   </div>
</div>

<div id="showHouses">

</div>

<div class="modal fade" id="infobox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">@lang('texts.houses_guildhalls')</h4>

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">@lang('texts.close')</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<script src="/js/houses.js?version={{ rand(10,99) }}"></script>
<script src="/js/maps.js?version={{ rand(10,99) }}"></script>

@if(!empty($status))
<script type="text/javascript">
@foreach ($status as $i=>$house)
    houses[{{ $i }}] = {id:'{{ $house->house->id }}', name:'{{ $house->house->name_decoded }}',color:'{{ $house->color }}',coord:[{{ $house->house->coord }}],floor:{{ $house->house->floor }}};
@endforeach

@if(!empty($town_zoom))

floor = {{ $town_zoom->floor }};
start_x = {{ $town_zoom->coord_x }};
start_y = {{ $town_zoom->coord_y }};
start_zoom = 4;
@endif

</script>
@endif

@endpush
