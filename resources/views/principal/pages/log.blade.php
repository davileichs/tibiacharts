@extends('layouts.block')

@push('title')
@lang('texts.log_system')
@endpush

@push('styles')
<link rel="stylesheet" href="/css/log.css">
@endpush
@section('content')


@foreach ($logs as $k=>$log)

<div class="row">
    <div class="col-md-12">
           
        <h3 class="box-title col-md-3 col-xs-12">{{ $log->name }}</h3>
        <label>link: </label>
        <input type="text" class="text-red" id="linkCode" value="http://tibiacharts.com.br/loger/{{ $log->code }}" size="65">
    </div>
</div>

<div class="row log-box">
  
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.exp')</dtitle>
            <div class="text-information">
            {{ $log->hunt->exp }}
            </div>
        </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.time')</dtitle>
            <div class="text-information">
            {{ $log->hunt->time }}
            </div>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.average')</dtitle>
            <div class="text-information">
            {{ $log->hunt->perhour }}/h
            </div>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.profit')</dtitle>
            <div class="text-information">
            {{ $log->profit->profit }} gp's
            </div>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.waste')</dtitle>
            <div class="text-information">
            {{ $log->profit->waste }} gp's
            </div>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.total')</dtitle>
            <div class="text-information">
            {{ $log->profit->total }} gp's
            </div>
        </div>
    </div>
</div>

<div class="row log-box">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.hits')</dtitle>
            <div class="text-information">
            {{ $log->hits->total }}
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.dps')</dtitle>
            <div class="text-information">
            {{ $log->hits->dpt }}
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.best')</dtitle>
            <div class="text-information">
            {{ $log->hits->best }}
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.heal')</dtitle>
            <div class="text-information">
            {{ $log->heal->total }}
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.hps')</dtitle>
            <div class="text-information">
            {{ $log->heal->dpt }}
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="half-unit log-information text-center">
            <dtitle>@lang('texts.best')</dtitle>
            <div class="text-information">
            {{ $log->heal->best }}
            </div>
        </div>
    </div>

   
</div>
<div class="row">
    <div class="col-md-12">
        
        <div id="chart_div" style="width: 100%; height: 500px;" ></div>
        
    </div>
</div>
                  
<div class="row m-b-2">
    <div class="col-md-12">
        
          <div id="piechart" style="width: 100%; height: 350px;"></div>
        
    </div>
</div>
<div class="row m-b-2">
    <div class="col-md-12">
        
            <div class="half-unit">
                <dtitle><h5 class="size 10">@lang('texts.itens_used')</h5></dtitle>
                @foreach ($log->itensUsed as $used)
                    
                    <img src="{{ $used->img }}" title="{{ $used->title }}"> <label>{{ $used->label }}</label>
                  
              @endforeach
            </div>
        
    </div>
</div>
<div class='row'>
    
    @foreach ($log->races as $race)
        <div class="col-md-4">
            <div class="half-unit text-center">
                <img src='{{ $race->race->imagePath }}'>
                <dtitle><h5 class="size 10">{{ $race->race->name }} <small>({{ $race->killed }})</small></h5></dtitle>
                
               @foreach ($race->loot as $loot)
                    <a href="{{ $loot->urlWiki }}" target="_blank"><img src="{{ $loot->img }}" title="{{ $loot->title }} ({{ $loot->amount }})"> </a>
                @endforeach

        </div>
    </div>
    @endforeach
</div>
@endforeach

@endsection

@push('scripts')
<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart () {

        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Tempo');
        data.addColumn('number', 'Experiência');
        data.addColumn('number', 'Média');

        data.addRows([
          @foreach ($log->exp_array as $k=>$exp)
              [{{ $k*5 }}, {{ $exp }}, {{ ($log->hunt->perhour_int) }}],
          @endforeach
        ]);
      var options = {
       chartArea:{left:100,top:40,width:'90%',height:'75%',color: 'blue'},
       curveType: 'function',
       min: 0,
       backgroundColor: '#383737',
        chart: {
          title: 'Experiência / tempo',
          subtitle: '',
          color: '#CCCCCC'
        },
     
        hAxis: {title: 'Minutos', 
                titleTextStyle: {
                    color: '#CCCCCC'
                },
                textStyle: {
                    fontName: 'Times-Roman',
                    fontSize: 16,
                    bold: true, 
                    opacity: 1,
                    color: '#CCCCCC'
                },
    
                gridlines: {count: {{ count($log->exp_array) }},
                            color: '#2f2f2f'}
        },
        vAxis: {
                1: {
                    title:'Losses',
                    textStyle: {color: 'red'}
                },
                title: 'Experiência por hora', 
                titleTextStyle: {
                    color: '#CCCCCC'
                },
                textStyle: {
                    fontName: 'Times-Roman',
                    fontSize: 16,
                    bold: true,
                    opacity: 0.8,
                    format: 'long',
                    color: '#CCCCCC'
                },
                gridlines: {count: 10,
                            color: '#2f2f2f'}
        },
        tooltip: {
            textStyle: {
                color: '#CCCCCC',
             
            }
         }

      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawChartPie);
    
    function drawChartPie() {

                var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Criaturas mortas');
                    data.addColumn('number', 'Kills');
        

                    data.addRows([
                      @foreach ($log->races as $k=>$race)
                           ["{{ $race->race->name }}",{{ $race->killed }}],
                      @endforeach
                 ]);
      
    
                var options = {
                  title: 'Criaturas mortas',
                  chartArea:{left:100,top:50,width:'80%',height:'75%'},
                  pieHole: 0.3,
                  backgroundColor: '#383737',
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, options);
              }
    

</script>
@endpush