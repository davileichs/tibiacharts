@extends('layouts/default')

@section('title')
Eventos
@endsection

@section('cssfile')
<link rel="stylesheet" href="/plugins/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="/plugins/fullcalendar/fullcalendar.print.css" media="print">
@endsection

@section('content')
<div class="row">
    <div class="box box-primary">
        <div class="box-body no-padding">
            <!-- THE CALENDAR -->
            <div id="calendar"></div>
        </div>
    <!-- /.box-body -->
    </div>
</div>
@endsection

@section('jsfile')
<!-- fullCalendar 2.2.5-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/plugins/fullcalendar/fullcalendar.min.js"></script>

<script>
/* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
      },
      buttonText: {
        today: 'today',
      },
      //Random default events
      events: [
      @foreach ($events as $event)
        {
          title: "{{ $event->name_html }}",
          start: "{{ $event->date_start_int }}",
          end: '{{ $event->date_end_int }}',
          url: 'http://www.tibia.com/library/?subtopic=worldquests&page=details&worldquest={{ $event->name_url }}',
          backgroundColor: "{{ $event->color }}",
          borderColor: "{{ $event->color }}"
        },
      @endforeach
      ],
      editable: false,
      droppable: false, // this allows things to be dropped onto the calendar !!!
    });    
</script>
@endsection