@extends('layouts.block')

@push('title')
@lang('texts.log_system')
@endpush

@push('styles')
<link rel="stylesheet" href="/plugins/iCheck/all.css">
<link rel="stylesheet" href="/plugins/ionslider/ion.rangeSlider.css">
<link rel="stylesheet" href="/plugins/ionslider/ion.rangeSlider.skinHTML5.css">
@endpush
@section('content')
<h1>@yield('title')</h1>
<div class="row logs">
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body">
                <button class="btn btn-block btn-primary btn-lg cleanForm col-md-12" data-toggle="modal" data-target="#formLog">
                    @lang('texts.insert_log')
                 </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="formLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formLog"  enctype="multipart/form-data" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Inserir Log</h4>

            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-xs-12">
                  <label for="inputText">Texto</label>
                  <textarea name="logtext" class="form-control" id="inputText" ></textarea>
                </div>
                  <h4>OU</h4>
                <div class="form-group col-xs-12">
                  <label for="inputFile">Escolha um arquivo</label>
                  <input type="file" id="inputFile" name="log">
                </div>
                      
                <div class="form-group col-xs-12 col-sm-6">
                  <label for="inputName">Nome</label>
                  <input type="text" name="name" class="form-control" id="inputName" >
                </div>
                  <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="typeSubmit" id="typeSubmit" value="new">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Enviar</button>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<!--<script src="/plugins/iCheck/icheck.min.js"></script>
<script src="/plugins/ionslider/ion.rangeSlider.min.js"></script>-->
@endpush

