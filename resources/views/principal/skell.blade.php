<!DOCTYPE html>
<html lang="en">
		<head>
		@include('principal.sides.header')
		</head>
		<body>

		<div id="wrapper">
			<!-- start header -->
			<header>
				<div class="top">
						<div class="container">
							<div class="row">

								@include('principal.sides.top')

							</div>
						</div>
				</div>


				<div class="navbar navbar-default navbar-static-top">
            <div class="container">

							@include('principal.sides.nav')


            </div>
        </div>
			</header>
			<!-- end header -->

			<section id="content">

					<div class="container">

						<section>
									<ul class="breadcrumb">
												<li><i class="fa fa-home"></i><i class="icon-angle-right"></i></li>
												<li class="active">@stack('title')</li>
									</ul>
						</section>

							@yield('content')
					</div>
			</section>

			<footer>
					@include('principal.sides.footer')
			</footer>

		</div>
		<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
		@include('principal.sides.scripts')
		</body>
</html>
