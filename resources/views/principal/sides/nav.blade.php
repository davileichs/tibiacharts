
<div class="nav navbar-right">
    <a class="navbar-brand" href="/"><img src="/images/tibiacharts.png" alt="" height="60" /></a>
</div>
<div class="nav navbar-left">
    <ul class="nav navbar-nav">
        <li class="text-center @if (Request::is('*/houses') || Request::is('houses')) active @endif "><a href="houses"><i class="fa fa-home fa-4x color-yellow"></i> <br />@lang('texts.houses_guildhalls')</a></li>
        <li class="text-center @if (Request::is('*/npcs') || Request::is('npcs')) active @endif "><a href="npcs"><i class="fa fa-users fa-4x"></i> <br />@lang('texts.npcs')</a></li>
        <li class="text-center @if (Request::is('*/bosses') || Request::is('bosses')) active @endif "><a href="bosses"><i class="fa fa-dot-circle-o fa-4x"></i> <br />@lang('texts.bosses')</a></li>
    </ul>
</div>
