<div id="sub-footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="copyright">
          <p>&copy; TibiaCharts - All Right Reserved</p>
          <p>2015-{{ date('Y') }}</p>
        </div>
      </div>

    </div>
  </div>
</div>
