
<div class="nav navbar-left col-xs-9 col-sm-3">
  <form action="setCookie" id="formHeadWorld" class="topleft-info">
      <input type="hidden" name="ck_name" value="world_id" />
      <select class="form-control" name="ck_value" id="head_world" style="width: 100%;">
          @foreach (App\Http\Controllers\Controller::getWorlds() as $w)
          <option value="{{ $w->id }}" @if (Cookie::get('world_id') == $w->id) selected @endif>{{ $w->name }}</option>
          @endforeach
      </select>
  </form>
</div>

<div class="nav navbar-right col-xs-3 col-sm-2">
  <div id="filters-container" class="cbp-l-filters-button">
    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

          <a rel="alternate" class="btn btn-default btn-xs" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">{{ $localeCode }}</a>

		@endforeach
		</div>
</div>
