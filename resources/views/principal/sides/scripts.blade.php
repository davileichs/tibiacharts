<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCv3x9-PZE6UJwhbRkKWgGUFZ8w-9SJmD8"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="/plugins/jQueryUI/jquery-ui.min.js"></script>
<script src="/templates/Sailor/js/modernizr.custom.js"></script>
<script src="/templates/Sailor/js/jquery.easing.1.3.js"></script>
<script src="/templates/Sailor/js/bootstrap.min.js"></script>
<script src="/templates/Sailor/js/jquery.appear.js"></script>
<script src="/templates/Sailor/js/stellar.js"></script>
<script src="/templates/Sailor/js/classie.js"></script>
<script src="/templates/Sailor/js/jquery.cubeportfolio.min.js"></script>
<script src="/templates/Sailor/js/google-code-prettify/prettify.js"></script>
<script src="/templates/Sailor/js/animate.js"></script>

<script type="text/javascript" src="/js/all.js"></script>

@stack('scripts')

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69727713-1', 'auto');
ga('send', 'pageview');

</script>
<!-- Hotjar Tracking Code for www.tibiacharts.com.br -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:99037,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
