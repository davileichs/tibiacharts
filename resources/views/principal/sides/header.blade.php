<meta charset="utf-8">
<title>TibiaCharts | @stack('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="/templates/Sailor/css/bootstrap.min.css" rel="stylesheet" />
<link href="/templates/Sailor/css/cubeportfolio.min.css" rel="stylesheet" />
<link href="/templates/Sailor/css/style.css" rel="stylesheet" />

<link href="/css/all.css" rel="stylesheet" />
<!-- Theme skin -->
<link id="t-colors" href="/templates/Sailor/skins/yellow.css" rel="stylesheet" />

<!-- boxed bg -->
<link id="bodybg" href="/templates/Sailor/bodybg/bg1.css" rel="stylesheet" type="text/css" />
@stack('styles')
<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->
