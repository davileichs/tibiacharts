@extends('ajax.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('texts.npcs')</div>

            <div class="panel-body">
                  @foreach ($npcs as $value)
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="npc-unit" data-coord_x="{{ $value->coord_x}}"  data-coord_y="{{ $value->coord_y}}" data-floor="{{ $value->floor}}">
                          <dtitle>{{ $value->name }} </dtitle>
                          <hr>

                          <div class="col-md-12">
                              <h4>{{ $value->town->name }}</h4>
                          </div>

                      </div>
                  </div>
                  @endforeach
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    $('.npc-unit').click(function(){
        var start_x = $(this).data('coord_x');
        var start_y = $(this).data('coord_y');
        var floor = $(this).data('floor');
        var start_zoom = 5;

        Init(start_x,start_y,floor,start_zoom);

        $('html, body').animate({
            scrollTop: $("#showMap").offset().top-50
        }, 1000);

    });
</script>
@endsection
