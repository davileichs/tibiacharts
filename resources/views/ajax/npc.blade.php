@extends('ajax.layout')

@section('content')
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.name')</p>
              <h4 class="list-group-item-heading">{{ $npc->name }}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.profession')</p>
              <h4 class="list-group-item-heading">{{ $npc->profession->profession_trans }}</h4>
            </div>
        </div>
    </div>
 </div>   
@if (!empty($npc->observation_trans))
<div class="row">
    <div class="col-md-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.observation'):</p>
              <h4 class="list-group-item-heading">{{ $npc->observation_trans }}</h4>
            </div>
        </div>
    </div>
</div>
@endif
</div>

@endsection