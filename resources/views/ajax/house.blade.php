@extends('ajax.layout')

@section('content')

<div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">{{ $status->house->type }}</p>
              <h4 class="list-group-item-heading">{{ $status->house->name }}</h4>

            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.size')</p>
              <h4 class="list-group-item-heading">{{ $status->house->size }} sqm</h4>

            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="list-group">
            <div class="list-group-item  list-group-item-{{ $status->btn }}">
              <p class="list-group-item-text">@lang('texts.status')</p>
              <h4 class="list-group-item-heading">{{ $status->simple_status }}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.rent')</p>
              <h4 class="list-group-item-heading">{{ $status->house->rent }} gps</h4>

            </div>
        </div>
    </div>
    @if (!empty($status->time_status[0]))
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.bid')</p>
              <h4 class="list-group-item-heading">{{ $status->time_status[0] }}</h4>
            </div>
        </div>
    </div>
    @endif

    @if (!empty($status->time_status[1]))
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.time_left')</p>
              <h4 class="list-group-item-heading">{{ $status->time_status[1] }}</h4>
            </div>
        </div>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.last_update')</p>
              <h4 class="list-group-item-heading">{{ $status->last_update_formated }}</h4>

            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">

                <form action="https://secure.tibia.com/community/?subtopic=houses&page=view" method="post" target="_blank">
                    <input type="hidden" name="world" value="{{ $status->world->name }}">
                    <input type="hidden" name="town" value="{{ $status->town->name }}">
                    <input type="hidden" name="state" value="">
                    <input type="hidden" name="type" value="houses">
                    <input type="hidden" name="order" value="">
                    <input type="hidden" name="houseid" value="{{ $status->house->idtibia }}">
                    <button type="submit" class="btn btn-block btn-info" style="padding: 20px" name="View" alt="View">@lang('texts.checkin') Tibia.com</button>
                </form>


    </div>
</div>
@endsection
