@extends('ajax.layout')

@section('content')
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.location')</p>
              <h4 class="list-group-item-heading">{{ $place->hunt->name }} <small>{{ $place->title }}</small></h4>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.level')</p>
              <h4 class="list-group-item-heading">{{ $place->hunt->level }}</h4>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.creatures')</p>
              <div class="row-heigth">
              @foreach($place->races as $race)
              <div class="col-md-4 col-sm-4 col-xs-4 text-center"><h6>{{ $race->name }}<br /><small>Exp: {{ $race->exp }}</small></h6><img src="{{ $race->image_path }}" /></div>
              @endforeach
              </div>
              <div class="clearfix"></div>
            </div>
            
        </div>
    </div>
</div>

@endsection