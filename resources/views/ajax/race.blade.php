@extends('ajax.layout')

@section('content')
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.name')</p>
              <h4 class="list-group-item-heading">{{ $race->name }}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item text-center">
              <img src="{{ $race->image_path }}" />
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.hp')</p>
              <h4 class="list-group-item-heading">{{ $race->hp }}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
              <p class="list-group-item-text">@lang('texts.exp')</p>
              <h4 class="list-group-item-heading">{{ $race->exp }}</h4>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="list-group">
            <div class="list-group-item">
                <h4>Kill Statistics</h4>
                <div id="chart_box">
                    <div id="chart_div"></div>
                </div>
            </div>
        </div>
    </div>

 </div>

</div>
@endsection
