@extends('ajax.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="dash-unit">
            <dtitle>@lang('texts.hunts')</dtitle>
            <hr>
            @foreach ($places as $place)
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="hunt-unit hunt-box" data-coord_x="{{ $place->coord_x }}"  data-coord_y="{{ $place->coord_y }}" data-floor="{{ $place->floor }}">
                    <h4>{{ $place->hunt->name }} </h4>
                    <h5>{{ $place->title }}</h5>

                </div>
            </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    $('.hunt-unit').click(function(){
        var start_x = $(this).data('coord_x');
        var start_y = $(this).data('coord_y');
        var floor = $(this).data('floor');
        var start_zoom = 5;

        Init(start_x,start_y,floor,start_zoom);

        $('html, body').animate({
            scrollTop: $("#showMap").offset().top-50
        }, 1000);

    });
</script>
@endsection
