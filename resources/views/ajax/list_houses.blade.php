@extends('ajax.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('texts.houses_guildhalls')</div>
              <div class="panel-body">
                    <div class="row col-md-12">
                        <form id="form" class="box" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <div class="form-group">
                                        <label>@lang('texts.select_order')</label>
                                        <select class="form-control" id="changeorder" name="order">
                                            <option value=""></option>
                                            <option value="name">@lang('texts.name')</option>
                                            <option value="size">@lang('texts.size')</option>
                                            <option value="rent">@lang('texts.rent')</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    @foreach ($houses as $value)
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="house-unit house-{{ $value->btn }}" data-coord_x="{{ $value->house->coord_x}}"  data-coord_y="{{ $value->house->coord_y}}" data-floor="{{ $value->house->floor}}">
                            <dtitle>{{ $value->town->name }} - {{ $value->house->type }}</dtitle>
                            <hr>

                            <div class="col-md-12">
                                <h4>{{ $value->house->name_utf8 }}</h4>
                            </div>

                            <hr>
                            <div class="col-md-12">
                                <span class="pull-left">{{ $value->house->size }} sqm </span> <span class="pull-right"> {{ $value->house->rent }} gps  </span>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                    @endforeach
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
 $(document).ready(function(){
    $('.house-unit').click(function(){
        var start_x = $(this).data('coord_x');
        var start_y = $(this).data('coord_y');
        var floor = $(this).data('floor');
        var start_zoom = 7;

        Init(start_x,start_y,floor,start_zoom);

        $('html, body').animate({
            scrollTop: $("#showMap").offset().top-50
        }, 1000);

    });

    $('#changeorder').change(function(){
        listHouse();
    });
});
</script>
@endsection
