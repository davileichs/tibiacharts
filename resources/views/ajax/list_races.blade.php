@extends('ajax.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="panel panel-primary">
          <div class="panel-heading">@lang('texts.races')</div>

          <div class="panel-body">

                @foreach ($coords as $i=>$coord)
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <div class="race-unit" data-coord_x="{{ $coord->x }}"  data-coord_y="{{ $coord->y }}" data-floor="{{ $coord->floor}}">
                      <dtitle>{{ $race->name }} </dtitle>
                      <hr>

                      <div class="col-md-12">
                          <h4>{{ $coord->town }}</h4>
                      </div>

                    </div>
                </div>
                @endforeach
          </div>
          <div class="clearfix"></div>
      </div>
    </div>
</div>
<script>
    $('.race-unit').click(function(){
        var start_x = $(this).data('coord_x');
        var start_y = $(this).data('coord_y');
        var floor = $(this).data('floor');
        var start_zoom = 5;

        Init(start_x,start_y,floor,start_zoom);

        $('html, body').animate({
            scrollTop: $("#showMap").offset().top-50
        }, 1000);

    });
</script>
@endsection
