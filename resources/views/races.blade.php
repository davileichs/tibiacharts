@extends('layouts.block')

@push('title')
@lang('texts.npcs')
@endpush

@push('styles')
<link href="/css/races.css?version={{ rand(10,99) }}" rel="stylesheet">
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="dash-unit">
            <dtitle>@lang('texts.races')</dtitle>
            <hr>
            <form id="form" class="box" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">

                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <label>@lang('texts.select_race'):</label>
                          <select class="form-control listRaces" name="id">
                              <option value="">@lang('texts.all')</option>

                              @foreach ($races as $k=>$race) {
                                  <option value="{{ $race->id }}" @if (!empty($request->input('id') && $request->input('id') == $race->id)) selected @endif>{{ $race->name }}</option>';
                              @endforeach
                          </select>
                        </div>
                        <div class='clearfix'></div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">@lang('texts.search')</button>
                        </div>
                  </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="row" id="showMap">
    <div class="col-md-12">
        <div class="dash-unit">
            <dtitle>@lang('texts.map')</dtitle>
            <hr>
            <div class="box col-md-12">
                <div id="tibia_map"></div>
                <div id="map_info" class="bg-gray">
                    <div  id="info_coords" onclick="SelectElement('info_coords');"></div>
                    <small>Original Code by Aissy of tibialibrary.org.</small>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showRaces">

</div>

<div class="modal fade" id="infobox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content formNew" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Race</h4>

            </div>
            <div class="modal-body col-md-12">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">@lang('texts.close')</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="/js/races.js?version={{ rand(10,99) }}"></script>
<script src="/js/maps.js?version={{ rand(10,99) }}"></script>


@if(!empty($places))
<script type="text/javascript">
SetSize();
@if (!empty($races))
  @foreach ($races as $i=>$race)
    @if ($race->coords)
        races.push({id: {{ $race->id }}, name:"{{ $race->name }}", img: "{{ $race->image_path }}", coord:{!! ($race->coords) !!} });
    @endif
  @endforeach
@endif


</script>
@endif

@endpush
