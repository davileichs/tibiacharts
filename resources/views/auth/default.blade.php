<!DOCTYPE html>
<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <meta charset="UTF-8">
    <title>TibiaCharts -Admin Login</title>
    <link href="{{ asset('templates/zontal/assets/css/login.css') }}" rel="stylesheet" type="text/css" />
</head>

<body>
    @yield('content')
</body>
</html>
<script src="{{ asset('templates/zontal/assets/js/login.js') }}"></script>