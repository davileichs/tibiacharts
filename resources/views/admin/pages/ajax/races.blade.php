<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <button class="btn btn-primary btn-block cleanForm newRace">
                Novo
             </button>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Races
        </div>
        <div class="panel-body">
            <div class='table-responsive'>
                <table id="" class="table table-responsive">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($races as $race)
               
                        <tr>
                             <td>{{ $race->id }}</td>
                             <td>{{ $race->name }}</td>

                             <td>
                                <form class="deleteRace"  method="POST">
                                    <button type="submit" class="col-md-6 btn btn-danger"><i class="fa fa-trash"></i></button>
                                    <input type="hidden" name="place_id" value="{{ $race->pivot->place_id }}">
                                    <input type="hidden" name="race_id" value="{{ $race->pivot->race_id }}">
                                    {{ method_field('DELETE')}}
                                     {!! csrf_field() !!}
                                </form>
                             </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>