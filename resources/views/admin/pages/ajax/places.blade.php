<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <button class="btn btn-primary btn-block cleanForm newPlace">
                Novo
             </button>
        </div>
    </div>

</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Places
        </div>
        <div class="panel-body">
            <div class='table-responsive'>
                <table id="" class="table table-responsive">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>

                            <th>Floor</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($places as $place)
                        <tr>
                             <td>{{ $place->id }}</td>
                             <td>{{ $place->title }}</td>

                             <td>{{ $place->real_floor }}</td>
                             <td>
                                <button class="btn btn-primary col-md-4 col-xs-6 editPlace" data-id="{{ $place->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-warning col-md-4 col-xs-6 openRaces" data-id="{{ $place->id }}">
                                    <i class="fa fa-crosshairs"></i>
                                </button>
                                <button class="btn btn-info col-md-4  col-xs-6 copyPlace" data-id="{{ $place->id }}">
                                    <i class="fa fa-files-o"></i>
                                </button>
                                <form class="deletePlace"  method="POST">
                                    <button type="submit" class="col-md-4 col-xs-6 btn btn-danger"><i class="fa fa-trash"></i></button>
                                    <input type="hidden" name="id" value="{{ $place->id }}">
                                    {{ method_field('DELETE')}}
                                     {!! csrf_field() !!}
                                </form>
                             </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Title</th>

                            <th>Floor</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
