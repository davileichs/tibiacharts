@extends('admin.layouts.default')

@section('includes')
<link href="{{ asset('/css/hunts.css') }}" rel="stylesheet" />
<script src="{{ asset('/js/admin.hunts.js') }}" type="text/javascript" ></script>

@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <button class="btn btn-primary btn-lg cleanForm newHunt" data-toggle="modal" data-target="#formHunt">
                    Inserir
                 </button>
            </div>
        </div>

    </div>
    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Hunts
           </div>
           <div class="panel-body">
               <div class='table-responsive'>
                   <table id="" class="table table-responsive">
                       <thead>
                           <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Level</th>

                               <th>Town</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                       @foreach ($hunts as $hunt)
                           <tr>
                                <td>{{ $hunt->id }}</td>
                                <td>{{ $hunt->name }}</td>
                                <td>{{ $hunt->level }}</td>
                                <td>{{ $hunt->town->name }}</td>
                                <td>
                                  <button class="btn btn-warning col-md-3 col-xs-6 openPlaces"  data-id="{{ $hunt->id }}">
                                      <i class="fa fa-map-pin"></i> Places
                                  </button>
                                  <button class="btn btn-primary col-md-2  col-md-offset-1 col-xs-6 editHunt"  data-toggle="modal" data-target="#formHunt">
                                       <i class="fa fa-edit"></i> Edit
                                       <input type="hidden" class="getIdPlaceHunt" value="{{ $hunt->id }}">
                                       <input type="hidden" class="getName" value="{{ $hunt->name }}">
                                       <input type="hidden" class="getTown" value="{{ $hunt->town->id }}">
                                       <input type="hidden" class="getBestFor" value="{{ $hunt->best_for }}">
                                       <input type="hidden" class="getSize" value="{{ $hunt->size }}">
                                   </button>

                                   <form class="col-md-2 col-xs-6" action="/admin/hunts" method="POST">
                                       <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                       <input type="hidden" name="id" value="{{ $hunt->id }}">
                                       {{ method_field('DELETE')}}
                                        {!! csrf_field() !!}
                                   </form>
                                </td>
                           </tr>
                       @endforeach
                       </tbody>
                       <tfoot>
                           <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Level</th>

                               <th>Town</th>
                               <th>Action</th>
                           </tr>
                       </tfoot>
                   </table>
               </div>
               <div class="col-md-offset-3 col-md-9">
                {{ $hunts->links() }}
               </div>
           </div>


       </div>

   </div>
</div>
<div class="modal fade" id="formHunt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formHunt" action="/admin/hunts" method="POST">
            <input type="hidden" name="_method" id="method" value="PUT"></span>
            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-6 col-xs-12">
                    <label for="huntName">Name</label>
                    <input type="text" class="form-control" id="huntName" placeholder="Enter name" name="name" />
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <label>Best for</label>
                    <select class="form-control" id="huntBestFor" name="best_for">
                        <option value="Knights">Knights</option>
                        <option value="Paladins">Paladins</option>
                        <option value="Mages">Mages</option>
                    </select>
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label>Town</label>
                    <select class="form-control" id="huntTown" name="town_id">
                        @foreach ($towns as $town)
                       <option value='{{ $town->id }}'>{{ $town->name }}</option>
                       @endforeach
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" id="huntIdPlaceHunt" name="id" value="">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
</div>



<!-- MODAL TO EDIT PLACES -->
<div class="modal fade" id="placesBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Places</h4>

            </div>
            <div class="modal-body col-md-12">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">@lang('texts.close')</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formPlace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" enctype="multipart/form-data" method="post">
            <form class="modal-content formPlace">
            <input type="hidden" id="methodPlace" name="_method" value="PUT">

            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Place</h4>

            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-6 col-xs-12">
                    <label for="title">Title</label>
                    <input type="text" id="title" class="form-control" placeholder="Enter title" name="title" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="huntCoord">Coord</label>
                    <input type="text" id="huntCoord" class="form-control coordenadas" placeholder="Enter Coords" name="coord" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="huntFloor">Floor</label>
                    <select class="form-control floor" id="huntFloor" name="floor">
                        <option value="0">7</option>
                        <option value="1">6</option>
                        <option value="2">5</option>
                        <option value="3">4</option>
                        <option value="4">3</option>
                        <option value="5">2</option>
                        <option value="6">1</option>
                        <option value="7">0</option>
                        <option value="8">-1</option>
                        <option value="9">-2</option>
                        <option value="10">-3</option>
                        <option value="11">-4</option>
                        <option value="12">-5</option>
                        <option value="13">-6</option>
                        <option value="14">-7</option>
                        <option value="15">-8</option>
                    </select>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <label for="huntSize">Size</label>
                    <input type="text" id="huntSize" class="form-control size" name="size" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="huntLevel">Level</label>
                    <input type="text" id="huntLevel" class="form-control" placeholder="Level" name="level" />
                </div>
                <div class="form-group col-md-12 col-xs-12 setMap">
                    
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="id" name="id" value="">
                <input type="hidden" id="hunt_id" name="hunt_id" value="">
                <input type="hidden" id="typeMap" value="polygon">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
        </div>
    </div>
</div>

<!-- MODAL TO EDIT RACES -->

<div class="modal fade" id="racesBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Races</h4>

            </div>
            <div class="modal-body col-md-12">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">@lang('texts.close')</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formRace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Creatures</h4>

            </div>
            <form class="formRace">
                {!! csrf_field() !!}
                {{ method_field('PUT')}}
                <div class="modal-body col-md-12">
                    <div class="row">
                        <div class="col-md-12 form-group">

                                <label> Adicionar: </label>
                                    <select id="race" name="race_id" class="form-control">
                                        <option value=""></option>
                                        @foreach($races as $race)
                                        <option value="{{ $race->id }}">{{ $race->name }}</option>
                                        @endforeach
                                    </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <input type="hidden" id="place_id" name="place_id" value="">
                    <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
