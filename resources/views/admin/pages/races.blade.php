@extends('admin.layouts.default')

@section('includes')
<link href="{{ asset('/css/races.css') }}" rel="stylesheet" />
<script src="{{ asset('/js/admin.races.js') }}" type="text/javascript" ></script>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <button class="btn btn-primary btn-lg cleanForm newRace" data-toggle="modal" data-target="#formRace">
                    Novo
                 </button>
            </div>
        </div>
    </div>
    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Races
           </div>
           <div class="panel-body">
               <div class='table-responsive'>
                   <table id="tableRaces2" class="table table-responsive">
                       <thead>
                           <tr>
                               <th width="1%">#</th>
                               <th width="1%">Image</th>
                               <th width="20%">Name</th>
                               <th>Type</th>
                               <th width="20%">Coords</th>
                               <th>Action</th>
                               <th></th>
                           </tr>
                       </thead>
                       <tbody>
                       @foreach ($races as $race)
                           <tr>
                               <td>{{ $race->id }}</td>
                               <td><img src="{{ $race->image_path }}"></td>
                               <td>{{ $race->name }}</td>
                               <td>{{ $race->type_name }}</td>
                               <td>{{ $race->coords }}</td>
                               <td><button class="btn btn-primary col-md-2 col-xs-6 editRace"  data-toggle="modal" data-target="#formRace">
                                      <i class="fa fa-edit"></i> Edit
                                      <input type="hidden" class="getIdRace" value="{{ $race->id }}">
                                      <input type="hidden" class="getName" value="{{ $race->name }}">
                                      <input type="hidden" class="getType" value="{{ $race->type }}">
                                      <input type="hidden" class="getHp" value="{{ $race->hp }}">
                                      <input type="hidden" class="getExp" value="{{ $race->exp }}">
                                      <input type="hidden" class="getCoords" value="{{ $race->coords }}">
                                  </button>
                                  <form class="col-md-2 col-xs-6" action="/admin/races" method="POST" id="formDelete">
                                      <button class="btn btn-danger deleteRace"><i class="fa fa-trash"></i> Delete</button>
                                      <input type="hidden" name="id" value="{{ $race->id }}">
                                      {{ method_field('DELETE')}}
                                       {!! csrf_field() !!}
                                  </form>

                               </td>
                               <td align="center"><a href="http://tibiawiki.com.br/wiki/{{ $race->name_url }}" target="_blank">Wiki</a></td>
                           </tr>
                       @endforeach
                       </tbody>
                       <tfoot>
                           <tr>
                               <th>#</th>
                               <th>Image</th>
                               <th>Name</th>
                               <th>Type</th>
                               <th>Coords</th>
                               <th>Action</th>
                               <th></th>
                           </tr>
                       </tfoot>
                   </table>
               </div>
           </div>
       </div>
       <div class="col-md-offset-3 col-md-9">
         {{ $races->links() }}
       </div>
   </div>
</div>


<div class="modal fade" id="formRace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formRace" action="/admin/races" method="POST">
            <input type="hidden" name="_method" id="method" value="PUT"></span>
            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-6 col-xs-12">
                    <label for="raceName">Name</label>
                    <input type="text" class="form-control" id="raceName" placeholder="Enter name" name="name" />
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <label for="raceType">Type</label>
                    <select class="form-control" id="raceType" name="type">
                        <option value="1">Boss</option>
                        <option value="2">Normal</option>
                    </select>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <label for="raceHp">HP</label>
                    <input type="text" class="form-control" id="raceHp" placeholder="Enter hp" name="hp" />
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <label for="raceExp">EXP</label>
                    <input type="text" class="form-control" id="raceExp" placeholder="Enter exp" name="exp" />
                </div>

                <div class="form-group col-md-12 col-xs-12">
                    <label for="raceCoords">Coords</label>
                    <textarea class="form-control coordenadas" id="raceCoords" placeholder="" name="coords" ></textarea>
                </div>

                <div class="form-group col-md-12 col-xs-12 setMap">
                    <iframe src="/map.html" width="540" height="350"></iframe>
                </div>



            </div>
            <div class="modal-footer">
                <input type="hidden" id="raceId" name="id" value="">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
</div>
@stop
