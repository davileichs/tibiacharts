@extends('admin.layouts.default')

@section('includes')
<link href="{{ asset('/css/professions.css') }}" rel="stylesheet" />
<script src="{{ asset('/js/admin.professions.js') }}" type="text/javascript" ></script>
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <button class="btn btn-primary btn-lg cleanForm newProfession" data-toggle="modal" data-target="#formProfession">
                    Novo
                 </button>
            </div>
        </div>
    </div>
    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Professions
           </div>
           <div class="panel-body">
               <div class='table-responsive'>
                   <table id="tableProfessions" class="table table-responsive">
                       <thead>
                           <tr>
                               <th>#</th>   
                               <th>Name</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                       @foreach ($professions as $profession)
                           <tr>
                                <td>{{ $profession->id }}</td>
                                <td>{{ $profession->profession_trans }}</td>
                              
                                <td><button class="btn btn-primary col-md-2 col-xs-6 editProfession"  data-toggle="modal" data-target="#formProfession">
                                       <i class="fa fa-edit"></i> Edit
                                       <input type="hidden" class="getIdProfession" value="{{ $profession->id }}">
                                       <input type="hidden" class="getName" value="{{ $profession->name }}">
                                   </button>
                                    <form class="col-md-2 col-xs-6 deleteProfession" action="/admin/professions" method="POST">
                                       <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                       <input type="hidden" name="id" value="{{ $profession->id }}">
                                       {{ method_field('DELETE')}}
                                        {!! csrf_field() !!}
                                   </form>
                                </td>
                                
                           </tr>
                       @endforeach
                       </tbody>
                       <tfoot>
                           <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Action</th>
                           </tr>
                       </tfoot>
                   </table>
               </div>
           </div>
       </div>
        <div class="col-md-8 col-md-offset-4">
        {!! $professions->render() !!}
        </div>
   </div>
</div>

<div class="modal fade" id="formProfession" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formProfession" action="/admin/professions" method="POST">
            <input type="hidden" name="_method" id="method" value="PUT"></span>
            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
       
            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-12 col-xs-12">
                    <label for="professionName">Name</label>
                    <textarea type="text" class="form-control" id="professionName" name="name"></textarea>
                </div>
               
                
            </div>
            <div class="modal-footer">
                <input type="hidden" id="professionId" name="id" value="">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
</div>




@stop