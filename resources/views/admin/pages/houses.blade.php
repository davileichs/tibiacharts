@extends('admin.layouts.default')

@section('includes')
<link href="{{ asset('/css/houses.css') }}" rel="stylesheet" />
<script src="{{ asset('/js/admin.houses.js') }}" type="text/javascript" ></script>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Houses e GuildHall
           </div>
           <div class="panel-body">
               <div class='table-responsive'>
                   <table id="tableHouses" class="table table-responsive">
                       <thead>
                           <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Town</th>
                               <th>Coord</th>
                               <th>Floor</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                       @foreach ($houses as $house)
                           <tr>
                               <td>{{ $house->id }}</td>
                               <td>{{ $house->name }}</td>
                               <td>{{ $house->town->name }}</td>
                               <td>{{ $house->coord }}</td>
                               <td>{{ $house->real_floor }}</td>
                               <td>
                                   <button class="btn btn-primary col-md-12 col-xs-12 editHouse"  data-toggle="modal" data-target="#formHouse">
                                       <i class="fa fa-edit"></i> Edit
                                       <input type="hidden" class="getIdHouse" value="{{ $house->id }}">
                                       <input type="hidden" class="getName" value="{{ $house->name }}">
                                       <input type="hidden" class="getCoord" value="{{ $house->coord ? $house->coord : $house->coord_aux }}">
                                       <input type="hidden" class="getFloor" value="{{ $house->floor }}">
                                       <input type="hidden" class="getIdTown" value="{{ $house->town->id }}">
                                   </button>
                                </td>
                           </tr>
                       @endforeach
                       </tbody>
                       <tfoot>
                           <tr>
                                <th>#</th>
                               <th>Name</th>
                               <th>Town</th>
                               <th>Coord</th>
                               <th>Floor</th>
                               <th>Action</th>
                           </tr>
                       </tfoot>
                   </table>
               </div>
           </div>
       </div>
         <div class="col-md-8 col-md-offset-4">

        </div>
   </div>
</div>
<div class="modal fade" id="formHouse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formHouse" action='/admin/houses' method="POST">
            {{ method_field('PATCH')}}
            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-6 col-xs-12">
                    <label for="housesName">Name</label>
                    <input type="text" class="form-control" disabled="disabled" id="housesName" placeholder="Enter name" name="name" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="housesEmail">Coord</label>
                    <input type="text" class="form-control coordenadas" id="housesCoord" placeholder="Enter Coords" name="coord" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label>Floor</label>
                    <select class="form-control floor" id="housesFloor" name="floor">
                        <option value="0">7</option>
                        <option value="1">6</option>
                        <option value="2">5</option>
                        <option value="3">4</option>
                        <option value="4">3</option>
                        <option value="5">2</option>
                        <option value="6">1</option>
                        <option value="7">0</option>
                        <option value="8">-1</option>
                        <option value="9">-2</option>
                        <option value="10">-3</option>
                        <option value="11">-4</option>
                        <option value="12">-5</option>
                        <option value="13">-6</option>
                        <option value="14">-7</option>
                        <option value="15">-8</option>
                    </select>
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label>Town</label>
                    <select class="form-control" id="housesIdTown" name="town_id">
                       @foreach ($towns as $town)
                       <option value='{{ $town->id }}'>{{ $town->name }}</option>
                       @endforeach
                    </select>
                </div>
                <div class="form-group col-md-12 col-xs-12 setMap">
                    <iframe src="/map.html" width="540" height="350"></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="housesIdHouse" value="">
                <input type="hidden" id="typeMap" value="square">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
</div>

@stop
