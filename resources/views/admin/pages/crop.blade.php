@extends('admin.layouts.default')

@section('includes')

@stop

@section('content')
<div class="row">
    
    <div class="col-md-12">
       <div class="panel panel-default">
           
           <div class="panel-body">
               <form method="post" action="crop">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                   <div class="col-md-4">
                        <label>Location Source:</label>
                        <input type="text" name="locationFrom">
                   </div>
                   <div class="col-md-4">
                        <label>Location To:</label>
                        <input type="text" name="locationTo">
                   </div>
                   <div class="col-md-4">
                        <label>Name:</label>
                        <input type="text" name="name">
                   </div>
                   <button type="submit" class="btn btn-primary">GO</button>
               </form>
           </div>
 
           
       </div>

   </div>
</div>
@stop