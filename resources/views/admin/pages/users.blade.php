@extends('admin.layouts.default')

@section('includes')

@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <button class="btn btn-primary btn-lg cleanForm newUser" data-toggle="modal" data-target="#formUser">
                    Inserir
                 </button>
            </div>
        </div>
    
    </div>
    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Users
           </div>
           <div class="panel-body">
               <div class="table-responsive">
                   <table class="table">
                       <thead>
                           <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>e-mail</th>
                               <th>Type</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                       @foreach ($users as $user)
                           <tr>
                               <td>{{ $user->id }}</td>
                               <td>{{ $user->name }}</td>
                               <td>{{ $user->email }}</td>
                               <td>{{ $user->type }}</td>
                               <td><button class="btn btn-primary col-md-2 col-xs-6 editUser"  data-toggle="modal" data-target="#formUser">
                                       <i class="fa fa-edit"></i> Edit
                                       <input type="hidden" class="getIdAdmin" value="{{ $user->id }}">
                                       <input type="hidden" class="getName" value="{{ $user->name }}">
                                       <input type="hidden" class="getEmail" value="{{ $user->email }}">
                                       <input type="hidden" class="getType" value="{{ $user->type }}">
                                   </button>
                                   <form method="post" action="/admin/users" class="col-md-2 col-xs-6">
                                       <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                       <input type="hidden" name="idadmin" value="{{ $user->id }}">
                                       <input type="hidden" name="delete" value="1">
                                       {{ method_field('DELETE')}}
                                        {!! csrf_field() !!}
                                   </form></td>
                           </tr>
                       @endforeach
                       </tbody>
                   </table>
               </div>
           </div>
       </div>
         <!-- End  Basic Table  -->
   </div>
</div>
<div class="modal fade" id="formUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formUser" action="/admin/users" method="post">
            <span id="method"></span>
            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">User</h4>
            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-6 col-xs-12">
                    <label for="userName">Name</label>
                    <input type="text" class="form-control" id="userName" placeholder="Enter name" name="name" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="userEmail">E-mail</label>
                    <input type="text" class="form-control" id="userEmail" placeholder="Enter email" name="email" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="userPass">Password</label>
                    <input type="password" class="form-control" id="userPass" placeholder="password" name="password" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label>Type</label>
                    <select class="form-control" id="userType" name="type">
                        <option value="1">Master</option>
                        <option value="2">Two Value</option>
                        <option value="3">Three Value</option>
                        <option value="4">Four Value</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="userIdAdmin" value="">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    
    $(document).on('click','.editUser',function(){
        $('#method').html('{{ method_field("PATCH")}}');
        var idAdmin = $(this).find('.getIdAdmin').val();
        var name = $(this).find('.getName').val();
        var email = $(this).find('.getEmail').val();
        var type = $(this).find('.getType').val();
        $('#userIdAdmin').val(idAdmin);
        $('#userName').val(name);
        $('#userEmail').val(email);
        $('#userType').val(type);
        $('#typeSubmit').val('edit');
    });
    
    $(document).on('click','.cleanForm',function(){
       $('.formUser').find("input[type=text],input[type=password], textarea").val("");
       $('#userIdAdmin').val('');
    });
    
    $(document).on('click','.newUser',function(){
        $('#method').html('{{ method_field("PUT")}}');
    });
});
</script>
@stop