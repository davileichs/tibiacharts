@extends('admin.layouts.default')

@section('includes')
<link href="{{ asset('/css/events.css') }}" rel="stylesheet" />
<script src="{{ asset('/js/events.js') }}" type="text/javascript" ></script>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <button class="btn btn-primary btn-lg cleanForm newEvent" data-toggle="modal" data-target="#formEvent">
                    Inserir
                 </button>
            </div>
        </div>
    </div>
    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Eventos
           </div>
           <div class="panel-body">
               <div class='table-responsive'>
                   <table id="tableEvents" class="table table-responsive">
                       <thead>
                           <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Date Start</th>
                               <th>Date End</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                       <?php foreach ($events as $event) { ?>
                           <tr>
                               <td>{{ $event->id }}</td>
                               <td>{{ $event->name }}</td>
                               <td>{{ $event->date_start_formated }}</td>
                               <td>{{ $event->date_end_formated }}</td>
                               <td><button class="btn btn-primary col-md-4 col-xs-6 editEvent"  data-toggle="modal" data-target="#formEvent">
                                       <i class="fa fa-edit"></i> Edit
                                       <input type="hidden" class="getIdEvent" value="{{ $event->id }}">
                                       <input type="hidden" class="getName" value="{{ $event->name }}">
                                       <input type="hidden" class="getDateStart" value="{{ $event->date_start }}">
                                       <input type="hidden" class="getDateEnd" value="{{ $event->date_end }}">
                                       <input type="hidden" class="getDescription" value="{{ $event->description }}">
                                   </button>
                                   <form class="col-md-2 col-xs-6" action="/admin/events" method="POST">
                                       <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                       <input type="hidden" name="id" value="{{ $event->id }}">
                                       {{ method_field('DELETE')}}
                                       {!! csrf_field() !!}
                                   </form>
                                   </td>
                           </tr>
                       <?php } ?>
                       </tbody>
                       <tfoot>
                           <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Date Start</th>
                               <th>Date End</th>
                               <th>Action</th>
                           </tr>
                       </tfoot>
                   </table>
               </div>
           </div>
       </div>
         <!-- End  Basic Table  -->
   </div>
</div>
<div class="modal fade" id="formEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formHouse" action="/admin/events" method="POST">
            <span id="method"></span>
            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
                
            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-12 col-xs-12">
                    <label for="eventsName">Name</label>
                    <input type="text" class="form-control" id="eventsName" placeholder="Enter name" name="name" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="eventsDateStart">Date Start</label>
                    <input type="text" class="form-control" id="eventsDateStart" placeholder="Data inicial" name="date_start" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="eventsDateEnd">Date End</label>
                    <input type="text" class="form-control" id="eventsDateEnd" placeholder="Data final" name="date_end" />
                </div>
                <div class="form-group col-md-12 col-xs-12">
                    <label for="eventsDescription">Name</label>
                    <textarea type="text" class="form-control" rows="6" id="eventsDescription" placeholder="Descrição" name="description"></textarea>
                </div>
                
            </div>
            <div class="modal-footer">
                <input type="hidden" id="eventsIdEvent" name="id" value="">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function(){
    
    $('#tableEvents').DataTable();
    
    $(document).on('click','.editEvent',function(){
        $('#method').html('{{ method_field("PATCH")}}');
        
        var idEvent = $(this).find('.getIdEvent').val();
        var name = $(this).find('.getName').val();
        var date_start = $(this).find('.getDateStart').val();
        var date_end = $(this).find('.getDateEnd').val();
        var description = $(this).find('.getDescription').val();
        
        $('#eventsIdEvent').val(idEvent);
        $('#eventsDateStart').val(date_start);
        $('#eventsDateEnd').val(date_end);
        $('#eventsName').val(name);
        $('#eventsDescription').val(description);
        $('#typeSubmit').val('edit');
        
    });
    
    $(document).on('click','.cleanForm',function(){
       $('.formHouse').find("input[type=text],input[type=password], textarea").val("");
       $('#eventsIdEvent').val('');
    });
    
    $(document).on('click','.newEvent',function(){
        $('#method').html('{{ method_field("PUT")}}');
    });
   
});
</script>
@stop