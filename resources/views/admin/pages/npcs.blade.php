@extends('admin.layouts.default')

@section('includes')
<link href="{{ asset('/css/npcs.css') }}" rel="stylesheet" />
<script src="{{ asset('/js/admin.npcs.js') }}" type="text/javascript" ></script>
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <button class="btn btn-primary btn-lg cleanForm newNpc" data-toggle="modal" data-target="#formNpc">
                    Novo
                 </button>
            </div>
        </div>
    </div>
    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Npcs
           </div>
           <div class="panel-body">
               <div class='table-responsive'>
                   <table id="tableNpcs" class="table table-responsive">
                       <thead>
                           <tr>
                               <th>#</th>
                               <th width="10%">Image</th>
                               <th>Name</th>
                               <th>Coord</th>
                               <th>Floor</th>
                               <th>Town</th>
                               <th>Action</th>
                               <th>Wiki</th>
                           </tr>
                       </thead>
                       <tbody>
                       @foreach ($npcs as $npc)
                           <tr>
                                <td>{{ $npc->id }}</td>
                                <td align="center"><img src="{{ $npc->image_path }}"></td>
                                <td>{{ $npc->name }}</td>
                                <td>{{ $npc->coord }}</td>
                                <td>{{ $npc->real_floor }}</td>
                                <td>{{ $npc->town->name }}</td>
                                <td><button class="btn btn-primary col-md-12 editNpc"  data-toggle="modal" data-target="#formNpc">
                                       <i class="fa fa-edit"></i> Edit
                                       <input type="hidden" class="getIdNpc" value="{{ $npc->id }}">
                                       <input type="hidden" class="getName" value="{{ $npc->name }}">
                                       <input type="hidden" class="getCoord" value="{{ $npc->coord }}">
                                       <input type="hidden" class="getTown" value="{{ $npc->town->id }}">
                                       <input type="hidden" class="getProfession" value="{{ $npc->profession->id }}">
                                       <input type="hidden" class="getObservation" value="{{ $npc->observation }}">

                                   </button>
                                </td>
                                <td align="center"><a href="http://tibiawiki.com.br/wiki/{{ $npc->name_url }}" target="_blank">Wiki</a></td>
                           </tr>
                       @endforeach
                       </tbody>
                       <tfoot>
                           <tr>
                               <th>#</th>
                               <th>Image</th>
                               <th>Name</th>
                               <th>Coord</th>
                               <th>Floor</th>
                               <th>Town</th>
                               <th>Action</th>
                               <th>Wiki</th>
                           </tr>
                       </tfoot>
                   </table>
               </div>
           </div>
       </div>
        <div class="col-md-8 col-md-offset-4">
        {!! $npcs->render() !!}
        </div>
   </div>
</div>

<div class="modal fade" id="formNpc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content formNpc" action="/admin/npcs" method="POST">
            <input type="hidden" name="_method" id="method" value="PUT"></span>
            {!! csrf_field() !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <div class="modal-body col-md-12">
                <div class="form-group col-md-6 col-xs-12">
                    <label for="npcName">Name</label>
                    <input type="text" class="form-control" id="npcName" professionholder="Enter name" name="name" />
                </div>
               <div class="form-group col-md-6 col-xs-12">
                    <label for="npcCoord">Coord</label>
                    <input type="text" id="npcCoord" class="form-control coordenadas" professionholder="Enter Coords" name="coord" />
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label for="npcFloor">Floor</label>
                    <select class="form-control floor" id="npcFloor" name="floor">
                        <option value="0">7</option>
                        <option value="1">6</option>
                        <option value="2">5</option>
                        <option value="3">4</option>
                        <option value="4">3</option>
                        <option value="5">2</option>
                        <option value="6">1</option>
                        <option value="7">0</option>
                        <option value="8">-1</option>
                        <option value="9">-2</option>
                        <option value="10">-3</option>
                        <option value="11">-4</option>
                        <option value="12">-5</option>
                        <option value="13">-6</option>
                        <option value="14">-7</option>
                        <option value="15">-8</option>
                    </select>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <label>Town</label>
                    <select class="form-control" id="npcTown" name="town_id">
                        @foreach ($towns as $town)
                        <option value='{{ $town->id }}'>{{ $town->name }}</option>
                       @endforeach
                    </select>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <label>Profession</label>
                    <select class="form-control" id="npcProfession" name="profession_id">
                        @foreach ($professions as $profession)
                        <option value='{{ $profession->id }}'>{{ $profession->profession_trans }}</option>
                       @endforeach
                    </select>
                </div>

                <div class="form-group col-md-12">
                    <label for="npcObservation">Observation</label>
                    <textarea type="text" class="form-control" id="npcObservation" name="observation" ></textarea>
                </div>

                <div class="form-group col-md-12 col-xs-12 setMap">
                    <iframe src="/map.html" width="540" height="350"></iframe>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" id="npcId" name="id" value="">
                <input type="hidden" id="typeMap" value="icon">
                <button type="button" class="btn btn-default cleanForm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
</div>

@stop
