@extends('admin.layouts.default')

@section('includes')
<link href="{{ asset('/css/hunts.css') }}" rel="stylesheet" />
<script src="{{ asset('/js/hunts.js') }}" type="text/javascript" ></script>
@stop

@section('content')

<div class="row">

    <div class="col-md-12">
       <div class="panel panel-default">
           <div class="panel-heading">
               Atualizar Mapa
           </div>

            
           <div class="panel-body">
             <form method="post" class="form-group">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
 
                <div class="form-group  col-md-3">
                    Folder: <input type="text" name="tile">
                </div>
                <div class="form-group  col-md-1">
                    <input type="checkbox" name="new"> Novo
                </div>
                <div class="form-group  col-md-3">
                    <select class="form-control" name="level">
                        @for($i=0;$i<16;$i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="form-group  col-md-5">
                    <select class="form-control" name="file">
                        @foreach($files as $file)
                        <option value="{{ $file }}">{{ $file }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="panel-footer">
                    <button class="btn btn-primary btn-lg cleanForm" type="submit">
                        OK
                     </button>
                </div>
              
            </form>
           </div>
       </div>
         <!-- End  Basic Table  -->
   </div>
</div>

@stop