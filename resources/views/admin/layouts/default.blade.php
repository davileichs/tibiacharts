<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('admin.includes.header')
</head>
<body>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="left-div">
                <div class="user-settings-wrapper">

                    <h3 class="text-yellow"> {{ Auth::user()->name }}</h3>

                </div>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        @include('admin.includes.nav')
    </section>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">@yield('title')</h4>
                </div>
            </div>
            
            @yield('content')
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        @include('admin.includes.footer')
    </footer>
</body>
</html>