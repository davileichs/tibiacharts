<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="" />
<meta name="author" content="" />

<!-- CSS -->
<title>TibiaCharts Admin | @yield('title')</title>
<link href="{{ asset('/templates/zontal/assets/css/bootstrap.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link href="{{ asset('/templates/zontal/assets/css/style.css') }}" rel="stylesheet" />

<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{ asset('/templates/zontal/assets/js/bootstrap.js') }}"></script>
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<!-- Page JS and CSS -->
@yield('includes')
