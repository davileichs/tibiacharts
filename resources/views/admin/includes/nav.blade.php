<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="navbar-collapse collapse ">
                <ul id="menu-top" class="nav navbar-nav navbar-right">
                    <li><a href="/admin/home">Home</a></li>
                    <li><a href="/admin/users">Site Users</a></li>
                    <li><a href="/admin/npcs">Npcs</a></li>
                    <li><a href="/admin/professions">Professions</a></li>
                    <li><a href="/admin/houses">Houses</a></li>
                    <li><a href="/admin/races">Creatures</a></li>
                    <li><a href="/admin/hunts">Hunts Places</a></li>
                    <li><a href="/admin/events">Events</a></li>
                    <li><a href="/admin/map">Map</a></li>
                    <li><a href="/admin/crop">Crop Image</a></li>
                    <li class="pull-left"><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                </ul>
            </div>
        </div>

    </div>
</div>