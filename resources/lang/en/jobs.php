<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Jobs of Npcs
    |--------------------------------------------------------------------------
    |
    */

  'Blacksmith'              => 'Blacksmith',
  'Librarian'               => 'Librarian',
  'Roleplay'                => 'Roleplay',
  'Food Trader'             => 'Food Trader',
  'Tools Trader'            => 'Tools Trader',
  'Environmentalist'        => 'Environmentalist',
  'Jeweler'                 => 'Jeweler',
  'Ship Captain'            => 'Ship Captain',
  'Druid Guild Leader'      => 'Druid Guild Leader',
  'Hunter'                  => 'Hunter',
  'Blesser'                 => 'Blesser',
  'Caste Leader'            => 'Caste Leader',
  'Guard'                   => 'Guard',
  'Instruments Trader'      => 'Instruments Trader',
  'Banker'                  => 'Banker',
  'Guider'                  => 'Guider',
  'Archer'                  => 'Archer',
  'Matchmaker'              => 'Matchmaker',
  'Priest'                  => 'Priest',
  'Postman'                 => 'Postman',
  'Ambassador'              => 'Ambassador',
  'Weapons Trader'          => 'Weapons Trader',
  'Magic Itens Trader'      => 'Magic Itens Trader',
  'Miner'                   => 'Miner',
  'Fornitune Trader'        => 'Fornitune Trader',
  'Unknow'                  => 'Unknow',
  'Special Itens Trader'    => 'Special Itens Trader',
  'Trader'                  => 'Trader',
  'Equipments Trader'       => 'Equipments Trader',
  'Sultan'                  => 'Sultan',
  'Djin'                    => 'Djinn',
  'Arena Referee'           => 'Arena Referee',
  'Vizir'                   => 'Vizir',
  'Chef'                    => 'Chef',
  'Beggar'                  => 'Beggar',
  'Leader'                  => 'Leader',
  'Knight Guild Leader'     => 'Knight Guild Leader',
  'Spy'                     => 'Spy',
  'Sorcerer Guild Leader'   => 'Sorcerer Guild Leader',
  'Emperor'                 => 'Emperor',
  'Criminal'                => 'Criminal',
  'Baker'                   => 'Baker',
  'Armor Trader'            => 'Armor Trader',
  'CGB Leader'              => 'CGB Leader',
  'Flower Trader'           => 'Flower Trader',
  'Fan'                     => 'Fan',
  'Paladin Guild Leader'    => 'Paladin Guild Leader',
  'Tanner'                  => 'Tanner',
  'Waterman'                => 'Waterman',
  'Actor'                   => 'Actor',
  'Professor'               => 'Professor',
  'Queen'                   => 'Queen',
  'Drunk Guy'               => 'Drunk Guy',
  'Attendant'               => 'Attendant',
  'Magic Carpet Manager'    => 'Magic Carpet Manager',
  'Caliph'                  => 'Caliph',
  'Monk'                    => 'Monk',
  'Healer'                  => 'Healer',
  'Director'                => 'Director',
  'Hero'                    => 'Hero',
  'Guild Leader'            => 'Guild Leader',
  'Scientist'               => 'Scientist',
  'Inventor'                => 'Inventor',
  'Scholar'                 => 'Scholar',
  'Housekeeper'             => 'Housekeeper',
  'Magistrate'              => 'Magistrate',
  'Caveman'                 => 'Caveman',
  'Expedition Leader'       => 'Expedition Leader',
  'Rebeld'                  => 'Rebeld',
  'Chief'                   => 'Chief',
  'Acornbuyer'              => 'Acornbuyer',
  'Barbarian'               => 'Barbarian',
  'Lumberjack'              => 'Lumberjack',
  'Researcher'              => 'Researcher',
  'Ghost'                   => 'Ghost',
  'Doctor'                  => 'Doctor',
  'Recruiter'               => 'Recruiter',
  'Nurse'                   => 'Nurse',
  'Tactical Advisor'        => 'Tactical Advisor',
  'Armorer'                 => 'Armorer',
  'Secretary'               => 'Secretary',
  'Adventurer'              => 'Adventurer',
  'Ticket Agent'            => 'Ticket Agent',
  'Rotworn Trainer'         => 'Rotworn Trainer',
  'Technomancer'            => 'Technomancer',
  'Vampire Hunter'          => 'Vampire Hunter',
  'Exploerer Society Delegate' => 'Exploerer Society Delegate',
  'Sugar Caner Cutter'      => 'Sugar Caner Cutter',
  'Bard'                    => 'Bard',
  'Inn Owner'               => 'Inn Owner',
  'Govenor'                 => 'Govenor',
  'Spokesman'               => 'Spokesman',
  'Shaman'                  => 'Shaman',
  'Mermaid'                 => 'Mermaid',
  'Liberator'               => 'Liberator',
  'King'                    => 'King',
  'Pirate'                  => 'Pirate',
  'Bone Master'             => 'Bone Master',
  'Dream Master'            => 'Dream Master',
  'Dryad'                   => 'Dryad',
  'Prophet'                 => 'Prophet',
  'Fisher'                  => 'Fisher',
  'Traveler'                => 'Traveler',
  'Oracle'                  => 'Oracle',
  'Inquisitor'              => 'Inquisitor',
  'Dog'                     => 'Dog',
  'Musher'                  => 'Musher',
  'Frog'                    => 'Frog',
  'Interloper'              => 'Interloper',
  'Jester'                  => 'Jester',
  'TBI Leader'              => 'TBI Leader',
  'Farmer'                  => 'Farmer',
  'Astrologer'              => 'Astrologer',
  'Combined Magical Winterberry Society Member' => 'Combined Magical Winterberry Society Member',
  'Mage'                    => 'Mage',
  'Horse Trainer'           => 'Horse Trainer',
  'Princess'                => 'Princess',
  'Fortune-teller'          => 'Fortune-teller',
  'Real Advertiser'         => 'Real Advertiser',
  'Xeriff'                  => 'Xeriff',
  'Treasure Hunter'         => 'Treasure Hunter',
  'Santa Helper'            => 'Santa Helper',
  'Producer'                => 'Producer',
  'Witcher'                 => 'Witcher',
  'AVIN Leader'             => 'AVIN Leader',
  'Punter'                  => 'Punter',
  'Lightbearer'             => 'Lightbearer',
  'Baron'                   => 'Baron',
  'Plumber'                 => 'Plumber',
  'Voting Judge'            => 'Voting Judge'
];
